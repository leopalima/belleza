<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        /**
         * Roles.
         *
         */

        $listaRoles = [
            [
                'name' => 'Esteticista',
                'descripcion' => 'Las esteticistas llevan a cabo tratamientos de belleza en las personas para mejorar su aspecto, y para ayudarles a sentirse mejor consigo mismas. Estos tratamientos incluyen técnicas de camuflaje cosmético, depilación, tratamientos faciales y masajes.',
            ],
            [
                'name' => 'Especialista en manicura',
                'descripcion' => 'Los especialistas en manicura ofrecen distintos tratamientos para las uñas (de manos y pies) a sus clientes. También trabajan en el arte y la moda para las uñas, por ejemplo, pintándolas en diferentes estilos. Los especialistas en manicura también aplican, reparan y quitan uñas postizas o extensiones.',
            ],
            [
                'name' => 'Asesor de imagen',
                'descripcion' => 'Los asesores de imagen ayudan a sus clientes a sacar el máximo provecho de su aspecto, para que se sientan bien y seguros de sí mismos. También muestran a la gente cómo crear una buena impresión a partir de su forma de hablar o de comportarse, por ejemplo.',
            ],
            [
                'name' => 'Maquilladora',
                'descripcion' => 'Las maquilladoras se aseguran de que las personas que aparecen en televisión, cine o teatro lleven el maquillaje y peinado convenientes. En obras de teatro y programas de entretenimiento se precisa maquillaje creativo. Los programas de noticias, de temas de actualidad y tertulias requieren maquillaje "correctivo" y arreglo general.',
            ],
        ];

        factory('App\Roles')->create();

        factory('App\Roles', 'administrador', 1)->create();

        factory('App\Roles', 'cliente', 1)->create();

        factory('App\Roles', 'profesional', 1)->create();

        for($i=0; $i<4; $i++){
            factory('App\Roles', 1)->create([
                'name' => $listaRoles[$i]['name'],
                'descripcion' => $listaRoles[$i]['descripcion'],
            ]);
        }

        /**
         * Usuarios.
         *
         */

        factory('App\User', 'root', 1)->create();

        factory('App\User', 'admin', 1)->create();

        factory('App\User', 'cliente', 1)->create();

        factory('App\User', 'profesional', 1)->create();
        
        //factory('App\User', 10000)->create();

        /**
         * Servicios.
         *
         */

        $listaServicios = [
            'MANOS MANICURA CLÁSICA',
            'MANOS MANICURA NUTRI-CURA CON CÍTRICOS',
            'MANOS MANICURA PARA NIÑOS',
            'MANOS MANICURA SHELLAC',
            'PIES PEDICURA CLÁSICA',
            'PIES PEDICURA CON SHELLAC',
            'PIES PEDICURA NUTRI-CURA CON CÍTRICOS',
            'PIES PEDICURA PARA NIÑOS',
            'REMOCIÓN DE SHELLAC MANOS',
            'REMOCIÓN DE SHELLAC PIES',
            'CAMBIO DE ESMALTE',
            'CAMBIO DE ESMALTE MANOS',
            'CAMBIO DE ESMALTE PIES',
            'CORTES DE PELO DE ALTA PELUQUERÍA',
            'CORTE Y PEINADO (CABELLO CORTO)',
            'CORTE Y PEINADO (CABELLO LARGO)',
            'CORTE Y PEINADO PARA CABALLEROS',
            'CORTE EN CAPAS LARGAS',
            'CORTE PARA NIÑOS',
            'CORTE PARA NIÑAS',
            'CABALLERO RESPLANDECIENTE CABELLO MAGNÍFICAMENTE ARREGLADO',
            'CABALLERO RESPLANDECIENTE CORONA REAL',
            'CABALLERO RESPLANDECIENTE ESPADA DE LA VERDAD',
            'CABALLERO RESPLANDECIENTE ESCUDO DE VALOR',
            'PEINADOS DISTINTIVOS LAVADO Y SECADO (CABELLO CORTO)',
            'PEINADOS DISTINTIVOS LAVADO Y SECADO (CABELLO LARGO)',
            'PEINADOS DISTINTIVOS LAVADO Y PEINADO CON EXTENSIONES',
            'PEINADOS DISTINTIVOS PEINADO RECOGIDO Y RIZOS ROMÁNTICOS',
            'COLOR PERSONALIZADO REFLEJOS',
            'COLOR PERSONALIZADO REFLEJOS PARCIALES',
            'COLOR PERSONALIZADO BALAYAGE Y OMBRE (TÉCNICA A MANO ALZADA)',
            'COLOR PERSONALIZADO COLOR COMPLETO',
            'COLOR PERSONALIZADO RETOQUE DE COLOR',
            'COLOR PERSONALIZADO TRATAMIENTO DE BRILLO PARA EL CABELLO',
            'COLOR PERSONALIZADO COLOR PARA CABALLEROS',
            'COLOR PERSONALIZADO COLOR CORRECTIVO',
            'RITUALES NUTRITIVOS INDULGENT DELUXE',
            'RITUALES NUTRITIVOS CHRONOLOGISTE',
            'EXTENSIONES DE CABELLO',
            'TRATAMIENTO SUAVIZANTE DE LUJO CON QUERATINA',
            'SERVICIOS PARA BODAS PEINADO PARA EL DÍA DE LA BODA',
            'SERVICIOS PARA BODAS PEINADO DE PRUEBA PARA BODA',
            'SERVICIOS PARA BODAS PEINADO PARA EL DÍA DEL ENSAYO',
            'MAQUILLAJE APLICACIÓN DE MAQUILLAJE CON PESTAÑAS',
            'MAQUILLAJE CLASE DE MAQUILLAJE',
            'MAQUILLAJE MAQUILLAJE PARA NOVIA',
        ];

        for($i=0; $i<46; $i++){
            factory('App\Servicios', 1)->create([
                'name' => $listaServicios[$i],
            ]);
        }
    }
}
