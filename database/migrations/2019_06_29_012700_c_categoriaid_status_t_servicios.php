<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CCategoriaidStatusTServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servicios_al_cliente', function (Blueprint $table) {
            $table->boolean('status')->default(0);
            $table->bigInteger('categoria_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servicios_al_cliente', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropForeign(['categoria_id']);
        });
    }
}
