<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaUserRolesRolesMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersroles_rolesmenu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('roles_menu_id');
            $table->foreign('roles_menu_id')->references('id')->on('roles_menu');
            $table->unsignedBigInteger('user_roles_id');
            $table->foreign('user_roles_id')->references('id')->on('users_roles');
            $table->boolean('visualizar');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersroles_rolesmenu');
    }
}
