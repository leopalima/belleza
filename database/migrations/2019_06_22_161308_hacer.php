<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Hacer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hacer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('user_id',36);
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('tipo');
            $table->text('descripcion');
            $table->text('estado');
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hacer');
    }
}
