<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaServicioAlClienteComponentes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios_al_cliente_componentes', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('perfil', 254);
            $table->string('url_componente');
            $table->char('servicio_id', 36);
            $table->foreign('servicio_id')
                    ->references('id')->on('servicios_al_cliente');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_al_cliente_componentes');
    }
}
