<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaServicioAlClienteRecomendacionesUse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio_recomendaciones_use', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->bigInteger('producto_id')->unsigned();
            $table->foreign('producto_id')
                    ->references('id')->on('producto');
            $table->integer('cantidad');
            $table->char('medida');
            $table->char('recomendaciones_id', 36);
            $table->foreign('recomendaciones_id')
                    ->references('id')->on('servicios_al_cliente_recomendaciones');
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicio_recomendaciones_use');
    }
}
