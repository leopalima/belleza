<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaServicioAlClienteRecomendacionesDodont extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio_recomendaciones_dodont', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('tipo');
            $table->char('tarea');
            $table->char('recomendaciones_id', 36);
            $table->foreign('recomendaciones_id')
                    ->references('id')->on('servicios_al_cliente_recomendaciones');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicio_recomendaciones_dodont');
    }
}
