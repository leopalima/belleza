<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaUsersRolesRolesFormularios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersroles_rolesformularios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('roles_formularios_id');
            $table->foreign('roles_formularios_id')->references('id')->on('roles_formularios');
            $table->unsignedBigInteger('users_roles_id');
            $table->foreign('users_roles_id')->references('id')->on('users_roles');
            $table->boolean('listar');
            $table->boolean('crear');
            $table->boolean('editar');
            $table->boolean('borrar');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersroles_rolesformularios');
    }
}
