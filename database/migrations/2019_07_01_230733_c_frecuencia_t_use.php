<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CFrecuenciaTUse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servicio_recomendaciones_use', function (Blueprint $table) {
            $table->string('frecuencia')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servicio_recomendaciones_use', function (Blueprint $table) {
            $table->dropColumn('frecuencia');
        });
    }
}
