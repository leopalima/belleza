<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaServicioAlCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios_al_cliente', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->integer('satisfaccion')->nullable();
            $table->string('mensaje')->nullable();
            $table->date('fecha_recordatorio')->nullable();
            $table->char('profesional_id', 36);
            $table->foreign('profesional_id')
                        ->references('id')->on('users');
            $table->char('cliente_id', 36);
            $table->foreign('cliente_id')
                        ->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_al_cliente');
    }
}
