<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CFotoPrincipalTServiciosalcliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servicios_al_cliente', function (Blueprint $table) {
            $table->string('foto_principal')->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servicios_al_cliente', function (Blueprint $table) {
            $table->dropColumn("foto_principal");
        });
    }
}
