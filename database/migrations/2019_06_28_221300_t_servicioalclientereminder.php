<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TServicioalclientereminder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio_al_cliente_reminder', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('servicio_id', 36);
            $table->foreign('servicio_id')->references('id')->on('servicios_al_cliente');
            $table->char('cliente_id', 36);
            $table->foreign('cliente_id')->references('id')->on('users');
            $table->char('profesional_id', 36);
            $table->foreign('profesional_id')->references('id')->on('users');
            $table->string("mensaje");
            $table->timestamp("fecha");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicio_al_cliente_reminder');
    }
}
