<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaServicioAlClienteFormulaProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios_al_cliente_formula_productos', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->bigInteger('producto_id')->unsigned();
            $table->foreign('producto_id')
                    ->references('id')->on('producto');
            $table->integer('cantidad');
            $table->char('medida');
            $table->char('formula_id', 36);
            $table->foreign('formula_id')
                    ->references('id')->on('servicios_al_cliente_formula');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_al_cliente_formula_productos');
    }
}
