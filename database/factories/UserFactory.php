<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->defineAs(App\User::class, 'root', function (Faker $faker) {
    return [
        'name' => 'Super Usuario',
        'email' => 'root@root.com',
        'email_verified_at' => now(),
        'password' => bcrypt('1q2w3e4r'), // secret
        'remember_token' => str_random(10),
    ];
});

$factory->defineAs(App\User::class, 'admin', function (Faker $faker) {
    return [
        'name' => 'Usuario Administrador',
        'email' => 'admin@admin.com',
        'email_verified_at' => now(),
        'password' => bcrypt('1q2w3e4r'), // secret
        'remember_token' => str_random(10),
    ];
});

$factory->defineAs(App\User::class, 'cliente', function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => 'cliente@cliente.com',
        'email_verified_at' => now(),
        'password' => bcrypt('1q2w3e4r'), // password
        'remember_token' => Str::random(10),
    ];
});

$factory->defineAs(App\User::class, 'profesional', function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => 'profesional@profesional.com',
        'email_verified_at' => now(),
        'password' => bcrypt('1q2w3e4r'), // password
        'remember_token' => Str::random(10),
    ];
});
