<?php

use Faker\Generator as Faker;

$factory->define(App\Roles::class, function (Faker $faker) {
    return [
        'name' => 'root',
        'descripcion' => 'Super usuario',
    ];
});

$factory->defineAs(App\Roles::class, 'administrador',function (Faker $faker) {
    return [
        'name' => 'administrador',
        'descripcion' => 'Usuario administrador',
    ];
});

$factory->defineAs(App\Roles::class, 'cliente',function (Faker $faker) {
    return [
        'name' => 'cliente',
        'descripcion' => 'Cliente básico',
    ];
});

$factory->defineAs(App\Roles::class, 'profesional',function (Faker $faker) {
    return [
        'name' => 'Peluquero',
        'descripcion' => 'Los peluqueros lavan, acondicionan, secan, cortan, estilizan, hacen la permanente, alisan y tiñen el pelo. También pueden aconsejar a los clientes sobre productos para el pelo que se venden en el salón, realizar tareas de recepción y ocuparse de los pagos. Algunos peluqueros trabajan por su cuenta.',
    ];
});
