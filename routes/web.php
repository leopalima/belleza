<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*
|--------------------------------------------------------------------------
| Plantilla
|--------------------------------------------------------------------------
| Ruta para la plantilla y todos sus elementos
|
*/

Route::get('/plantilla', function () {
    return view('plantilla/index');
});

Auth::routes();

Auth::routes(['verify' => true]);

Route::get('/password', function(){
    return redirect()->route('password.update');
});
Route::get('/resetcomplete', function(){
    Auth::logout();
    return view('auth/resetcomplete');
});

/*
|--------------------------------------------------------------------------
| Rutas con autenticación
|--------------------------------------------------------------------------
| Rutas para usuarios logueados
|
*/

Route::get('signup/activate/{user}/code/{code}', 'SignupActivateController@activarCuentaApi');

Route::middleware(['auth'])->group(function(){
	/*
	| Dashboard
	*/
	// Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/', 'dashboard\dashboardController@index');
	Route::get('/home', 'dashboard\dashboardController@index')->name('home');

	/*
	| Roles
	*/
	Route::resource('/roles', 'roles\rolesController');
	Route::get('/roles/{role}/servicios', 'roles\serviciosController@index');
	Route::post('/roles/{role}/servicios', 'roles\serviciosController@store');

	/*
	| Menu
	*/
	Route::resource('/menu', 'menu\menuController');

	/*
	| Servicios
	*/
	Route::resource('/servicios', 'servicios\serviciosController');
	Route::post('/servicios/{id}', 'servicios\serviciosController@servicioDetalles');
	Route::get('/nuevos_servicios', 'servicios_al_cliente\servicios_al_clienteController@index')->name('servicios_al_cliente.index');
	Route::post('/nuevos_servicios/buscar', 'servicios_al_cliente\servicios_al_clienteController@buscar')->name('servicios_al_cliente.buscar');
	Route::post('/nuevos_servicios/mostrar', 'servicios_al_cliente\servicios_al_clienteController@mostrar')->name('servicios_al_cliente.mostrar');
	Route::post('/nuevos_servicios/cformula', 'servicios_al_cliente\servicios_al_clienteController@cformula')->name('servicios_al_cliente.cformula');
	Route::post('/nuevos_servicios/pformula', 'servicios_al_cliente\servicios_al_clienteController@pformula')->name('servicios_al_cliente.pformula');
	Route::post('/nuevos_servicios/hacer', 'servicios_al_cliente\servicios_al_clienteController@hacer')->name('servicios_al_cliente.hacer');
	Route::post('/nuevos_servicios/cuse', 'servicios_al_cliente\servicios_al_clienteController@cuse')->name('servicios_al_cliente.cuse');
	Route::post('/nuevos_servicios/puse', 'servicios_al_cliente\servicios_al_clienteController@puse')->name('servicios_al_cliente.puse');
	Route::post('/nuevos_servicios/recibir', 'servicios_al_cliente\servicios_al_clienteController@recibir')->name('servicios_al_cliente.recibir');

	// Route::get('/Hacer_nohacer', 'hacer_no_hacer\hacerNoHacerController@index')->name('hacer_no_hacer.index');
	// Route::post('/Hacer_nohacer', 'hacer_no_hacer\hacerNoHacerController@store')->name('hacer_no_hacer.store');
	// Route::get('/Hacer_nohacer/{id}', 'hacer_no_hacer\hacerNoHacerController@edit')->name('hacer_no_hacer.edit');
	// Route::post('/Hacer_nohacer/{id}', 'hacer_no_hacer\hacerNoHacerController@update')->name('hacer_no_hacer.update');
	// Route::get('/Hacer_nohacer/destroy/{id}', 'hacer_no_hacer\hacerNoHacerController@destroy')->name('hacer_no_hacer.destroy');

	Route::get('/do/{ye}', 'hacer_no_hacer\hacerNoHacerController@index')->name('hacer_no_hacer.index');
	Route::post('/do', 'hacer_no_hacer\hacerNoHacerController@store')->name('hacer_no_hacer.store');
	Route::get('/do /{id?}', 'hacer_no_hacer\hacerNoHacerController@edit')->name('hacer_no_hacer.edit');
	Route::post('/do /{id}', 'hacer_no_hacer\hacerNoHacerController@update')->name('hacer_no_hacer.update');
	Route::get('/do/destroy/{id}', 'hacer_no_hacer\hacerNoHacerController@destroy')->name('hacer_no_hacer.destroy');

	Route::get('/dont/{ye}', 'hacer_no_hacer\hacerNoHacerController@index')->name('nohacer_no_hacer.index');
	Route::post('/dont', 'hacer_no_hacer\hacerNoHacerController@store')->name('nohacer_no_hacer.store');
	Route::get('/dont/{id}', 'hacer_no_hacer\hacerNoHacerController@edit')->name('nohacer_no_hacer.edit');
	Route::post('/dont/{id}', 'hacer_no_hacer\hacerNoHacerController@update')->name('nohacer_no_hacer.update');
	Route::get('/dont/destroy/{id}', 'hacer_no_hacer\hacerNoHacerController@destroy')->name('nohacer_no_hacer.destroy');
	
	
	

	

	
	/*
	| Producto
	*/
	// Route::resource('/producto/marca', 'producto\marcaController');
	Route::resource('/marca', 'producto\marcaController');
	Route::get('/producto/marca/categoria/{id?}', 'producto\marcaController@marcaCategoria')->name('marca.categoria');
	// Route::resource('/producto/categoria', 'producto\categoriaController');
	Route::resource('/categoria', 'producto\categoriaController');
	Route::resource('/producto/producto', 'producto\productoController');
	Route::get('/importar/producto/', 'producto\importarController@index')->name('producto.importar');
	Route::post('/importar/producto/importar', 'producto\importarController@subirArchivo')->name('producto.subir.archivo');
	Route::get('/producto/exportar/formato', 'producto\exportarController@exportarFormatoModeloProducto')->name('producto.exportar.formato');
	Route::get('/producto/exportar/productos', 'producto\exportarController@exportarProductos')->name('producto.exportar.productos');

	/*
	| Mis categorias
	*/
	Route::get('/categorias/buscarProfesional', 'usuarios\CategoriasController@buscarProfesional')->name('categorias.buscarProfesional');
	Route::get('/categorias/usuario/{id?}', 'usuarios\CategoriasController@categoriasUsuario')->name('categorias.usuario');
	Route::post('/categorias/usuario/guardar', 'usuarios\CategoriasController@categoriasUsuarioGuardar')->name('categorias.usuario.guardar');
	Route::get('/categorias/listarCategorias', 'usuarios\CategoriasController@listarCategorias')->name('categorias.listarCategorias');
	Route::get('/miscategorias/usuario/{id}', 'usuarios\MisCategoriasController@index');
	Route::post('/miscategorias/usuario/{id}', 'usuarios\MisCategoriasController@modificarCategorias');
	Route::resource('/categorias', 'usuarios\CategoriasController');
	Route::post('/categorias/buscar', 'usuarios\CategoriasController@buscarCategorias')->name('categorias.buscar');

	/*
	| Administrar usuarios
	*/
	Route::resource('/usuarios', 'usuarios\UsuariosController');




});


