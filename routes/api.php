<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
| Ruta para iniciar sesión
*/
Route::post('login', 'API\AuthController@login');
Route::post('signup', 'API\AuthController@signup');

Route::put('usuario/editar/foto/{user_id}', 'API\UsersController@editarFoto');
Route::get('version/control/{so}', 'API\VersionController@muestraVersion');

/*
| Rutas que necesitan autorizacion
*/
Route::group(['middleware' => 'auth:api'], function() {

    /*
    | Ruta para desloguearse
    */
    Route::get('logout', 'API\AuthController@logout');

    /*
    | Ruta que trae información del usuario logueado.
    */
    Route::get('usuario', 'API\AuthController@usuario')->middleware('verified');

    Route::resource('categorias', 'API\CategoriasController');

    Route::post('profesional/busqueda', 'API\BuscarProfesionalController@buscar');
    Route::get('profesional/mostrar', 'API\BuscarProfesionalController@mostrar');
    
    Route::post('profesional/postlogin', 'API\ProfesionalController@postLogin');
    Route::resource('profesional', 'API\ProfesionalController');

    Route::get('discovery', 'API\DiscoveryController@discovery');
    Route::get('discovery/busqueda', 'API\DiscoveryController@busqueda');
    Route::resource('serviciocliente', 'API\ServiciosClientesController');
    
    Route::get('dodont', 'API\ServicioRecomendacionesDodontController@index');

    Route::post('clientes/busqueda', 'API\BuscarClienteController@buscar')->name('api.busqueda');
    Route::get('clientes/mostrar', 'API\BuscarClienteController@mostrar');
    
    Route::post('followup/seguir/{id}', 'API\FollowupController@seguir');
    Route::put('followup/dejarseguir/{id}', 'API\FollowupController@dejarseguir');


    Route::post('productos/marcas/buscar', 'API\ProductosController@buscarMarca');
    Route::post('productos/marcas/{marca_id}/categorias/buscar', 'API\ProductosController@buscarMarcaCategoria');
    Route::post('productos/marcas/categorias/{categoria_id}/producto/buscar', 'API\ProductosController@buscarMarcaCategoriaProducto');
    Route::post('productos/marcas/categorias/{categoria_id}/producto/buscarnumero', 'API\ProductosController@buscarMarcaCategoriaProductoNumero');

    Route::get('chats/activos', 'API\ChatController@chatsActivos');
    Route::get('chats/usuario/{user_id}', 'API\ChatController@chatUsuario');
    Route::get('chats/usuario/ultimomensaje/{user_id}', 'API\ChatController@chatUltimosMensajes');
    Route::post('chats/usuario/{user_id}', 'API\ChatController@chatUsuarioPost');
    Route::get('chats/obtener/reminders', 'API\ChatController@obtenerReminders');

    Route::put('usuario/editar', 'API\UsersController@update');
    Route::post('invitar/cliente', 'API\InvitarController@invitarcliente');
    
});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



// Route::post('clientes/busqueda', 'API\BuscarClienteController@buscar')->name('api.busqueda');
// Route::get('clientes/mostrar', 'API\BuscarClienteController@mostrar');

// Route::post('profesional/busqueda', 'API\BuscarProfesionalController@buscar');
// Route::get('profesional/mostrar', 'API\BuscarProfesionalController@mostrar');

// Route::post('productos/marcas/buscar', 'API\ProductosController@buscarMarca');
// Route::post('productos/marcas/{marca_id}/categorias/buscar', 'API\ProductosController@buscarMarcaCategoria');
// Route::post('productos/marcas/categorias/{categoria_id}/producto/buscar', 'API\ProductosController@buscarMarcaCategoriaProducto');
