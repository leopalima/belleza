<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categoria';

    protected $fillable = [
        'nombre', 'marca_id','user_id'
    ];

    public function marca(){
        return $this->hasOne('App\Marca', 'id', 'marca_id');
    }

    public function propietario(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getNombreAttribute($value){
        return strtoupper($value);
    }
}
