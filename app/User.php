<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Notifications\EmailResetPasswordNotification;
use Webpatser\Uuid\Uuid;
use App\UsersRoles;
use Auth;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $uuid = Uuid::generate()->string;
            $model->{$model->getKeyName()} = $uuid;
/*
            switch ($model->email) {
                case 'root@root.com':
                    $model->tipo = 'root';
                    break;
                case 'admin@admin.com':
                    $model->tipo = 'admin';
                    break;
                case 'profesional@profesional.com':
                    $model->tipo = 'profesional';
                    break;
                case 'cliente@cliente.com':
                    $model->tipo = 'cliente';
                    break;
                
                default:
                $model->tipo = 'cliente';
                    break;
            }
*/
        });

        static::created(function ($model) {
            $TIPO_USUARIO = 3;

            switch ($model->email) {
                case 'root@root.com':
                    $rol = new UsersRoles;
                    $rol->user_id = $model->id;
                    $rol->roles_id = 1;
                    $rol->save();
                    break;
                case 'admin@admin.com':
                    $rol = new UsersRoles;
                    $rol->user_id = $model->id;
                    $rol->roles_id = 2;
                    $rol->save();
                    break;
                case 'profesional@profesional.com':
                    $rol = new UsersRoles;
                    $rol->user_id = $model->id;
                    $rol->roles_id = 4;
                    $rol->save();
                    break;
                case 'cliente@cliente.com':
                    $rol = new UsersRoles;
                    $rol->user_id = $model->id;
                    $rol->roles_id = $TIPO_USUARIO;
                    $rol->save();
                    break;
                
                default:
                    $rol = new UsersRoles;
                    $rol->user_id = $model->id;
                    $rol->roles_id = $TIPO_USUARIO;
                    $rol->save();
                    break;
            }
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'location', 'phone', 'instagram', 'tipo', 'foto',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $incrementing = false;

    // public function formularios(){
    //     return $this->hasMany('App\Usuariosformularios', 'users_roles_id', 'id');
    // }

    /**
     * Campos personalizados.
     */
    protected $appends = ['foto_url', 'soyseguidor', 'mesigue', 'seguidores', 'seguidos', 'servicios'];

    public function getFotoUrlAttribute(){
        return $this->foto;
    }

    public function getSoyseguidorAttribute(){
        $soyseguidor = $this->estoySiguiendo()->where('user_id', Auth::user()->id)->where('estatus', 1)->count();
        return $soyseguidor != null ? $soyseguidor : 0;
    }

    public function getMesigueAttribute(){
        $mesigue = $this->meSigue()->where('seguido_id', Auth::user()->id)->where('estatus', 1)->count();
        return $mesigue != null ? $mesigue : 0;
    }

    public function getSeguidosAttribute()
    {
        $cantidad = $this->misSeguidos()->count();
        return $cantidad;
    }

    public function getSeguidoresAttribute()
    {
        $cantidad = $this->misSeguidores()->count();
        return $cantidad;
    }

    public function getServiciosAttribute()
    {
        $servicios = 0;
        switch ($this->tipo) {
            case 'profesional':
                $servicios = $this->hasMany('App\Servicios_al_cliente', 'profesional_id', 'id')->count();
                break;
            case 'cliente':
                $servicios = $this->hasMany('App\Servicios_al_cliente', 'cliente_id', 'id')->count();
                break;
            
            default:
                $servicios = 0;
                break;
        }
        return $servicios;
    }

    public function roles(){
        return $this->hasMany('App\UsersRoles', 'user_id', 'id');
    }

    public function miscategorias(){
        return $this->hasMany('App\Categorias_user', 'user_id', 'id');
    }

    public function categorias(){
        $categorias = $this->miscategorias;
        $catuser = [];
        foreach($this->miscategorias as $c){
            $catuser[] = [
                "id" => $c->categoria->id,
                "nombre" => $c->categoria->nombre,
                "formulacion" => $c->categoria->formulacion,
                "disenio" => $c->categoria->disenio,
                "icon_url" => $c->categoria->icon_url,
            ];
        }
        return $catuser;
    }

    public function estoySiguiendo(){
        return $this->hasOne('App\Followup', 'seguido_id', 'id');
    }

    public function meSigue(){
        return $this->hasOne('App\Followup', 'user_id', 'id');
    }

    public function misSeguidores(){
        return $this->hasMany('App\Followup', 'seguido_id', 'id');
    }

    public function misSeguidos(){
        return $this->hasMany('App\Followup', 'user_id', 'id');
    }

    public function misServicios(){

        switch (Auth::user()->tipo) {
            case 'profesional':
                $servicios = $this->hasMany('App\Servicios_al_cliente', 'cliente_id', 'id');
                break;
            case 'cliente':
                $servicios = $this->hasMany('App\Servicios_al_cliente', 'profesional_id', 'id');
                break;
            
            default:
                $servicios = [];
                break;
        }
        return $servicios;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new EmailResetPasswordNotification($token));
    }

}
