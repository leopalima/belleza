<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersRoles extends Model
{
    protected $table = 'users_roles';

    protected $fillable = [
        'user_id', 'roles_id',
    ];

    public function rolesFormulario(){
    	return $this->hasMany('App\RolesFormularios', 'roles_id', 'roles_id');
    }

    public function rolesMenu(){
    	return $this->hasMany('App\RolesMenus', 'roles_id', 'roles_id');
    }
}
