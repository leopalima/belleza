<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolesFormularios extends Model
{
    protected $table = 'roles_formularios';

    protected $fillable = [
        'roles_id', 'formularios_id',
    ];

    public function usuarioRolesRolesFormularios(){
    	return $this->hasMany('App\Usuariosformularios', 'roles_formularios_id', 'roles_id');
    }
}
