<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuariosmenus extends Model
{
    protected $table = 'usersroles_rolesmenu';

    protected $fillable = [
        'roles_menu_id', 'users_roles_id', 'visualizar',
    ];
}
