<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolesServicios extends Model
{
    protected $table = 'roles_servicios';

    protected $fillable = [
        'roles_id', 'servicios_id',
    ];

    public function servicios(){
    	return $this->hasOne('App\Servicios', 'id', 'servicios_id');
    }

    public function scopeActivo($q){
        $q->whereActivo(1);
    }
}
