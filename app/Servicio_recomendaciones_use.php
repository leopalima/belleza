<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio_recomendaciones_use extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cantidad', 'medida', 'producto_id', 'recomendaciones_id',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'servicio_recomendaciones_use';

    /**
     * Muestra el Uuid correctamente.
    */
    public $incrementing = false;

    public function producto(){
        return $this->hasOne('App\Producto', 'id', 'producto_id');
    }
}
