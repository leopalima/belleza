<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio_recomendaciones_dodont extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo', 'tarea', 'recomendaciones_id',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'servicio_recomendaciones_dodont';

    /**
     * Muestra el Uuid correctamente.
    */
    public $incrementing = false;

}
