<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chat';

    protected $fillable = [
        'de_user_id', 'para_user_id','mensaje', 'tipo_mensaje', 'identificador',
    ];

    public function chatcon()
    {
        return $this->hasOne('App\User', 'id', 'para_user_id');
    }

    public function chatde()
    {
        return $this->hasOne('App\User', 'id', 'de_user_id');
    }

}
