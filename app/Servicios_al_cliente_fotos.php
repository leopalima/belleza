<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicios_al_cliente_fotos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'perfil', 'url_fotos', 'servicio_id',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'servicios_al_cliente_fotos';

    /**
     * Muestra el Uuid correctamente.
    */
    public $incrementing = false;

}
