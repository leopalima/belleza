<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;
use App\User;

class ExportarFormatoModeloProducto implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // return User::where('id', '=', 0)->get();
        $coleccion = new Collection;
        return $coleccion;
    }

    
    public function headings(): array
    {
        return [
            'Id',
            'Marca',
            'Categoria',
            'Producto',
            'Descripcion',
            'Medida',
            'Numero',
            'Tipo_uso',
        ];
    }
    
}
