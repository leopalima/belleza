<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Support\Collection;
use App\Producto;
use Auth;

class ExportarProductos implements FromQuery, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        $coleccion = Producto::where('user_id', Auth::user()->id);
        return $coleccion;
    }

    public function map($m): array
    {
        return [
            $m->id,
            $m->categoria->marca->nombre,
            $m->categoria->nombre,
            $m->nombre,
            $m->descripcion,
            $m->medida,
            $m->numero,
            $m->tipo,
        ];
    }

    public function headings(): array
    {
        return [
            'Id',
            'Marca',
            'Categoria',
            'Producto',
            'Descripcion',
            'Medida',
            'Numero',
            'Tipo_uso',
        ];
    }

}
