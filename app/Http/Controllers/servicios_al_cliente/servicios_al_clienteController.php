<?php

namespace App\Http\Controllers\servicios_al_cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Servicios_al_cliente as SC;
use Auth;
use Webpatser\Uuid\Uuid;
use App\Servicios_al_cliente_formula as SCF;
use App\Servicios_al_cliente_formula_productos as SCFP;
use App\Servicios_al_cliente_recomendaciones as SCR;
use App\Servicio_recomendaciones_dodont as SRD;
use App\Servicio_recomendaciones_use as SRU;
use App\hacer_no_hacer;

class servicios_al_clienteController extends Controller
{
    public function index(Request $r)
    {
        
        $marca_list = DB::table('marca')
         ->get();
        return view('servicios_al_cliente.index')->with('marca_list', $marca_list);
        
    }
    //step-1
        function buscar(Request $request)
            {
                
             if($request->get('query'))
             {
                $output='';
                $query = $request->get('query');
                    $data = DB::table('users')
                    ->where('name', 'like', '%' . $request['query'] . '%')
                    ->where('tipo', 'cliente')
                    ->get();

                $output = '<ul class="dropdown-menu" style="display:block; position:relative">';

                    if(count($data))
                    {
                        foreach($data as $row)
                        {
                            $output .= '
                            <li class="usuariosclientes"><a href="#">'.$row->name.'</a></li>
                            ';
                        }

                    }else{

                            $output .= '
                            <li><a href="#">No existe ese cliente</a></li>
                            ';

                    }

                $output .= '</ul>';
                echo $output;
             }

        }

        function mostrar(Request $request)
            {
            // http_response_code(500);
               // dd($request);
            if($request->get('mostrar'))
            {
               $nombre='';
               $mostrar = $request->get('mostrar');
                   $data = DB::table('users')
                   ->where('name', 'like', $request['mostrar'] )
                   ->get();
            
               //     foreach($data as $row)
               //     {
               //         $nombre->name=$row->name;
            
               //     }

            
                   return $data;
                
            }
        
        }
    //step-1


    //step-2
        //categoria
            function cformula(Request $request)
                {   



                 $select = $request->get('select');
                 $value = $request->get('value');
                 $dependent = $request->get('dependent');
                 $data = DB::table('categoria')
                   ->where('marca_id', '=', $value)
                   ->get();

                 $output = '<option value="">Seleccione categoría</option>';
                 foreach($data as $row)
                 {
                  $output .= '<option value="'.$row->id.'">'.$row->nombre.'</option>';
                 }
             
                 echo $output;
            }
        //categoria

        //producto
            function pformula(Request $request)
                {   


                 $select = $request->get('select');
                 $value = $request->get('value');
                 $dependent = $request->get('dependent');
                 $data = DB::table('producto')
                   ->where('categoria_id', '=', $value)
                   ->where('tipo', '=', 'Local')
                   ->get();
 
                 $output = '<option value="">Seleccione producto</option>';
                 foreach($data as $row)
                 {
                  $output .= '<option id="copyproducto" name="'.$row->id.'" value="'.$row->nombre.'">'.$row->nombre.'</option>';
                 }
             
                 echo $output;
            }
        //producto

    //step-2


    //step-3
        //hacer
            function hacer(Request $request)
                {   

                 $select = $request->get('select');
                 $value = $request->get('value');
                 $dependent = $request->get('dependent');
                 $data = DB::table('hacer')
                   ->where('tipo', '=', $value)
                   ->get();

                 $output = '<option value="">Seleccione categoría</option>';
                 foreach($data as $row)
                 {
                  $output .= '<option value="'.$row->descripcion.'">'.$row->descripcion.'</option>';
                 }
             
                 echo $output;
            }
        //hacer


    //step-3


    //step-4

        //categoria
            function cuse(Request $request)
            {   

             $select = $request->get('select');
             $value = $request->get('value');
             $dependent = $request->get('dependent');
             $data = DB::table('categoria')
               ->where('marca_id', '=', $value)
               ->get();
           
             $output = '<option value="">Seleccione categoría</option>';
             foreach($data as $row)
             {
              $output .= '<option value="'.$row->id.'">'.$row->nombre.'</option>';
             }
         
             echo $output;
            }
        //categoria

        //producto
            function puse(Request $request)
                {   
            

                 $select = $request->get('select');
                 $value = $request->get('value');
                 $dependent = $request->get('dependent');
                 $data = DB::table('producto')
                   ->where('categoria_id', '=', $value)
                   ->where('tipo', '=', 'Cliente')
                   ->get();
                   
                 $output = '<option value="">Seleccione producto</option>';
                 foreach($data as $row)
                 {
                  $output .= '<option id="usecopyproducto" name="'.$row->id.'" value="'.$row->nombre.'">'.$row->nombre.'</option>';
                 }
             
                 echo $output;
            }
        //producto


    //step-4
    
    public function recibir(Request $r )
    {

                    // http_response_code(500);
               // dd($request);

        $servicio=new SC;
        $servicio->id=Uuid::generate()->string;
        $servicio->satisfaccion=$r->selec_satisfaccion;
        $servicio->mensaje=$r->mensaje;
        $servicio->cliente_id=$r->nombreid;
        $servicio->profesional_id=$this->profesional()->id;
        $servicio->fecha_recordatorio=now();
        $servicio->save();

        $formula=new SCF;
        $formula->name=$r->formulanombre;
        $formula->id=Uuid::generate()->string;
        $formula->descripcion_proceso=$r->Descripcion;
        $formula->servicio_id=$servicio->id;
        $formula->save();

        for($i=0 ; $i < count($r->formulatipo);$i++)
        {
            $fproducto=new SCFP;
            $fproducto->id=Uuid::generate()->string;
            $fproducto->producto_id=$r->productoid[$i];
            $fproducto->cantidad=$r->formulacantidad[$i];
            $fproducto->medida=$r->formulatipo[$i];
            $fproducto->formula_id=$formula->id;
            $fproducto->save();
        }

        $recomendacion=new SCR;
        $recomendacion->id=Uuid::generate()->string;
        $recomendacion->servicio_id=$servicio->id;
        $recomendacion->save();

        for($i=0 ; $i < count($r->hacertipo);$i++)
        {
            $hacer=new SRD;
            $hacer->id=Uuid::generate()->string;
            $hacer->tipo=$r->hacertipo[$i];
            $hacer->tarea=$r->quehacerdetalles[$i];
            $hacer->recomendaciones_id=$recomendacion->id;
            $hacer->save();
        }

        for($i=0 ; $i < count($r->tipousorecomendado);$i++)
        {
            $use=new SRU;
            $use->id=Uuid::generate()->string;
            $use->producto_id=$r->iduseproducto[$i];
            $use->cantidad=$r->usorecomendado[$i];
            $use->medida=$r->tipousorecomendado[$i];
            $use->recomendaciones_id=$recomendacion->id;
            $use->save();

        }

            

            return redirect()->route('servicios_al_cliente.index');

            
    }

    public function profesional(){

        return Auth::user();
    }

}
