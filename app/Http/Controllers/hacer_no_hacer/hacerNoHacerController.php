<?php

namespace App\Http\Controllers\hacer_no_hacer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\hacer_no_hacer;
use Validator;
use Auth;

class hacerNoHacerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($ye)
    {
        switch ($ye) {
            case 'do':
                $dodont = "hacer";
                break;
            case 'dont':
                $dodont = "no hacer";
                break;
            
            default:
                $dodont = "hacer";
                break;
        }
        $hacer = hacer_no_hacer::where('user_id', $this->usuario()->id)
                                ->where('tipo', $dodont)
                                ->where('estado','=','activo')
                                ->paginate(15);
        // $hacer=hacer_no_hacer::all()->where('estado','=','activo')->paginate(15);
        return view('hacer_no_hacer.index',compact('hacer', 'dodont'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
                   // http_response_code(500);
               // dd($nombre);

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
   


        $rules = array(
            'tipohacer'    =>  'required',
            'hacerdescripcion'     =>  'required',
            
        );

        $error = Validator::make($request->all(), $rules);


        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        

        $form_data = array(
            'tipo'        =>  $request->tipohacer,
            'descripcion'         =>  $request->hacerdescripcion,
            'user_id' => $this->usuario()->id,
            'estado' => 'activo',
        );

        hacer_no_hacer::create($form_data);

        return response()->json(['success' => 'Los datos fueron guardados.']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // http_response_code(500);
        // dd($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        if(request()->ajax())
        {
            
            $data = hacer_no_hacer::where('estado','=','activo')->findOrFail($id);
            return response()->json(['data' => $data]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , $id)
    {


        
        $rules = array(
            'tipohacer'    =>  'required',
            'hacerdescripcion'     =>  'required',
            
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        

        $form_data = array(
            'tipo'        =>  $request->tipohacer,
            'descripcion'         =>  $request->hacerdescripcion,
            'user_id' => $this->usuario()->id,
        );

        hacer_no_hacer::whereId($request->hidden_id)->update($form_data);

        return response()->json(['success' => 'Los cambios fueron guardados.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        
        $data = hacer_no_hacer::findOrFail($id);
        $data->estado = 'inactivo';
        $data->update();

    }

    public function usuario(){

        return Auth::user();
    }

}
