<?php

namespace App\Http\Controllers\servicios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Servicios;
use App\Servicios_al_cliente;
use App\Servicios_al_cliente_formula;
use Auth;
use App\Servicios_al_cliente_formula_productos;

class serviciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $servicios = $this->servicios()->paginate(10);
        $servicios =Servicios_al_cliente::all();
       
        return view('servicios.index', compact('servicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('servicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
            ],
            [
                'name.required' => 'Indique el nombre del servicio',
                'name.min' => 'Mínimo tres caracteres para el nombre',
            ]
        );

        if ($validator->fails()) {
            return redirect('servicios/create')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $servicio = new Servicios;
        $servicio->name = $request->name;
        $servicio->descripcion = $request->descripcion;
        $servicio->save();

        return redirect('servicios/create')->with('creado', 'Nuevo servicios creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = $this->servicios($id)->first();
        return view('servicios.edit', compact('servicio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
            ],
            [
                'name.required' => 'Indique el nombre del servicio',
                'name.min' => 'Mínimo tres caracteres para el nombre',
            ]
        );

        if ($validator->fails()) {
            return redirect('servicios/'. $id .'/edit')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $servicio = $this->servicios($id)->first();
        $servicio->name = $request->name;
        $servicio->descripcion = $request->descripcion;
        $servicio->save();

        return redirect('servicios/'. $id .'/edit')->with('actualizado', 'Servicio actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function servicios($id = ''){
        if($id == ''){
            $servicios = Servicios::orderBy('id', 'desc');
        }else
        {
            $servicios = Servicios::where('id', $id);
        }

        return $servicios;
    }

    public function servicioDetalles(Request $r){

        $servicios =Servicios_al_cliente::with(['formula.productos.producto', 'recomendaciones.dodont', 'recomendaciones.use.producto', 'cliente', 'profesional'])->findOrFail($r->id);

        // $servicios =Servicios_al_cliente_formula::all()->where('servicio_id','=',$r->id);
        // $formula='';
        // foreach($servicios as $s)
        // {
        //     $formula=$s->id;
        // }

        // $formula_producto =Servicios_al_cliente_formula_productos::all()->where('formula_id','=',$formula);
        

        // http_response_code(500);
        // dd($servicios );
  
        // $compra = Servicios_al_cliente::with(['cliente', 'detalles.presentacion.obra', 'detalles.producto'])->findOrFail($r->id);
        return response()->json($servicios);
        
    }

}
