<?php

namespace App\Http\Controllers\usuarios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categorias;
use App\Categorias_user;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias= Categorias::all();
        return view('categorias.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $categoria = new Categorias;
            $categoria->nombre = $r->nombre;
            $r->has('formulacion') ? $categoria->formulacion = 1 : $categoria->formulacion = 0;
            $r->has('disenio') ? $categoria->disenio = 1 : $categoria->disenio = 0;

            if($r->file('icon_url')){
                $imagen = $r->file('icon_url');          
    
                $nombreImagen = Str::uuid() . "." . $r->icon_url->extension();
                $imagen->originalName = $nombreImagen;
    
                if (!Storage::disk('public')->exists('categorias')) {
                  Storage::disk('public')->makeDirectory('categorias');
                }
    
                Storage::disk('public')->put('categorias/' . $nombreImagen, file_get_contents($imagen));
    
                $categoria->icon_url = url('storage/categorias') . '/' . $nombreImagen;
            }

            $categoria->save();
            return ["exito" => 1];
        } catch (\Throwable $th) {
            return ["error" => $th];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $categoria = Categorias::find($r->id);
        $categoria->nombre = $r->nombre;
        $r->has('formulacion') ? $categoria->formulacion = 1 : $categoria->formulacion = 0;
        $r->has('disenio') ? $categoria->disenio = 1 : $categoria->disenio = 0;

        if($r->file('icon_url')){
            $imagen = $r->file('icon_url');          

            $nombreImagen = Str::uuid() . "." . $r->icon_url->extension();
            $imagen->originalName = $nombreImagen;

            if (!Storage::disk('public')->exists('categorias')) {
              Storage::disk('public')->makeDirectory('categorias');
            }

            Storage::disk('public')->put('categorias/' . $nombreImagen, file_get_contents($imagen));

            $categoria->icon_url = url('storage/categorias') . '/' . $nombreImagen;
        }

        $categoria->save();
        return response()->json($categoria->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function buscarCategorias(Request $r){
        $categoria = Categorias::find($r->data);
        $categoria->toArray();
        return response()->json($categoria);
    }

    public function buscarProfesional(Request $r){
        // $usuarios = User::select('name')->where('tipo', 'profesional')->get();
        $usuarios = User::select('name', 'id')
                    ->where('tipo', 'profesional')
                    ->where('name', 'like', '%' . $r->term .'%')
                    ->get();
        
        $respuesta = [];

        if(count($usuarios)){
            foreach($usuarios as $u => $v){
                $respuesta[] = [
                    "label" => $v->name,
                    "value" => $v->id,
                ];
            }
        }

        return response()->json($respuesta);
    }

    public function listarCategorias(){
        $categorias= Categorias::all();
        return response()->json($categorias);
    }

    public function categoriasUsuario($id){
        $categorias = Categorias_user::select('categoria_id')->where('user_id', $id)->get();
        if($categorias)
        return response()->json($categorias);
        
        $categorias = [];
        return response()->json($categorias);
    }

    public function categoriasUsuarioGuardar(Request $r){
        Categorias_user::where('user_id', $r->prof_id)->delete();

        try {
            $categorias = Categorias::find($r->categorias);
        } catch (\Throwable $th) {
            // return response()->json("Hubo un error");
        }

        try {
            if(count($categorias)){
                foreach($categorias as $c){
                    $cat_user = new Categorias_user;
                    $cat_user->user_id = $r->prof_id;
                    $cat_user->categoria_id = $c->id;
                    $cat_user->save();
                }
            }
            return response()->json(["exito" => 1]);
        } catch (\Throwable $th) {
            //throw $th;
        }
        return response()->json(["error" => 0]);
    }


}
