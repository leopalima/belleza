<?php

namespace App\Http\Controllers\usuarios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Categorias;
use App\Categorias_user;
use Auth;

class MisCategoriasController extends Controller
{
    public function index($id){
        $categorias = Categorias::all();
        // return response()->json(Auth::user()->miscategorias);
        return view('usuarios.miscategorias', compact('categorias'));
    }

    public function modificarCategorias(Request $r, $id){
        Categorias_user::where('user_id', Auth::user()->id)->delete();

        try {
            $categorias = Categorias::find($r->categorias);
        } catch (\Throwable $th) {
            // return response()->json("Hubo un error");
        }

        if(count($categorias)){
            foreach($categorias as $c){
                $cat_user = new Categorias_user;
                $cat_user->user_id = $id;
                $cat_user->categoria_id = $c->id;
                $cat_user->save();
            }
        }

        return $this->index($id);
    }
}
