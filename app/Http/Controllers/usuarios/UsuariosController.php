<?php

namespace App\Http\Controllers\usuarios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Categorias;
use App\Categorias_user;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::where('tipo', '!=', 'root')->paginate(15);
        $categorias = Categorias::all();

        return view('usuarios.index', compact('usuarios', 'categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::where('id', $id)
                    ->with("miscategorias")
                    ->first();
        return response()->json($usuario);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        try {
            $usuario = User::where('id', $id)->first();
            $usuario->name = $r->enombre;
            $usuario->email = $r->eemail;
            $usuario->phone = $r->ephone;
            $usuario->instagram = $r->einstagram;
            $usuario->tipo = $r->etipo;
            $usuario->save();
            
            Categorias_user::where('user_id', $id)->delete();
            
            if($r->has("ecategorias")){
                if(count($r->ecategorias)){
                    foreach($r->ecategorias as $c){
                        $categorias = new Categorias_user;
                        $categorias->user_id = $id;
                        $categorias->categoria_id = $c;
                        $categorias->save();
                    }
                }

            }
            return ["exito" => 1];
        } catch (\Throwable $th) {
            //throw $th;
        }

        return ["error" => 0];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
