<?php

namespace App\Http\Controllers\producto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Imports\importarProducto;
use Maatwebsite\Excel\Facades\Excel;

class importarController extends Controller
{
    public function index(){
        return view('importar.index');
    }

    public function subirArchivo(Request $request){

        if(!$request->hasFile('archivo')){
            return response()->json("Archivo no seleccionado");
        }

        try{
            Excel::import(new importarProducto, $request->archivo);
        }catch(\Exception $e){
           
            return response()->json(['error'=>$e]);
        }
        
        return response()->json('completado');
    }
}
