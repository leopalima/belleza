<?php

namespace App\Http\Controllers\producto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\producto\marcaController as Marca;
use Validator;
use App\Categoria;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class categoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = $this->categoria()->paginate(10);
        return view('categoria.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $marca = new Marca;
        $marcas = $marca->marca()->get();
        return view('categoria.create', compact('marcas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'nombre' => 'required|min:3',
            ],
            [
                'nombre.required' => 'Indique el nombre de la categoria',
                'nombre.min' => 'Mínimo tres caracteres para el nombre',
            ]
        );

        if ($validator->fails()) {
            return redirect('producto/categoria/create')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $categoria = new Categoria;
        $categoria->user_id= $this->usuario()->id;
        $categoria->nombre = $request->nombre;
        $categoria->marca_id = $request->marca;
        if($request->file('url_foto')){
            $imagen = $request->file('url_foto');          
    
                $nombreImagen = Str::uuid() . "." . $request->url_foto->extension();
                $imagen->originalName = $nombreImagen;
    
                if (!Storage::disk('public')->exists('categorias')) {
                  Storage::disk('public')->makeDirectory('categorias');
                }
    
                Storage::disk('public')->put('categorias/' . $nombreImagen, file_get_contents($imagen));
    
                $categoria->url_foto = url('storage/categorias') . '/' . $nombreImagen;
        }
        $categoria->save();

        return redirect()->route('categoria.create')->with('creado', 'Nueva categoria creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria = $this->categoria($id)->first();
        $marca = new Marca;
        $marcas = $marca->marca()->get();
        return view('categoria.edit', compact('categoria', 'marcas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
                'nombre' => 'required|min:3',
            ],
            [
                'nombre.required' => 'Indique el nombre de la categoria',
                'nombre.min' => 'Mínimo tres caracteres para el nombre',
            ]
        );

        if ($validator->fails()) {
            return redirect('producto/categoria/'. $id .'/edit')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $categoria = $this->categoria($id)->first();
        $categoria->nombre = $request->nombre;
        $categoria->marca_id = $request->marca;
        if($request->file('url_foto')){
            $imagen = $request->file('url_foto');          
    
                $nombreImagen = Str::uuid() . "." . $request->url_foto->extension();
                $imagen->originalName = $nombreImagen;
    
                if (!Storage::disk('public')->exists('categorias')) {
                  Storage::disk('public')->makeDirectory('categorias');
                }
    
                Storage::disk('public')->put('categorias/' . $nombreImagen, file_get_contents($imagen));
    
                $categoria->url_foto = url('storage/categorias') . '/' . $nombreImagen;
        }
        $categoria->save();

        return redirect()->route('categoria.edit', ['id' => $id])->with('actualizado', 'La categoria se ha actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function categoria($id=''){
        if($id == ''){
            $categoria = Categoria::orderBy('id', 'desc');
        }else
        {
            $categoria = Categoria::where('id', $id);
        }

        return $categoria;

    }
    public function usuario(){

        return Auth::user();
    }
}
