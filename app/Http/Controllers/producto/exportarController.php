<?php

namespace App\Http\Controllers\producto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExportarFormatoModeloProducto;
use App\Exports\ExportarProductos;

class exportarController extends Controller
{
    public function exportarFormatoModeloProducto()
    {
        return Excel::download(new ExportarFormatoModeloProducto, 'formato_importar_productos.xlsx');
    }

    public function exportarProductos()
    {
        return Excel::download(new ExportarProductos, 'todos_productos.xlsx');
    }
}
