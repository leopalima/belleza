<?php

namespace App\Http\Controllers\producto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Marca;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class marcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marcas = $this->marca()->paginate(10);
        return view('marca.index', compact('marcas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('marca.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'nombre' => 'required|min:3',
            ],
            [
                'nombre.required' => 'Indique el nombre de la marca',
                'nombre.min' => 'Mínimo tres caracteres para el nombre',
            ]
        );

        if ($validator->fails()) {
            return redirect('producto/marca/create')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $marca = new Marca;
        $marca->user_id=$this->usuario()->id;
        $marca->nombre = $request->nombre;
        if($request->file('url_foto')){
            $imagen = $request->file('url_foto');          
    
                $nombreImagen = Str::uuid() . "." . $request->url_foto->extension();
                $imagen->originalName = $nombreImagen;
    
                if (!Storage::disk('public')->exists('marcas')) {
                  Storage::disk('public')->makeDirectory('marcas');
                }
    
                Storage::disk('public')->put('marcas/' . $nombreImagen, file_get_contents($imagen));
    
                $marca->url_foto = url('storage/marcas') . '/' . $nombreImagen;
        }
        $marca->save();

        return redirect()->route('marca.create')->with('creado', 'Nueva marca creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marca = $this->marca($id)->first();
        return view('marca.edit', compact('marca'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
                'nombre' => 'required|min:3',
            ],
            [
                'nombre.required' => 'Indique el nombre de la marca',
                'nombre.min' => 'Mínimo tres caracteres para el nombre',
            ]
        );

        if ($validator->fails()) {
            return redirect('producto/marca/'. $id .'/edit')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $marca = $this->marca($id)->first();
        $marca->nombre = $request->nombre;
        if($request->file('url_foto')){
            $imagen = $request->file('url_foto');          
    
                $nombreImagen = Str::uuid() . "." . $request->url_foto->extension();
                $imagen->originalName = $nombreImagen;
    
                if (!Storage::disk('public')->exists('marcas')) {
                  Storage::disk('public')->makeDirectory('marcas');
                }
    
                Storage::disk('public')->put('marcas/' . $nombreImagen, file_get_contents($imagen));
    
                $marca->url_foto = url('storage/marcas') . '/' . $nombreImagen;
        }
        $marca->save();

        return redirect()->route('marca.edit', ['id' => $id])->with('actualizado', 'La marca se ha actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function marca($id=''){
        if($id == ''){
            $marca = Marca::orderBy('id', 'desc');
        }else
        {
            $marca = Marca::where('id', $id);
        }

        return $marca;

    }

    public function marcaCategoria($id){
        $marca = $this->marca($id)->get();
        foreach($marca as $marcac){
            $categoria[] = $marcac->categoria;
        }
        return $categoria;
    }

    public function usuario(){

        return Auth::user();
    }
}
