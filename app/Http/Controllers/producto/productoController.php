<?php

namespace App\Http\Controllers\producto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\producto\marcaController as Marca;
use App\Http\Controllers\producto\categoriaController as Categoria;
use Validator;
use App\Producto;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class productoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        switch ($r->u) {
            case 'cliente':
                $uso = 'cliente';
                break;
            case 'local':
                $uso = 'local';
                break;
            default:
                $uso = 'local';
                break;
        }

        if($this->usuario()->tipo === "root" || $this->usuario()->tipo === "admin"){
            if($r->has('q')){
                $productos = $this->producto()
                                ->where('user_id', $this->usuario()->id)
                                ->where(function($q) use ($r){
                                    $q->orWhere('nombre', 'like', '%' . $r->q . '%')
                                    ->orWhere('medida', 'like', '%' . $r->q . '%')
                                    ->orWhere('numero', 'like', '%' . $r->q . '%')
                                    ->orWhere('descripcion', 'like', '%' . $r->q . '%')
                                    ->orWhere('tipo', 'like', '%' . $r->q . '%')
                                    ->orWhereHas('categoria', function($q) use ($r){
                                        $q->where('nombre', 'like', '%' . $r->q . '%');
                                    })
                                    ->orWhereHas('categoria.marca', function($q) use ($r){
                                        $q->where('nombre', 'like', '%' . $r->q . '%');
                                    });
                                })
                                ->paginate(15);
                $productos->appends([
                    'q' => $r->q,
                    'u' => $uso,
                ]);

            }else{
                $productos = $this->producto()
                                ->where('user_id', $this->usuario()->id)
                                ->where('tipo', 'like', '%'. $uso .'%')
                                ->paginate(15);
                $productos->appends([
                    'u' => $uso,
                ]);
            }
        }else{
            if($r->has('q')){
                $productos = $this->producto()
                                ->where('user_id', $this->usuario()->id)
                                ->where(function($q) use ($r){
                                    $q->orWhere('nombre', 'like', '%' . $r->q . '%')
                                    ->orWhere('medida', 'like', '%' . $r->q . '%')
                                    ->orWhere('numero', 'like', '%' . $r->q . '%')
                                    ->orWhere('tipo', 'like', '%' . $r->q . '%')
                                    ->orWhereHas('categoria', function($q) use ($r){
                                        $q->where('nombre', 'like', '%' . $r->q . '%');
                                    })
                                    ->orWhereHas('categoria.marca', function($q) use ($r){
                                        $q->where('nombre', 'like', '%' . $r->q . '%');
                                    });
                                })
                                ->paginate(15);
                $productos->appends([
                    'q' => $r->q,
                    'u' => $uso,
                ]);
            }else{
                $productos = $this->producto()
                                ->where('user_id', $this->usuario()->id)
                                ->where('tipo', 'like', '%'. $uso .'%')
                                ->paginate(15);
                $productos->appends([
                    'u' => $uso,
                ]);
            }
        }

        return view('producto.index', compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $marca = new Marca;
        $marcas = $marca->marca()->get();
        $cantidadMarcas= count($marcas);
        // dd($marcas);
        if(count($marcas) != 0)
        {
        $categoria = new Categoria;
        $categorias = $categoria->categoria($marcas[$cantidadMarcas-1]->id)->get();
        }else{

            $categorias=[];
        }
        // dd($categorias);
        return view('producto.create', compact('marcas', 'categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'nombre' => 'required|min:3',
                'medida' => 'required',
                'categorias'=>'required',
            ],
            [
                'nombre.required' => 'Indique el nombre del producto',
                'nombre.min' => 'Mínimo tres caracteres para el nombre',
                'medida.required' => 'Indique la medida',
                'categorias.required' => 'Indique la categoria',
            ]
        );

        if ($validator->fails()) {
            return redirect('producto/producto/create')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $producto = new Producto;
        $producto->user_id= $this->usuario()->id;
        $producto->nombre = $request->nombre;
        $producto->medida = $request->medida;
        $producto->numero = ($request->numero == "") ? 0 : $request->numero;
        $producto->tipo = $request->Tipo_uso;
        $producto->descripcion = $request->descripcion;
        $producto->categoria_id = $request->categorias;
        if($request->file('url_foto')){
            $imagen = $request->file('url_foto');          
    
                $nombreImagen = Str::uuid() . "." . $request->url_foto->extension();
                $imagen->originalName = $nombreImagen;
    
                if (!Storage::disk('public')->exists('productos')) {
                  Storage::disk('public')->makeDirectory('productos');
                }
    
                Storage::disk('public')->put('productos/' . $nombreImagen, file_get_contents($imagen));
    
                $producto->url_foto = url('storage/productos') . '/' . $nombreImagen;
        }
        $producto->save();

        return redirect('producto/producto/create')->with('creado', 'El producto se ha creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = $this->producto($id)->first();
        $marca = new Marca;
        $marcas = $marca->marca()->get();
        $categoria = new Categoria;
        $categorias = $categoria->categoria($producto->categoria_id)->get();
        return view('producto.edit', compact('producto', 'marcas', 'categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
                'nombre' => 'required|min:3',
                'medida' => 'required',
            ],
            [
                'nombre.required' => 'Indique el nombre del producto',
                'nombre.min' => 'Mínimo tres caracteres para el nombre',
                'medida.required' => 'Indique la medida',
            ]
        );

        if ($validator->fails()) {
            return redirect('producto/producto/'. $id .'/edit')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $producto = $this->producto($id)->first();
        $producto->nombre = $request->nombre;
        $producto->medida = $request->medida;
        $producto->numero = ($request->numero == "") ? 0 : $request->numero;
        $producto->tipo = $request->Tipo_uso;
        $producto->descripcion = $request->descripcion;
        $producto->categoria_id = $request->categorias;
        if($request->file('url_foto')){
            $imagen = $request->file('url_foto');          
    
                $nombreImagen = Str::uuid() . "." . $request->url_foto->extension();
                $imagen->originalName = $nombreImagen;
    
                if (!Storage::disk('public')->exists('productos')) {
                  Storage::disk('public')->makeDirectory('productos');
                }
    
                Storage::disk('public')->put('productos/' . $nombreImagen, file_get_contents($imagen));
    
                $producto->url_foto = url('storage/productos') . '/' . $nombreImagen;
        }
        $producto->save();

        return redirect('producto/producto/'. $id .'/edit')->with('actualizado', 'El producto se ha actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function producto($id=''){
        if($id == ''){
            $producto = Producto::orderBy('id', 'desc');
        }else
        {
            $producto = Producto::where('id', $id);
        }

        return $producto;

    }

    public function usuario(){

        return Auth::user();
    }
}
