<?php

namespace App\Http\Controllers;

use App\Notifications\ConfirmedArtista;
use App\Notifications\ConfirmedClient;
use Illuminate\Http\Request;
use App\User;

class SignupActivateController extends Controller
{
    public function activarCuentaApi($user, $code){
        
        $codigo = now()->format('Ymd');

        $respuesta = array(
            "username" => "",
            "mensaje" => "",
        );

        try {
            if($code == $codigo){
                $usuario = User::where('id', $user)->firstOrFail();
                $respuesta['username'] = $usuario->name;
                $usuario->email_verified_at = now();
                $usuario->save();
                $respuesta['mensaje'] = "Your email has been registered";

                try {
                    switch ($usuario->tipo) {
                        case 'cliente':
                            $usuario->notify(new ConfirmedClient($usuario));
                            break;
                        case 'profesional':
                            $usuario->notify(new ConfirmedArtista($usuario));
                            break;
                        default:
                            break;
                    }
                } catch (\Exception $e) {}

            }else{
                $respuesta['mensaje'] = "El codigo ha expirado, solicita una nueva confirmación desde la app.";
            }
        } catch (\Exception $e) {
            $respuesta['mensaje'] = "Usuario no encontrado.";
        }

        return view('layout.signupactivate', compact('respuesta'));

    }
}
