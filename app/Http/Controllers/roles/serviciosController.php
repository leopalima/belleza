<?php

namespace App\Http\Controllers\roles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\roles\rolesController;
use App\Http\Controllers\servicios\serviciosController as Serv;
use App\RolesServicios;


class serviciosController extends Controller
{
    public function index($id){

    	$rol = new rolesController;
    	$rol = $rol->roles($id)->first();

    	$servicios = new Serv;
    	$servicios = $servicios->servicios()->get();

    	$serviciosAsociados = [];
    	foreach($rol->servicios as $servicio){
			$serviciosAsociados[] = $servicio->servicios->id;
		}

    	
    	return view('roles.servicios', compact('rol', 'servicios', 'serviciosAsociados'));

    }

    public function store(Request $request, $id){

    	$datos = [
    		'rol' => $id,
    		'servicio' => $request->servicio,
    	];

    	if($request->metodo=='agregar'){
    		$operacion = $this->agregar($datos);
    	}

    	if($request->metodo=='quitar'){
    		$operacion = $this->quitar($datos);
    	}

    	// if($operacion)
    	// 	return response()->json([$request->servicio, $request->metodo, $id]);

    	return response()->json($operacion);
    }

    public function agregar($datos){

    	try {
    		$rolServicio = RolesServicios::where('roles_id', $datos['rol'])
    										->where('servicios_id', $datos['servicio'])
											->firstOrFail();
    	} catch (\Exception $e) {
    		$rolServicio = new RolesServicios;
    	}

    	try {
    		// $rolServicio = new RolesServicios;
	    	$rolServicio->roles_id = $datos['rol'];
	    	$rolServicio->servicios_id = $datos['servicio'];
	    	$rolServicio->activo = 1;
	    	$rolServicio->save();

	    	return true;
    		
    	} catch (\Exception $e) {
    		return false;
    	}
    }

    public function quitar($datos){

    	try {
    		$rolServicio = RolesServicios::where('roles_id', $datos['rol'])
										->where('servicios_id', $datos['servicio'])->first();
	    	// $rolServicio->roles_id = $datos['rol'];
	    	// $rolServicio->servicios_id = $datos['servicio'];
	    	$rolServicio->activo = 0;
	    	$rolServicio->save();

	    	return true;
    		
    	} catch (\Exception $e) {
    		return $e;
    	}

    }
}
