<?php

namespace App\Http\Controllers\roles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Roles;
use Validator;

class rolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = $this->roles()->paginate(10);
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
            ],
            [
                'name.required' => 'Indique el nombre del rol',
                'name.min' => 'Mínimo tres caracteres para el nombre',
            ]
        );

        if ($validator->fails()) {
            return redirect('roles/create')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $rol = new Roles;
        $rol->name = $request->name;
        $rol->descripcion = $request->descripcion;
        $rol->save();

        return redirect('roles/create')->with('creado', 'Nuevo rol creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rol = $this->roles($id)->first();
        return view('roles.edit', compact('rol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
            ],
            [
                'name.required' => 'Indique el nombre del rol',
                'name.min' => 'Mínimo tres caracteres para el nombre',
            ]
        );

        if ($validator->fails()) {
            return redirect('roles/'. $id .'/edit')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $rol = $this->roles($id)->first();
        $rol->name = $request->name;
        $rol->descripcion = $request->descripcion;
        $rol->save();

        return redirect('roles/'. $id .'/edit')->with('actualizado', 'Rol actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function roles($id = ''){
        if($id == ''){
            $roles = Roles::orderBy('id', 'desc');
        }else
        {
            $roles = Roles::where('id', $id);
        }

        return $roles;
    }
}
