<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Servicios_al_cliente_recomendaciones as SCR;
use App\Http\Controllers\API\ServicioRecomendacionesDodontController as SCRDD;
use App\Http\Controllers\API\ServicioRecomendacionesUseController as SCRUSE;
use Webpatser\Uuid\Uuid;

class ServicioClienteRecomendacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $comp = new SCR;
        $comp->id = Uuid::generate()->string;
        $comp->servicio_id = $id;
        $comp->save();

        $scrdd = new SCRDD;
        $scrdd->store($request, $comp->id);

        $scrdd = new SCRUSE;
        $scrdd->store($request, $comp->id);

        return 1;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $comp = SCR::where('servicio_id', $id)->first();
            $comp->dodont()->delete();
            $comp->use()->delete();
            $comp->delete();
        } catch (\Throwable $th) {
            //throw $th;
        }

        $this->store($request, $id);



    return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function usuario($id = null)
    {
        if($id == null){
            return Auth::user();
        }else{
            return User::where('id', $id)->first();
        }
    }
}
