<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Notifications\SignupActivate;
use App\User;
use Auth;
use Carbon\Carbon;

class AuthController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware(['verified']);
    // }
 
    public function login(Request $request)
    {
        // return response()->json($request);
        $formato = new JSON;
        $request->validate([
            'email'       => 'required|string|email',
            'password'    => 'required|string',
        ]);

        $credentials = request(['email', 'password']);

        // $credentials['deleted_at'] = null;
        
        if (!Auth::attempt($credentials)) {
            $respuesta = $formato->error(['error' => 'Invalid username or password.']);
            return response()->json($respuesta);
        }

        $user = $request->user();
        if($user->email_verified_at == NULL){
            $respuesta = $formato->error(['error' => 'Confirm your email, check your inbox.'], '');

            try {
                $user->notify(new SignupActivate($user));
            } catch (\Exception $e) {}

                return response()->json($respuesta);
        }

        try{
            $user = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            if($request->has('fcm_token')){
                $user->fcm_token = $request->fcm_token;
            }
            $user->save();
            $token->save();
        }catch(\Exception $e){
            $respuesta = $formato->error(['error' => 'Failed to register token.' . $e], $e);
            return response()->json($respuesta);
        }

        // Aquí comprobamos si el usuario activo su cuenta
        // if($user->active==0){
        //     return response()->json($this->jsonResponse('error', null, 'Se te ha enviado un mensaje a tu email para activar tu cuenta. Por favor, actívalo para poder iniciar sesión.'), 200);
        // }

        $data = [
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => carbon::parse(
                $tokenResult->token->expires_at)
                    ->toDateTimeString(),
            // 'type' => $user->tipo->name,
            'name' => $user->name,
            // 'direccion' => $user->direccion,
            // 'latitud' => $user->latitud,
            // 'longitud' => $user->longitud,
            'email' => $user->email,
            'id' => $user->id,
            'tipo' => $user->tipo,
            'foto_url' => $user->foto_url,
            'phone' => $user->phone,
            'pais' => $user->pais,
            'foto' => $user->foto,
            'title' => $user->descripcion,
            'miscategorias' => $user->categorias(),
            'seguidos' => $user->seguidos,
            'seguidores' => $user->seguidores,
            'servicios' => $user->servicios,
        ];

        $respuesta = $formato->success($data);
        return response()->json($respuesta);
    }

    public function signup(Request $request)
    {
        $formato = new JSON;
        
        $valido = validator()->make($request->all(), [
            'name'     => 'required|string',
            'email'    => 'required|string|email|unique:users',
            'password' => 'required|min:6|string|confirmed',
            'phone' => 'required',
            'pais' => 'required',
            //'dni' => 'required|string',
        ],[
            'name.required' => "The name is required.",
            'name.string' => "I wait for a string.",
            'phone.required' => "The phone is required.",
            'email.unique' => 'The email you entered is already registered.',
            'email.email' => 'The email you entered has an invalid format.',
            'password.min' => 'Enter 8 or more characters.',
            'password.confirmed' => 'The password does not match.',
            'pais.required' => 'Enter the country.',
        ]);

        if($valido->fails()){
            $respuesta = $formato->error(['error' => $valido->errors()->first()], '');
            return response()->json($respuesta);
        }


        // $tipo = Users_tipo::where('name', 'cliente')->first();
        // $tipo_usuario = $tipo->id;
            
        
        try{
            $user = new User([
                'name'     => $request->name,
                'email'    => $request->email,
                'phone'    => $request->phone,
                'foto'    => $request->foto,
                'pais'    => $request->pais,
                'password' => bcrypt($request->password),
                'tipo' => $request->tipo,
            ]);

            // if($request->has('fcm_token')){
            //     $user->fcm_token = $request->fcm_token;
            // }
            
            $user->foto = "http://68.183.115.126/storage/categorias/81be6e30-222f-4f38-8041-76dd7a410301.jpeg";
            if($request->has('fcm_token')){
                $user->fcm_token = $request->fcm_token;
            }
            $user->pais = $request->pais;
            $user->tipo = $request->tipo;
            $user->save();
            $user->notify(new SignupActivate($user));
            $data = array(
                'mensaje' => 'Thank you for sign up, we have send you a confirmation email.',
                'user_id' => $user->id,
            );
            $respuesta = $formato->success($data);
            return response()->json($respuesta);
        }catch(\Exception $e){
            $respuesta = $formato->error(['error' => 'Error creating account.' . $e], $e);
            return response()->json($respuesta);
        }
        
    }

    public function usuario(Request $request)
    {
        $formato = new JSON;
        $usuario= [
            'id' => $request->user()->id,
            'name' => $request->user()->name,
            'email' => $request->user()->email,
            'tipo' => $request->user()->tipo,
            'miscategorias' => $request->user()->categorias(),
            'ultima_conexion' => $request->user()->updated_at->format("d/m/Y h:i A"),
            'seguidos' => $request->user()->seguidos,
            'seguidores' => $request->user()->seguidores,
        ];
        $respuesta = $formato->success($usuario);
        return response()->json($respuesta);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json(['message' => 
            'Successfully logged out']);
    }


}
