<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Servicio_al_cliente_reminder as SCRM;
use Webpatser\Uuid\Uuid;

class ServicioClienteReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        foreach($request->reminder as $rc){
            $comp = new SCRM;
            $comp->id = Uuid::generate()->string;
            $comp->mensaje = $rc['mensaje'];

            switch ($rc['tiempo']) {
                case 'semanas':
                    $comp->fecha = now()->addWeeks($rc['numero']);
                    break;
                case 'meses':
                    $comp->fecha = now()->addMonths($rc['numero']);
                    break;
                
                default:
                    $comp->fecha = now()->addDays($rc['numero']);
                    break;
            }

            // $comp->fecha = now()->addDays($rc['dias']);
            $comp->servicio_id = $id;
            $comp->cliente_id = $request->cliente_id;
            $comp->profesional_id = $request->profesional_id;
            $comp->save();
        }

        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $comp = SCRM::where('servicio_id', $id)->delete();
        } catch (\Throwable $th) {
            //throw $th;
        }

        $this->store($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
