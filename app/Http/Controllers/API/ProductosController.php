<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Marca;
use App\Categoria;
use App\Producto;

class ProductosController extends Controller
{
    public function buscarMarca(Request $r){
        $marca = Marca::where('nombre', 'like', '%' . $r->string . '%')->get();
        return response()->json($marca);
    }

    public function buscarMarcaCategoria(Request $r, $marca_id){
        $categoria = Categoria::where('nombre', 'like', '%' . $r->string . '%')
                            ->where('marca_id', $marca_id)
                            ->get()
                            ->unique('nombre')->flatten();
        return response()->json($categoria);
    }

    public function buscarMarcaCategoriaProducto(Request $r, $categoria_id){
        $producto = Producto::where('nombre', 'like', '%' . $r->string . '%')
                            ->where('categoria_id', $categoria_id)
                            // ->where('categoria_id', $categoria_id)
                            ->get()->unique('nombre')->flatten();

        return response()->json($producto);
    }

    public function buscarMarcaCategoriaProductoNumero(Request $r, $categoria_id){
        $producto = Producto::where('nombre', 'like', '%' . $r->nombreProducto . '%')
                            ->where('numero', 'like', '%'. $r->numero .'%')
                            ->where('categoria_id', $categoria_id)
                            ->get();
        return response()->json($producto);
    }
}
