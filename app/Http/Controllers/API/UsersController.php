<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\User;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $formato = new JSON;

        $valido = validator()->make($request->all(), [
            'name'     => 'required|string',
            'password' => 'required|min:6|string|confirmed',
            'phone' => 'required',
            'pais' => 'required',
            'foto' => 'required',
        ],[
            'name.required' => "The name is required.",
            'name.string' => "I wait for a string.",
            'phone.required' => "The phone is required.",
            'password.min' => 'Enter 8 or more characters.',
            'password.confirmed' => 'The password does not match.',
            'pais.required' => 'Enter the country.',
            'foto.required' => 'Enter profile picture.',
        ]);
        
        if($valido->fails()){
            $respuesta = $formato->error(['error' => $valido->errors()->first()], '');
            return response()->json($respuesta);
        }

        try {
            
            $usuario = User::where('id', Auth::user()->id)->firstOrFail();
            $usuario->name = $request->name;
            $usuario->password = bcrypt($request->password);
            $usuario->phone = $request->phone;
            $usuario->pais = $request->pais;
            $usuario->foto = $request->foto;
            $usuario->save();

        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => "Error updating the registry."], '');
            return response()->json($respuesta);
        }

        $respuesta = $formato->success(['mensaje' => "Successful update."], '');
        return response()->json($respuesta);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function editarFoto(Request $r, $user_id)
    {
        $formato = new JSON;
        try {
            $usuario = User::where('id', $user_id)
                            ->update(['foto' => $r->foto]);
            $respuesta = $formato->success(['mensaje' => 'Updated photo.']);
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'There was an error updating the photo.']);
        }
        
        return response()->json($respuesta);
    }
}
