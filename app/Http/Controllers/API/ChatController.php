<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FCMnotificacion as FCM;
use Auth;
use App\Chat;
use Carbon\Carbon;
use App\User;
use Log;

class ChatController extends Controller
{
    public function chatsActivos()
    {

        $usuarioactivo = $this->usuario()->id;

        $listarChat = Chat::where('identificador', 'like', '%'. $usuarioactivo .'%')
                            ->with(['chatcon' => function($q){
                                $q->select('id', 'name', 'email', 'foto');
                            }, 'chatde' => function($q){
                                $q->select('id', 'name', 'email', 'foto');
                            }])
                            ->orderBy('created_at', 'desc')
                            ->get()
                            ->unique('identificador')
                            ->flatten();

        $chatsactivos = array();

        foreach($listarChat as $lc){

            if($lc->chatcon->id == $usuarioactivo){
                $chatcon = array(
                    "id" => $lc->chatde->id,
                    "name" => $lc->chatde->name,
                    "email" => $lc->chatde->email,
                    "foto_url" => $lc->chatde->foto_url,
                    "soyseguidor" => $lc->chatde->soyseguidor,
                    "mesigue" => $lc->chatde->mesigue,
                    "seguidores" => $lc->chatde->seguidores,
                    "seguidos" => $lc->chatde->seguidos,
                    "servicios" => $lc->chatde->servicios,
                );
            }else{
                $chatcon = array(
                    "id" => $lc->chatcon->id,
                    "name" => $lc->chatcon->name,
                    "email" => $lc->chatcon->email,
                    "foto_url" => $lc->chatcon->foto_url,
                    "soyseguidor" => $lc->chatcon->soyseguidor,
                    "mesigue" => $lc->chatcon->mesigue,
                    "seguidores" => $lc->chatcon->seguidores,
                    "seguidos" => $lc->chatcon->seguidos,
                    "servicios" => $lc->chatcon->servicios,
                );
            }

            $chatsactivos[] = array(
                "id" => $lc->id,
                "identificador" => $lc->identificador,
                "de_user_id" => $lc->de_user_id,
                "para_user_id" => $lc->para_user_id,
                "mensaje" => $lc->mensaje,
                "leido" => $lc->leido,
                "tipo_mensaje" => $lc->tipo_mensaje,
                "created_at" => $lc->created_at,
                "updated_at" => $lc->updated_at,
                "deleted_at" => $lc->deleted_at,
                "chatcon" => $chatcon,
            );
        }

        return response()->json($chatsactivos);
    }

    public function chatUsuario($user_id)
    {
        $chatUsuario = Chat::orWhere(function($q) use ($user_id){
                                    $q->where('de_user_id', $this->usuario()->id);
                                    $q->where('para_user_id', $user_id);
                                })
                            ->orWhere(function($q) use ($user_id){
                                    $q->where('para_user_id', $this->usuario()->id);
                                    $q->where('de_user_id', $user_id);
                                })
                            ->select('mensaje', 'tipo_mensaje', 'created_at', 'de_user_id', 'para_user_id', 'identificador', 'leido')
                            ->orderBy('created_at', 'asc')
                            ->get();
        try {
            $chatLeido = Chat::where('para_user_id', $this->usuario()->id)
                                ->where('identificador', $chatUsuario->last()->identificador)
                                ->update(['leido' => true]);
        } catch (\Exception $e) {}
        
        return response()->json($chatUsuario);
    }

    public function chatUltimosMensajes(Request $r, $user_id)
    {
        $data = [
            "user_id" => $user_id,
            "fecha_ultimo_mensaje" => $r->fecha_ultimo_mensaje,
        ];

        $chatUsuario = Chat::orWhere(function($q) use ($data){
                                    $q->where('para_user_id', $this->usuario()->id);
                                    $q->where('de_user_id', $data['user_id']);
                                    $q->where("created_at", ">", Carbon::parse($data['fecha_ultimo_mensaje'])->format('Y-m-d H:i:s'));
                                })
                            ->select('mensaje', 'tipo_mensaje', 'created_at', 'de_user_id', 'para_user_id', 'identificador', 'leido')
                            ->orderBy('created_at', 'asc')
                            ->get();

        try {
            $chatUsuarioLeido = Chat::where('para_user_id', $this->usuario()->id)
                                        ->where('identificador', $chatUsuario->first()->identificador)
                                        ->update(['leido' => true]);
        } catch (\Exception $e) {}

        return response()->json($chatUsuario);
    }

    public function chatUsuarioPost(Request $r, $user_id)
    {
        if($r->has('identificador') && $r->identificador != ""){
            $identificador = $r->identificador;
        }else{
            $identificador = $this->usuario()->id . ":" . $user_id;
        }

        $mensaje = Chat::create([
            "identificador" => $identificador,
            "mensaje" => $r->mensaje,
            "tipo_mensaje" => $r->tipo_mensaje,
            "para_user_id" => $user_id,
            "de_user_id" => $this->usuario()->id,
        ]);

        try {

            $chatsSinLeer = Chat::where('identificador', 'like', '%'.$user_id.'%')
                                    ->where('leido', 0)
                                    ->select('identificador')
                                    ->distinct()
                                    ->count('identificador');

            $data = array(
                'tipo_notificacion' => 'inbox'
            );
            $notificacion = new FCM;
            $notificacion->enviarNotificacion($this->usuario($user_id)->fcm_token, '', 'Tienes un nuevo mensaje', $data, $chatsSinLeer);
        } catch (\Exception $e) {
            Log::error($e);
        }

        return response()->json($mensaje);
    }

    public function usuario($id = null)
    {
        if($id == null){
            return Auth::user();
        }else{
            return User::where('id', $id)->first();
        }
    }

    public function obtenerReminders()
    {
        $usuario = $this->usuario();

        $reminders = array();
        $recordatorios = array();

        try {
            $reminders = Chat::where('identificador', 'like', '%'.$usuario->id.'%')
                                ->where('tipo_mensaje', 'reminderCell')
                                //->select('mensaje', 'created_at')
                                ->orderBy('created_at', 'desc')
                                ->get();

            if($reminders->count()){
                foreach ($reminders as $r) {
                    $fecha = Carbon::parse($r->created_at);
                    $now = now();
                    // $tiempo = $fecha->diffInDays($now, 0) . " days";

                    $division01 = explode(';In ', $r->mensaje);
                    $tiempo = $division01[1];

                    $division02 = explode(" ", $tiempo);
                    $numero= $division02[0];
                    $palabra = $division02[1];

                    switch (strtolower($palabra)) {
                        case 'day':
                        case 'days':
                            $fechaReminder = $fecha->addDays($numero);
                            break;
                        case 'week':
                        case 'weeks':
                            $fechaReminder = $fecha->addWeeks($numero);
                            break;
                        case 'month':
                        case 'months':
                            $fechaReminder = $fecha->addMonths($numero);
                            break;
                        default:
                            $fechaReminder = 0;
                            break;
                    }

                    $diasRestantes = $now->diffInDays($fechaReminder, 0);

                    if($diasRestantes <= 0)
                        $diasRestantes = 0;

                    $recordatorios[] = array(
                        "mensaje" => $r->mensaje,
                        "tiempo"  => $diasRestantes . " Days",
                        "numero_restante" => $diasRestantes,
                        "tiempo_restante" => $palabra,
                    );
                }
            }
        } catch (\Exception $e) {
            
            Log::error($e);
            
        }
        
        return response()->json($recordatorios);
    }

}
