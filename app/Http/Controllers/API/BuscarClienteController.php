<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class BuscarClienteController extends Controller
{
    public function buscar(Request $r){

        $usuarioLogueado = $this->usuario();
        $usuarioTipo = $usuarioLogueado->tipo;
        
        switch ($usuarioTipo) {
            case 'profesional':
                if($r->chat){
                    $resultado = User::where(function($q) use ($r){
                                        $q->orWhere('id', 'like', '%' . $r->string . '%');
                                        $q->orWhere('name', 'like', '%' . $r->string . '%');
                                        $q->orWhere('email', 'like', '%' . $r->string . '%');
                                    })
                                    ->where('tipo', 'cliente')
                                    ->whereHas('misServicios', function($q) use ($usuarioLogueado){
                                        $q->where('profesional_id', $usuarioLogueado->id);
                                    })
                                    ->get();
                }else{
                    $resultado = User::where(function($q) use ($r){
                                        $q->orWhere('id', 'like', '%' . $r->string . '%');
                                        $q->orWhere('name', 'like', '%' . $r->string . '%');
                                        $q->orWhere('email', 'like', '%' . $r->string . '%');
                                    })
                                    ->where('tipo', 'cliente')
                                    ->get();
                }
                break;

            case 'cliente':
                if($r->chat){
                    $resultado = User::where(function($q) use ($r){
                                        $q->orWhere('id', 'like', '%' . $r->string . '%');
                                        $q->orWhere('name', 'like', '%' . $r->string . '%');
                                        $q->orWhere('email', 'like', '%' . $r->string . '%');
                                    })
                                    ->where('tipo', 'profesional')
                                    ->whereHas('misServicios', function($q) use ($usuarioLogueado){
                                        $q->where('cliente_id', $usuarioLogueado->id);
                                    })
                                    ->get();
                }else{
                    $resultado = User::where(function($q) use ($r){
                                        $q->orWhere('id', 'like', '%' . $r->string . '%');
                                        $q->orWhere('name', 'like', '%' . $r->string . '%');
                                        $q->orWhere('email', 'like', '%' . $r->string . '%');
                                    })
                                    ->where('tipo', 'profesional')
                                    ->get();
                }
                break;

            default:
                $resultado = [];
                break;
        }

        return response()->json($resultado);
    }

    public function mostrar(Request $r){

        $usuarioLogueado = $this->usuario();
        $usuarioTipo = $usuarioLogueado->tipo;

        switch ($usuarioTipo) {
            case 'profesional':
                $resultado = User::where('tipo', 'cliente')
                                ->whereHas('misServicios', function($q) use ($usuarioLogueado){
                                    $q->where('profesional_id', $usuarioLogueado->id);
                                })
                                ->get();
                break;

            case 'cliente':
                $resultado = User::where('tipo', 'profesional')
                                ->whereHas('misServicios', function($q) use ($usuarioLogueado){
                                    $q->where('cliente_id', $usuarioLogueado->id);
                                })
                                ->get();
                break;
            
            default:
                $resultado = [];
                break;
        }

        return response()->json($resultado);
    }

    public function usuario()
    {
        return Auth::user();
    }
}
