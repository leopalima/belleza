<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Servicio_recomendaciones_dodont;
use Webpatser\Uuid\Uuid;
use App\hacer_no_hacer;
use Auth;

class ServicioRecomendacionesDodontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formato = new JSON;
        $profesional_id = $this->profesional()->id;

        $do = hacer_no_hacer::where('user_id', $profesional_id)
                                ->where('estado', 'activo')
                                ->get();
        
        $lista= [];
        
        if(count($do)){
            foreach($do as $d){
                if($d['tipo'] === "Hacer"){
                    $lista[] = [
                        'tipo' => 'do',
                        'tarea' => $d['descripcion'],
                    ];
                }else{
                    $lista[] = [
                        'tipo' => 'dont',
                        'tarea' => $d['descripcion'],
                    ];

                }
            }
        }

        $respuesta = $formato->success($lista);

        return response()->json($respuesta);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if($request->has('recomendaciones.dodont')){
            foreach($request->recomendaciones['dodont'] as $rcdd){
                $dd = new Servicio_recomendaciones_dodont;
                $dd->id = Uuid::generate()->string;
                $dd->tipo = $rcdd['tipo'];
                $dd->tarea = $rcdd['tarea'];
                $dd->recomendaciones_id = $id;
                $dd->save();
            }
        }

        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profesional(){
        return Auth::user();
    }
}
