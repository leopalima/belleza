<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Servicios_al_cliente_formula as SCFo;
use App\Http\Controllers\API\ServicioClienteFormulaProductosController as SCFP;
use Webpatser\Uuid\Uuid;

class ServicioClienteFormulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if($request->formula['name'] != ""){
            $comp = new SCFo;
            $comp->id = Uuid::generate()->string;
            $comp->name = $request->formula['name'];
            $comp->descripcion_proceso = $request->formula['descripcion_proceso'];
            $comp->servicio_id = $id;
            $comp->save();

            $productos = new SCFP;
            $productos->store($request, $comp->id);
        }

        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $comp = SCFo::where('servicio_id', $id)->first();
            $comp->productos()->delete();
            $comp->delete();
        } catch (\Throwable $th) {
            //throw $th;
        }

        $this->store($request, $id);

        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
