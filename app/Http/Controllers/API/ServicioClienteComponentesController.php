<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Servicios_al_cliente_componentes as SCC;
use Webpatser\Uuid\Uuid;

class ServicioClienteComponentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        foreach($request->componentes as $rc){
            $comp = new SCC;
            $comp->id = Uuid::generate()->string;
            $comp->perfil = $rc['perfil'];
            $comp->url_componente = $rc['url_componente'];
            $comp->servicio_id = $id;
            $comp->save();
        }

        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $comp = SCC::where('servicio_id', $id)->delete();
        } catch (\Throwable $th) {
            //throw $th;
        }

        $this->store($request, $id);

        return 1;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
