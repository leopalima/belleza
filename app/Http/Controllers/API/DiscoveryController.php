<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Servicios_al_cliente;
use App\User;
use Auth;
use Log;

class DiscoveryController extends Controller
{
    
    public function discovery(){
        $sc = Servicios_al_cliente::with([
                    'cliente' => function($q){
                        $q->select('id', 'name', 'email');
                    },
                    'profesional' => function($q){
                        $q->select('id', 'name', 'email');
                    },
                    'categoria',
                    'fotos',
                ])
                ->orderBy('created_at', 'desc')
                ->get();
        return response()->json($sc);
    }

    public function busqueda(Request $r){
        $formato = new JSON;
        $respuesta = [];
        if($r->searchby == "professional"){
            $respuesta = $this->busquedaProfessional($r);
        }
        if($r->searchby == "service"){
            $respuesta = $this->busquedaServicio($r);
        }
        if($r->searchby == "location"){
            $respuesta = $this->busquedaLocation($r);
        }
        return $formato->success($respuesta);
    }

    public function busquedaProfessional($r){
        try {
            $professionalBusqueda = User::where('tipo', 'profesional')
                                    ->where(function($q) use ($r){
                                        $q->orWhere('name', 'like', '%'.$r->string.'%');
                                        $q->orWhere('email', 'like', '%'.$r->string.'%');
                                        $q->orWhere('descripcion', 'like', '%'.$r->string.'%');
                                    })
                                    ->get();

            $professional = [];
            foreach($professionalBusqueda as $c){
                $professional[] = [
                    "id"=> $c->id,
                    "name"=> $c->name,
                    "email"=> $c->email,
                    "email_verified_at"=> $c->email_verified_at,
                    "tipo"=> $c->tipo,
                    "descripcion"=> $c->descripcion,
                    "location"=> $c->location,
                    "created_at"=> $c->created_at,
                    "updated_at"=> $c->updated_at,
                    "foto_url"=> $c->foto_url,
                    "miscategorias"=> $c->categorias(),
                    "soyseguidor"=>  $c->soyseguidor,
                    "mesigue"=> $c->mesigue
                ];
            }
            return $professional;
        } catch (\Exception $e) {
           return [];
        }
    }
    
    public function busquedaServicio($r){
        try {

            switch ($this->usuario()->tipo) {
                case 'profesional':
                    $usuario = 'profesional_id';
                    break;
                case 'cliente':
                    $usuario = 'cliente_id';
                    break;

                default:
                    $usuario = 'profesional_id';
                    break;
            }

            $cliente = Servicios_al_cliente::with(['cliente', 'categoria', 'profesional', 'fotos'])
                                            ->where(function($q) use ($r){
                                                $q->orWhere('author_tag', 'like', '%'.$r->string.'%');
                                            })
                                            ->where($usuario, $this->usuario()->id)
                                            ->where('status', 1)
                                            ->get();
            return $cliente;
        } catch (\Exception $e) {
            Log::error($e);
            return $e;
        }
    }
    
    public function busquedaLocation($r){
        try {
            $location = User::where('tipo', 'profesional')
                                    ->where(function($q) use ($r){
                                        $q->orWhere('location', 'like', '%'.$r->string.'%');
                                    })
                                    ->get();

            $professional = [];
            foreach($location as $c){
                $professional[] = [
                    "id"=> $c->id,
                    "name"=> $c->name,
                    "email"=> $c->email,
                    "email_verified_at"=> $c->email_verified_at,
                    "tipo"=> $c->tipo,
                    "descripcion"=> $c->descripcion,
                    "location"=> $c->location,
                    "created_at"=> $c->created_at,
                    "updated_at"=> $c->updated_at,
                    "foto_url"=> $c->foto_url,
                    "miscategorias"=> $c->categorias(),
                    "soyseguidor"=>  $c->soyseguidor,
                    "mesigue"=> $c->mesigue
                ];
            }
            return $professional;
        } catch (\Throwable $th) {
           return [];
        }
    }

    public function usuario()
    {
        return Auth::user();
    }
}
