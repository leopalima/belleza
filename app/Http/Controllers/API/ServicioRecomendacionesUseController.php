<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Servicio_recomendaciones_use;
use Webpatser\Uuid\Uuid;

class ServicioRecomendacionesUseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if($request->has('recomendaciones.use')){
            foreach($request->recomendaciones['use'] as $rcdd){
                $dd = new Servicio_recomendaciones_use;
                $dd->id = Uuid::generate()->string;
                $dd->producto_id = $rcdd['producto_id'];
                $dd->cantidad = $rcdd['cantidad'];
                $dd->medida = $rcdd['medida'];
                $dd->frecuencia = $rcdd['frecuencia'];
                $dd->recomendaciones_id = $id;
                $dd->save();
            }
     }

        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
