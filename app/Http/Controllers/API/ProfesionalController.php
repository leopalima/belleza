<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\User;
use App\Categorias_user;
use Auth;

class ProfesionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profesional(){
        return Auth::user();
    }

    public function postLogin(Request $r){

        $formato = new JSON;

        try {
            $profesional_id = $this->profesional()->id;
            $profesional = User::find($profesional_id);
            $profesional->descripcion = $r->titulo;
            $profesional->save();

            Categorias_user::where('user_id', $profesional_id)->delete();

            foreach($r->categorias as $c){
                $catprof = new Categorias_user;
                $catprof->user_id = $profesional_id;
                $catprof->categoria_id = $c;
                $catprof->save();
            }

            $respuesta = $formato->success("Gracias!");
    
        } catch (\Exception $e) {
            $respuesta = $formato->error("Error!", $e);
        }

        return response()->json($respuesta);
    }
}
