<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\User;

class BuscarProfesionalController extends Controller
{
    public function buscar(Request $r){
        $cliente = User::orWhere('id', 'like', '%' . $r->string . '%')
                        ->orWhere('name', 'like', '%' . $r->string . '%')
                        ->orWhere('email', 'like', '%' . $r->string . '%')
                        ->where('tipo', 'profesional')
                        ->get();
        return response()->json($cliente);
    }

    public function mostrar(Request $r){
        $formato = new JSON;
        $clienteMostrar = User::where('tipo', 'profesional')->get();

        $clientes = [];
        foreach($clienteMostrar as $c){
            $clientes[] = [
                "id"=> $c->id,
                "name"=> $c->name,
                "email"=> $c->email,
                "email_verified_at"=> $c->email_verified_at,
                "tipo"=> $c->tipo,
                "descripcion"=> $c->descripcion,
                "location"=> $c->location,
                "created_at"=> $c->created_at,
                "updated_at"=> $c->updated_at,
                "foto_url"=> $c->foto_url,
                "servicios"=> $c->servicios,
                "miscategorias"=> $c->categorias(),
            ];
        }

        return response()->json($clientes);
    }
}
