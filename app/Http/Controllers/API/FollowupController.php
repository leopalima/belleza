<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\User;
use App\Followup;
use Auth;


class FollowupController extends Controller
{
    public function seguir($id){
        $formato = new JSON;

        try {

            $seguirA = User::findOrFail($id);

            $yaestoysiguiendo = Followup::where('user_id', $this->usuario()->id)
                                        ->where('seguido_id', $id)
                                        ->first();
            if($yaestoysiguiendo){
                $yaestoysiguiendo->estatus = 1;
                $yaestoysiguiendo->save();
            }else{
                $seguir = new Followup;
                $seguir->user_id = $this->usuario()->id;
                $seguir->seguido_id = $id;
                $seguir->estatus = 1;
                $seguir->save();
            }

            $respuesta = $formato->success("You are following.");

        } catch (\Exception $e) {
            $respuesta = $formato->error(["error" => "There was a problem following." . $e], $e);
        }

        return response()->json($respuesta);
        
    }
    
    public function dejarseguir($id){

        $formato = new JSON;
        try {
            
            $dejarseguir = Followup::where('user_id', $this->usuario()->id)
                                        ->where('seguido_id', $id)
                                        ->update(['estatus' => 0]);
            if(!$dejarseguir){
                throw new \Exception("Error Processing Request");
                
            }

            $respuesta = $formato->success("The user was stopped following.");
            

        } catch (\Exception $e) {
            $respuesta = $formato->error(["error" => "There was a problem."], $e);
        }

        return response()->json($respuesta);

    }

    public function usuario(){
        return Auth::user();
    }
}
