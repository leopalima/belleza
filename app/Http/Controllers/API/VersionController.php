<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Version;

class VersionController extends Controller
{
    public function muestraVersion($so)
    {
        try {
            $formato = new JSON;

            switch (strtoupper($so)) {
                case 'IOS':
                    $version = Version::where('so', $so)
                                    ->orderBy('created_at', 'DESC')
                                    ->first();
                    break;
                case 'ANDROID':
                    $version = Version::where('so', $so)
                                    ->orderBy('created_at', 'DESC')
                                    ->first();
                    break;
                
                default:
                    throw new \Exception("Sin versión para ese sistema operativo.");
                    break;
            }

            $version = [
                "id" => $version->id,
                "numero" => $version->version_num,
                "fecha" => $version->fecha,
                "autor" => $version->autor,
                "cambios" => $version->cambios,
                "created_at" => $version->created_at,
                "updated_at" => $version->updated_at,
            ];
    
            $respuesta = $formato->success($version);

        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => "Sin versión para ese sistema operativo"], $e);
        }
            
        return response()->json($respuesta);

    }
}
