<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Servicios_al_cliente;
use App\Http\Controllers\API\ServicioClienteComponentesController as SCC;
use App\Http\Controllers\API\ServicioClienteFotosController as SCF;
use App\Http\Controllers\API\ServicioClienteFormulaController as SCFo;
use App\Http\Controllers\API\ServicioClienteRecomendacionesController as SCR;
use App\Http\Controllers\API\ServicioClienteReminderController as SCRM;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use Webpatser\Uuid\Uuid;
use Auth;
use App\User;
use Log;
use App\Http\Controllers\FCMnotificacion as FCM;


class ServiciosClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipodeusuarioaevaluar = $this->usuario()->tipo;
        $idusuarioevaluado = $this->usuario()->id;
        switch ($tipodeusuarioaevaluar) {
            case 'profesional':
                $clavetipodeusuario = 'profesional_id';
                break;
            
            case 'cliente':
                $clavetipodeusuario = 'cliente_id';
                break;
            
            default:
                $clavetipodeusuario = 'profesional_id';
                break;
        }
        $profesional_id = $this->profesional()->id;
        $sc = Servicios_al_cliente::with([
                                    'cliente' => function($q){
                                        $q->select('id', 'name', 'email', 'foto');
                                    },
                                    'profesional' => function($q){
                                        $q->select('id', 'name', 'email', 'foto');
                                    },
                                    'componentes',
                                    'formula.productos.producto.categoria.marca',
                                    'fotos',
                                    'recomendaciones.dodont',
                                    'recomendaciones.use.producto.categoria.marca',
                                    'reminder',
                                    'categoria'
                                ])
                                ->where($clavetipodeusuario, $idusuarioevaluado)
                                ->orderBy('created_at', 'desc')
                                ->get();
        return response()->json($sc);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formato = new JSON;
        try {
            $request->profesional_id = $this->profesional()->id;

            $sc = new Servicios_al_cliente;
            $sc->id = Uuid::generate()->string;
            $sc->satisfaccion = $request->satisfaccion;
            $sc->foto_principal = $request->foto_principal;
            $sc->mensaje = $request->mensaje;
            $sc->fecha_recordatorio = now();
            $sc->profesional_id = $request->profesional_id;
            $sc->categoria_id = $request->categoria_id;
            $sc->cliente_id = $request->cliente_id;
            $sc->status = $request->status;
            $sc->author_tag = $request->author_tag;
            $sc->style = $request->style;
            $sc->save();

            if($request->has('componentes')){
                $rc = new SCC;
                $rc->store($request, $sc->id);
            }
            
            if($request->has('fotos')){
                $rf = new SCF;
                $rf->store($request, $sc->id);
            }

            if($request->has('formula')){
                $rf = new SCFo;
                $rf->store($request, $sc->id);
            }

            if($request->has('recomendaciones')){
                $rf = new SCR;
                $rf->store($request, $sc->id);
            }

            if($request->has('reminder')){
                $scrm = new SCRM;
                $scrm->store($request, $sc->id);
            }

            $respuesta = $formato->success('Recorded service.');

            try {
                $notificacion = new FCM;
                $notificacion->enviarNotificacion($this->usuario($request->cliente_id)->fcm_token, '', 'New service for you from ' . $this->profesional()->name . '.' , [], 0);
            } catch (\Exception $e) {
                Log::error($e);
            }

        } catch (\Exception $e) {
            $respuesta = $formato->error('Error recording service.', $e);
        }

        return response()->json($respuesta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::where('id', $id)->firstOrFail();
        $tipodeusuarioaevaluar = $usuario->tipo;
        $idusuarioevaluado = $usuario->id;
        switch ($tipodeusuarioaevaluar) {
            case 'profesional':
                $clavetipodeusuario = 'profesional_id';
                break;

            case 'cliente':
                $clavetipodeusuario = 'cliente_id';
                break;

            default:
                $clavetipodeusuario = 'profesional_id';
                break;
        }

        $sc = Servicios_al_cliente::with([
                                    'cliente' => function($q){
                                        $q->select('id', 'name', 'email', 'foto', 'tipo');
                                    },
                                    'profesional' => function($q){
                                        $q->select('id', 'name', 'email', 'foto', 'tipo');
                                    },
                                    'componentes',
                                    'formula.productos.producto.categoria.marca',
                                    'fotos',
                                    'recomendaciones.dodont',
                                    'recomendaciones.use.producto.categoria.marca',
                                    'reminder',
                                    'categoria'
                                ])
                                ->where($clavetipodeusuario, $idusuarioevaluado)
                                ->orderBy('created_at', 'desc')
                                ->get();
        return response()->json($sc);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formato = new JSON;
        try {
            $request->profesional_id = $this->profesional()->id;

            $sc = Servicios_al_cliente::where('id', $id)->first();
            // $sc->id = Uuid::generate()->string;
            $sc->satisfaccion = $request->satisfaccion;
            $sc->foto_principal = $request->foto_principal;
            $sc->mensaje = $request->mensaje;
            $sc->fecha_recordatorio = now();
            // $sc->profesional_id = $request->profesional_id;
            $sc->categoria_id = $request->categoria_id;
            $sc->cliente_id = $request->cliente_id;
            $sc->status = $request->status;
            $sc->save();

            $rc = new SCC;
            $rc->update($request, $sc->id);
            
            $rf = new SCF;
            $rf->update($request, $sc->id);

            $rf = new SCFo;
            $rf->update($request, $sc->id);

            $rf = new SCR;
            $rf->update($request, $sc->id);
            
            $scrm = new SCRM;
            $scrm->update($request, $sc->id);

            $respuesta = $formato->success('Recorded service.');

            try {
                $notificacion = new FCM;
                $notificacion->enviarNotificacion($this->usuario($request->cliente_id)->fcm_token, '', 'Edit service for you from ' . $this->profesional()->name . '.', [], 0);
            } catch (\Exception $e) {
                Log::error($e);
            }

        } catch (\Exception $e) {
            $respuesta = $formato->error('Error recording service.' . $e, $e);
        }

        return response()->json($respuesta);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profesional(){
        return Auth::user();
    }

    public function usuario($id = null)
    {
        if($id == null){
            return Auth::user();
        }else{
            return User::where('id', $id)->first();
        }
    }
}
