<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ArtistaInvitaCliente;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use Mail;
use Auth;

class InvitarController extends Controller
{
    public function invitarcliente(Request $i){
        $formato = new JSON;
        try {
            $profesional = Auth::user()->name;
            Mail::to($i->email)
                ->send(new ArtistaInvitaCliente($profesional));
            $respuesta = $formato->success(['mensaje' => 'You have made the invitation']);
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'An error occurred while sending the email'], $e);
            return response()->json($e);
        }

        return response()->json($respuesta);
    }
}
