<?php

namespace App\Http\Controllers\menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Menus;

class menuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = $this->menus()->paginate(10);
        return view('menu.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
            ],
            [
                'name.required' => 'Indique el nombre del menu',
                'name.min' => 'Mínimo tres caracteres para el nombre',
            ]
        );

        if ($validator->fails()) {
            return redirect('menu/create')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $menu = new Menus;
        $menu->name = $request->name;
        $menu->descripcion = $request->descripcion;
        $menu->save();

        return redirect('menu/create')->with('creado', 'Nuevo menú creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = $this->menus($id)->first();
        return view('menu.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
            ],
            [
                'name.required' => 'Indique el nombre del menu',
                'name.min' => 'Mínimo tres caracteres para el nombre',
            ]
        );

        if ($validator->fails()) {
            return redirect('menu/'. $id .'/edit')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $menu = $this->menus($id)->first();
        $menu->name = $request->name;
        $menu->descripcion = $request->descripcion;
        $menu->save();

        return redirect('menu/'. $id .'/edit')->with('actualizado', 'El menú se ha actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function menus($id = ''){
        if($id == ''){
            $menus = Menus::orderBy('id', 'desc');
        }else
        {
            $menus = Menus::where('id', $id);
        }

        return $menus;
    }
}
