<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $table = 'marca';

    protected $fillable = [
        'nombre', 'user_id'
    ];

    public function categoria(){
        return $this->hasMany('App\Categoria', 'marca_id', 'id');
    }

    public function propietario(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getNombreAttribute($value){
        return strtoupper($value);
    }
}
