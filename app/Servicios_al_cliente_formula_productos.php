<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicios_al_cliente_formula_productos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cantidad', 'medida', 'producto_id', 'formula', 'formula_id',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'servicios_al_cliente_formula_productos';

    /**
     * Muestra el Uuid correctamente.
    */
    public $incrementing = false;

    public function producto(){
        return $this->hasOne('App\Producto', 'id', 'producto_id');
    }
}
