<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hacer_no_hacer extends Model
{
    protected $table = 'hacer';

    protected $fillable = [
        'tipo', 'user_id', 'descripcion', 'estado',
    ];

 
}
