<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuariosformularios extends Model
{
    protected $table = 'usersroles_rolesformularios';

    protected $fillable = [
        'roles_formularios_id', 'users_roles_id', 'listar', 'crear', 'editar', 'borrar',
    ];
}
