<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolesMenus extends Model
{
    protected $table = 'roles_menu';

    protected $fillable = [
        'roles_id', 'menus_id',
    ];

    public function usuarioRolesRolesMenus(){
    	return $this->hasMany('App\Usuariosmenus', 'roles_menu_id', 'roles_id');
    }
}
