<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ArtistaInvitaCliente extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $invita;

    public function __construct($invita)
    {
        $this->invita = $invita;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $profesional = $this->invita;
        return $this->subject('You have invitation')
                ->view('notifications.artistainvitacliente', compact('profesional'));
    }
}
