<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Servicios_al_cliente extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'satisfaccion', 'mensaje', 'fecha_recordatorio', 'profesional_id', 'cliente_id', 'categoria_id', 'author_tag',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'servicios_al_cliente';

    /**
     * Muestra el Uuid correctamente.
    */
    public $incrementing = false;

    /**
     * Relación con clientes.
    */
    public function cliente(){
        return $this->hasOne('App\User', 'id', 'cliente_id');
    }

    /**
     * Relación con profesionales.
    */
    public function profesional(){
        return $this->hasOne('App\User', 'id', 'profesional_id');
    }

    /**
     * Relación con componentes.
    */
    public function componentes(){
        return $this->hasMany('App\Servicios_al_cliente_componentes', 'servicio_id', 'id');
    }

    /**
     * Relación con formulas.
    */
    public function formula(){
        return $this->hasOne('App\Servicios_al_cliente_formula', 'servicio_id', 'id');
    }

    /**
     * Relación con formulas.
    */
    public function fotos(){
        return $this->hasMany('App\Servicios_al_cliente_fotos', 'servicio_id', 'id');
    }

    /**
     * Relación con recomendaciones.
    */
    public function recomendaciones(){
        return $this->hasOne('App\Servicios_al_cliente_recomendaciones', 'servicio_id', 'id');
    }

    public function reminder(){
        return $this->hasMany('App\Servicio_al_cliente_reminder', 'servicio_id', 'id');
    }

    public function getFechaRecordatorioAttribute($value)
    {
        $fecha = Carbon::parse($value);
        return $fecha->format('M jS Y');
    }

    public function categoria(){
        return $this->hasOne('App\Categorias', 'id', 'categoria_id');
    }

}
