<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'producto';

    protected $fillable = [
        'nombre', 'categoria_id', 'medida', 'numero', 'descripcion', 'user_id', 'tipo'
    ];

    public function categoria(){
        return $this->hasOne('App\Categoria', 'id', 'categoria_id');
    }

    public function propietario(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getNombreAttribute($value)
    {
        return strtoupper($value);
    }
}
