<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicios_al_cliente_formula extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'descripcion_proceso',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'servicios_al_cliente_formula';

    /**
     * Muestra el Uuid correctamente.
    */
    public $incrementing = false;

    public function productos(){
        return $this->hasMany('App\Servicios_al_cliente_formula_productos', 'formula_id', 'id');
    }
}
