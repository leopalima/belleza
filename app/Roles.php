<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';

    protected $fillable = [
        'name', 'descripcion',
    ];

    public function servicios(){
    	return $this->hasMany('App\RolesServicios', 'roles_id', 'id')->activo();
    }
}
