<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\Formularios;
use App\Menus;
use Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Passport::ignoreMigrations();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('formulario', function ($opcion) {
            $opciones= explode(".", $opcion);
            try {
                $formulario = Formularios::where('name', $opciones[0])->first();
                $usuario = Auth::user();
                foreach (Auth::user()->roles as $roles) {
                    foreach ($roles->rolesFormulario as $rolesFormulario) {
                        if($rolesFormulario->formularios_id == $formulario->id){
                            foreach ($rolesFormulario->usuarioRolesRolesFormularios as $usuarioRolesRolesFormularios) {
                                if($usuarioRolesRolesFormularios->roles_formularios_id == $rolesFormulario->id){
                                    if(count($opciones)==1 or $opciones[1]=='listar' ){
                                        return $usuarioRolesRolesFormularios->listar;
                                    }else{
                                        switch ($opciones[1]) {
                                            case 'crear':
                                                return $usuarioRolesRolesFormularios->crear;
                                                break;
                                            case 'editar':
                                                return $usuarioRolesRolesFormularios->editar;
                                                break;
                                            case 'borrar':
                                                return $usuarioRolesRolesFormularios->borrar;
                                                break;
                                            
                                            default:
                                                return false;
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                return false;
            }
        });
        Blade::if('menu', function ($opcion) {
            if(Auth::user()->email == 'root@root.com')
                return true;
            
            try {
                $menu = Menus::where('name', $opcion)->first();
                $usuario = Auth::user();
                foreach (Auth::user()->roles as $roles) {
                    foreach ($roles->rolesMenu as $rolesMenu) {
                        if($rolesMenu->menus_id == $menu->id){
                            foreach ($rolesMenu->usuarioRolesRolesMenus as $usuarioRolesRolesMenus) {
                                if($usuarioRolesRolesMenus->roles_menu_id == $rolesMenu->id){
                                    return $usuarioRolesRolesMenus->visualizar;
                                }
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                return false;
            }
        });



        SChema::defaultStringLength(191);
        /*
        *To enable pagination for collection
        */
        if (!Collection::hasMacro('paginate')) {
            Collection::macro('paginate', 
                function ($perPage = 15, $page = null, $options = []) {
                $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
                return (new LengthAwarePaginator(
                    $this->forPage($page, $perPage), $this->count(), $perPage, $page, $options))
                    ->withPath('');
            });
        }

        Blade::if('usuario', function ($opcion) {
            if(Auth::user()->tipo == $opcion){
                return true;
            }
                return false;
        });

        Blade::if('usuarios', function ($opcion = []) {

            for($i=0; $i<count($opcion); $i++){
                if(Auth::user()->tipo == $opcion[$i]){
                    return true;
                }
            }
            return false;    
        });





    }
}
