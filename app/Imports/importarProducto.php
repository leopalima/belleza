<?php

namespace App\Imports;

use App\Producto;
use App\Marca;
use App\Categoria;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Auth;

class importarProducto implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // $productoEncontrado = Producto::where('nombre', '=', $row['producto'])->first();
        // if(!$productoEncontrado){
 
            return Producto::updateOrCreate(
                ['id' => $row['id'], 'user_id' => $this->usuario()->id],
                [
                'user_id' => $this->usuario()->id,
                'nombre'     => $row['producto'],
                'medida'    => $row['medida'], 
                'numero'    => $row['numero'], 
                'descripcion'    => $row['descripcion'], 
                'categoria_id'    => $this->categoria($row['categoria'], $row['marca']),
                'tipo'    => $row['tipo_uso'],
             ]);
        // }else{
        //         $productoEncontrado->nombre     = $row['producto'];
        //         $productoEncontrado->medida    = $row['medida'];
        //         $productoEncontrado->numero    = $row['numero'];
        //         $productoEncontrado->descripcion    = $row['descripcion'];
        //         $productoEncontrado->categoria_id = $this->categoria($row['categoria'], $row['marca']);
        //         $productoEncontrado->save();
        //         return $productoEncontrado;
        // }
    }

    public function marca($m){

        
        $marcaEncontrada = Marca::where('nombre', '=', $m)->first();

        if(!$marcaEncontrada){
            $marca = new Marca;
            $marca->nombre = $m;
            $marca->user_id=$this->usuario()->id;
            $marca->save();
            return $marca->id;
        }else{
            return $marcaEncontrada->id;
        }

    }

    public function categoria($c, $m){
        $categoriaEncontrada = Categoria::where('nombre', '=', $c)
                                        ->whereHas('marca', function($q) use ($m){
                                            $q->where('nombre', $m);
                                        })
                                        ->first();

        if(!$categoriaEncontrada){
            $categoria = new Categoria;
            $categoria->nombre = $c;
            $categoria->user_id= $this->usuario()->id;
            $categoria->marca_id = $this->marca($m);
            $categoria->save();
            return $categoria->id;
        }else{
            return $categoriaEncontrada->id;
        }
    }
    public function usuario (){

        return Auth::user();
    }

}
