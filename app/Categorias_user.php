<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias_user extends Model
{
    protected $table = 'categorias_user';

    protected $fillable = [
        'user_id', 'categoria_id',
    ];

    public function categoria(){
        return $this->hasOne('App\Categorias', 'id', 'categoria_id');
    }

}
