<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicios_al_cliente_recomendaciones extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo', 'servicio_id',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'servicios_al_cliente_recomendaciones';

    /**
     * Muestra el Uuid correctamente.
    */
    public $incrementing = false;

    public function dodont(){
        return $this->hasMany('App\Servicio_recomendaciones_dodont', 'recomendaciones_id', 'id');
    }

    public function use(){
        return $this->hasMany('App\Servicio_recomendaciones_use', 'recomendaciones_id', 'id');
    }
}
