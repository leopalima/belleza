<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Followup extends Model
{
    protected $table = 'followup';

    protected $fillable = [
        'user_id', 'seguido_id','estatus',
    ];
}
