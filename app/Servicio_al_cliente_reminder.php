<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Servicio_al_cliente_reminder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'servicio_id', 'cliente_id', 'profesional_id', 'mensaje', 'fecha',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'servicio_al_cliente_reminder';

    /**
     * Muestra el Uuid correctamente.
    */
    public $incrementing = false;

    /**
     * Campos personalizados.
     */
    protected $appends = ['tiempo', 'numero_restante', 'tiempo_restante'];

    /**
     * Establece los campos personalizados.
     */
    public function getTiempoAttribute(){
        // $tiempoEntrega =  $this->configuracion()->where('clave', 'tiempoEntrega')->first();
        // return $tiempoEntrega != null ? (int) $tiempoEntrega->valor : 15;

        $fecha = Carbon::parse($this->fecha);
        $now = now();
        return $fecha->diffInDays() . " days";
    }

    public function getNumeroRestanteAttribute(){
        $tiempo = $this->tiempo;
        $numero = explode(" ", $tiempo);

        if($numero[1] == "hours" || $numero[1] == "hour")
           return $numero = 1;

        return (int) $numero[0];

    }

    public function getTiempoRestanteAttribute(){
        $tiempo = $this->tiempo;
        $numero = explode(" ", $tiempo);
        switch ($numero[1]) {
            case 'day':
            case 'days':
                return "dias";
                break;
            case 'week':
            case 'weeks':
                return "semanas";
                break;
            case 'month':
            case 'months':
                return "meses";
                break;
            
            default:
                return "dias";
                break;
        }
    }
}
