<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Proyecto inBe</title>

		<!--favicon -->
		<link rel="icon" href="{{url('favicon.ico')}}" type="image/x-icon"/>

		<!--Bootstrap.min css-->
		<link rel="stylesheet" href="{{url('assets/plugins/bootstrap/css/bootstrap.min.css')}}">

		<!--Icons css-->
		<link rel="stylesheet" href="{{url('assets/css/icons.css')}}">

		<!--Style css-->
		<link rel="stylesheet" href="{{url('assets/css/style.css')}}">

		<!--mCustomScrollbar css-->
		<link rel="stylesheet" href="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css')}}">

		<!--Sidemenu css-->
		<link rel="stylesheet" href="{{url('assets/plugins/toggle-menu/sidemenu.css')}}">

		@yield('libreriascss')

	</head>

	<body class="app ">

		<div id="spinner"></div>

		<div id="app">
			<div class="main-wrapper" >
				@include('layout.menusuperior')

				@include('layout.menulateral')

				<div class="app-content">
					<div class="section">
{{--
                    	<ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Pages</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Empty page</li>
                        </ol>
--}}
                        @yield('cuerpo')
					</div>
				</div>

				<footer class="main-footer">
					<div class="text-center">
						Copyright &copy; Area51 @php echo date('Y') @endphp. Design By<a href=""> LPalima</a>
					</div>
				</footer>

			</div>
		</div>

		<!--Jquery.min js-->
		<script src="{{url('assets/js/jquery.min.js')}}"></script>

		<!--popper js-->
		<script src="{{url('assets/js/popper.js')}}"></script>

		<!--Tooltip js-->
		<script src="{{url('assets/js/tooltip.js')}}"></script>

		<!--Bootstrap.min js-->
		<script src="{{url('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

		<!--Jquery.nicescroll.min js-->
		<script src="{{url('assets/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>

		<!--Scroll-up-bar.min js-->
		<script src="{{url('assets/plugins/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>

		<!--mCustomScrollbar js-->
		<script src="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

		<!--Sidemenu js-->
		<script src="{{url('assets/plugins/toggle-menu/sidemenu.js')}}"></script>

		@yield('libreriasjavascript')


		<!--Scripts js-->
		<script src="{{url('assets/js/scripts.js')}}"></script>

		@yield('javascript')

	</body>
</html>