@extends('layout.index')

@section('libreriascss')

@endsection

@section('cuerpo')
{{-- Hover Table - Headercolor Table --}}
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h4>Menus</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover mb-0 text-nowrap">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Descripción</th>
							<th></th>
						</tr>
						@foreach($menus as $menu)
						<tr>
							<td>{{$menu->id}}</td>
							<td>{{$menu->name}}</td>
							<td>{{Str::limit($menu->descripcion, 25)}}</td>
							<td>
								<a class="btn btn-info" href="{{action('menu\menuController@edit', ['id' => $menu->id])}}">Editar</a>
							</td>
						</tr>
						@endforeach
					</table>
					{{ $menus->links() }}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('libreriasjavascript')

@endsection

@section('javascript')
@endsection
