@extends('layout.index')

@section('libreriascss')
{{-- Formularios --}}
<!--Morris css-->
<link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
{{-- Notificaciones emergentes --}}
<!--Toastr css-->
<link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">
@endsection

@section('cuerpo')
{{-- Formularios - General Elements --}}
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h4>Nuevo menú</h4>
			</div>
			<div class="card-body">
				<form name="frmMenus" class="form-horizontal" method="POST" action="{{action('menu\menuController@update', ['id' => $menu->id])}}">
					@csrf
					@method('PUT')
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Nombre</label>
						<div class="col-md-9">
							<input type="text" name="name" id="name" class="form-control" value="{{old('name') ? old('name') : $menu->name}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Descripción</label>
						<div class="col-md-9">
							<textarea class="form-control" name="descripcion" rows="6">{{old('descripcion') ? old('descripcion') : $menu->descripcion}}</textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label"></label>
						<div class="col-md-9">
							<button type="submit" class="btn btn-info">Guardar</button>
							<a href="{{route('menu.index')}}" class="btn btn-primary">Cancelar</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@if(session('actualizado'))
	<div style="display: none;" id="actualizado" name="actualizado" class="alert alert-success" data-mensaje="{{session('actualizado')}}"></div>
@endif
@if($errors->has('name'))
	<div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{$errors->first('name')}}"></div>
@endif

@endsection

@section('libreriasjavascript')
{{-- Notificaciones emergentes --}}
<!--Toastr js-->
<script src="{{url('assets/plugins/toastr/build/toastr.min.js')}}"></script>
<script src="{{url('assets/plugins/toaster/garessi-notif.js')}}"></script>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function($) {
	if($('#actualizado').length){
		$mensaje = $('#actualizado').data('mensaje');
		toastr.success($mensaje, 'inBe');
	}
	if($('#error').length){
		$mensaje = $('#error').data('mensaje');
		toastr.error($mensaje, 'inBe');
	}
});
</script>
@endsection
