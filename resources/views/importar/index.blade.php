@extends('layout.index')

@section('libreriascss')

{{-- Notificaciones emergentes --}}
<!--Toastr css-->
<link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">
@endsection

@section('cuerpo')
{{-- Hover Table - Headercolor Table --}}
<div class="row">
	<div class="col-lg-12">
		<div class="card">
        {{--
        <div style="display:scroll; position:fixed;top:170px;right:50px;"><a href="" class="btn btn-info m-b-5 m-t-5">Descargar formato modelo</a></div>
        --}}
			<div class="card-header">
				<h4>Producto - importar</h4>
			</div>
            <div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<form enctype="multipart/form-data" name="frmSubirArchivo" id="frmSubirArchivo" method="POST" action="{{route('producto.subir.archivo')}}">
						@csrf
							<div class="form-group files mb-lg-0">
								<input type="file" name="archivo" id="archivo" class="form-control1">
							</div>
							<!-- <div class="form-group">
								<input type="text" name="archivo" id="archivo" class="form-control1">
							</div> -->
						</form>
					</div>
                </div>
				<div><button name="btnImportar" id="btnImportar" type="button" disabled class="btn btn-info m-b-5 m-t-5">Importar</button>
				<a href="{{route('producto.exportar.formato')}}" class="btn btn-warning m-b-5 m-t-5">Descargar formato modelo</a></div>
            </div>
        </div>
	</div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div style="display: none;" id="completado" name="completado" class="alert alert-success" data-mensaje="Archivo importado correctamente"></div>
<div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="Importacion incorrecta"></div>

@endsection

@section('libreriasjavascript')
{{-- Notificaciones emergentes --}}
<!--Toastr js-->
<script src="{{url('assets/plugins/toastr/build/toastr.min.js')}}"></script>
<script src="{{url('assets/plugins/toaster/garessi-notif.js')}}"></script>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function($) {
	// if($('#actualizado').length){
	// 	$mensaje = $('#actualizado').data('mensaje');
	// 	toastr.success($mensaje, 'inBe');
	// }
	// if($('#error').length){
	// 	$mensaje = $('#error').data('mensaje');
	// 	toastr.error($mensaje, 'inBe');
	// }

	$("#archivo").change(function(){
		if($(this).val()!=""){
	 		$('#btnImportar').removeAttr('disabled');
		}else{
			$('#btnImportar').attr('disabled', 'disabled');
		}
	});

	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

	$("#btnImportar").click(function(){
		$('#btnImportar').attr('disabled', 'disabled');
		// $('#frmSubirArchivo').submit();


		// var formulario = $('#frmSubirArchivo').serialize();
		var formulario = new FormData(jQuery('#frmSubirArchivo')[0]);
		//console.log(formulario);
		var url = $('#frmSubirArchivo').attr('action');
		// var formulario = new FormData($("#frmSubirArchivo"));

		$.ajax({
			type: 'POST',
			url: url,
			data: formulario,
			dataType: 'json',
			success: function(data){
				if(data=="completado"){
					$mensaje = $('#completado').data('mensaje');
					toastr.success($mensaje, 'inBe');
					$("#frmSubirArchivo")[0].reset();
				}else{

					$('#frmSubirArchivo')[0].reset();
					$mensaje = $('#error').data('mensaje');
					toastr.error($mensaje, 'inBe');
				}

			},
            cache: false,
            contentType: false,
            processData: false,
			async: true,
		});

		// $.post('{{route('producto.subir.archivo')}}', $formulario)
		// 	.done(function(data){
		// 		console.log(data);
		// 	});

	});

});
</script>
@endsection
