@extends('layout.index')

@section('libreriascss')
<link rel="stylesheet" href="{{url('assets/css/smart_wizard.css')}}">
<link rel="stylesheet" href="{{url('assets/css/smart_wizard_theme_arrows.css')}}">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" > -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<style>
	.row {   
	    display: block;
	}
/* smart_wizard*/
	.ocultar{
		display:none;
	}
	.btn-finish{
	    color: #fff;
	    background-color: #01b8ff;
	    border-color: #01b8ff;
	}
	.btn-finish:hover{
	    color:#01b8ff;
	    background-color: #fff;
	    border-color: #01b8ff !important;
	}
	.btn-Cancel{
		color: #fff;
		background-color: #5458b3;
	    border-color: #5458b3;
	}
	.btn-Cancel:hover{
		color: #5458b3;
		background-color: #fff;
	    border-color: #5458b3 !important;
	}
	.list-unstyled{
		color:red; 
	}
	.nav-tabs{
		background: linear-gradient(#3d4192, #5d61bf); 
		border-radius: 15px 15px 0px 0px;
	}
	.sw-theme-default .sw-toolbar {
    	border-radius: 0px 0px 15px 15px !important;
	}
	.sw-main {
    	border-radius: 15px 15px 15px 15px !important;
	}

/* smart_wizard*/
/* buscador */
	.boxx .dropdown-menu{
	    width: 100%;
	    margin: auto;
	    padding-left: 15px;

	}
	.input-oculto{

		display: none;
	}
/* buscador */

</style>



@endsection


@section('cuerpo')
{{-- Hover Table - Headercolor Table --}}
<div class="row">
	<form action="{{ route('servicios_al_cliente.recibir')}}" id="myForm" class="needs-validation" role="form" data-toggle="validator"  method="post" accept-charset="utf-8" novalidate>
	@csrf
		<div id="smartwizard" >
		    <ul>

		        <li><a href="#step-1">Seleccionar cliente<br /><small></small></a></li>
		        <li><a href="#step-2">Formulacion<br /><small></small></a></li>
		        <li><a href="#step-3">Recomendaciones<br /><small></small></a></li>
		        <li><a href="#step-4">Recomendar producto<br /><small></small></a></li>
		    </ul>


		    <div>
		        <div id="step-1" class="">

	   				<div  id="form-step-0" role="form" data-toggle="validator">

						<div class="form-row" >
							<div class="form-group col-md-8" >
								<input type="text" class="form-control" id="buscar" placeholder="Buscar cliente"  >
								<div class="boxx form-group" id="countryList">
	    						</div>

					  		</div>
					  	</div>
					  	<div class="form-row">
					  	  	<div class="form-group col-md-6">
					  	  	  <label for="inputEmail4" id="label-nombre" class="input-oculto">Nombre</label>
								  <input type="text" class="form-control input-oculto" id="nombre" readonly="readonly"readon value="" required>
								  <input type="hidden"class="form-control" id="idcliente" name="nombreid"   value="">
					  	  	</div>
					  	  	<div class="form-group col-md-6">
					  	  	  <label for="inputPassword4" id="label-email" class="input-oculto">Email</label>
					  	  	  <input type="text" class="form-control input-oculto" id="email" name="dni"readonly="readonly" value="" required>
					  	  	</div>
					  	</div>
					  	<div class="form-row">

					  	  	<div class="form-group col-md-4">
					  	  	  	<label for="selec_satisfaccion">Nivel de satisfacción</label>
					  	  	  	<select id="selec_satisfaccion" name="selec_satisfaccion" class="form-control">
					  	  	  	<option>1</option>
					  	  	  	<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>

					  	  	  	</select>
							</div>

							<div class="form-group col-md-12 ">
									<label>Mensaje</label>
									<textarea class="form-control" id="mensaje" name="mensaje" rows="3" required></textarea>
									<div class="help-block with-errors"></div>

					    	</div>


					  	</div>

					</div>



		        </div>
		        <div id="step-2" class="">
	
	   				<div id="form-step-1" role="form" data-toggle="validator">

					  	<div class="form-row">
					    	<div class="form-group input-group-lg col-md-6">
					    	  <label >Nombre del servicio</label>
							  <input type="text" class="form-control " name="formulanombre" id="formulanombre"  placeholder="" required>
							  <div class="help-block with-errors"></div>

					    	</div>

					    	<div class="form-group col-md-6">
					    	  <label >Marca</label>
							  <select name="formulamarca" id="formulamarca" class="form-control input-lg dynamic" data-dependent="formulacategoria">
							  <option selected>Seleccione marca</option>
							  	@foreach($marca_list as $m)
	     							<option value="{{ $m->id }}" >{{ $m->nombre }}</option>
	     						@endforeach
							  </select>
							  <div class="help-block with-errors"></div>
					    	</div>

							<div class="form-group col-md-6">
					    	  <label >Categoria</label>
					    	  <select name="formulacategoria" id="formulacategoria" class="form-control input-lg dynamip" data-dependent="formulaproducto">
							  </select>
							  <div class="help-block with-errors"></div>
					    	</div>

							<div class="form-group col-md-6">
					    	  <label >Producto</label>
							  <select  id="formulaproducto" class="form-control input-lg "required>

							  </select>
							  <div class="help-block with-errors"></div>
					    	</div>
						</div>

							<div class="form-group col-md-12">
								<button class="btn btn-success" onclick="addRow2(this.form);" type="button" id="button-addon1">Agregar formula</button>
							</div>
							<!-- step-2 agregar  -->
							<div class="form-group col-md-12">
								<div id="itemRows2"></div>
							</div>



						<div class="form-group col-md-12 ">
							<label>Descripción de proceso</label>
							<textarea class="form-control" id="" name="Descripcion" rows="3"required ></textarea>
							<div class="help-block with-errors"></div>
					    </div>

					</div>


		        </div>
		        <div id="step-3" class="">
	
	   				<div id="form-step-2" role="form" data-toggle="validator">

					  	<div class="form-row">

						  	<div class="form-group col-md-6">
					    	  <label for="inputState">Hacer</label>
					    	  <select id="add_qty3" class="custom-select custom-select-lg dynamichacer" data-dependent="add_name3">
							  		<option selected>Seleccione acción</option>
					    	    	<option>Hacer</option>
									<option>No hacer</option>
					    	  </select>
					    	</div>

						  	<div class="form-group  col-md-6">
	  						  <label for="validationTextarea">Descripción de uso</label>
								<select name="formulacategoria" id="add_name3" class="form-control input-lg "  required>
							  	</select>
								<div class="help-block with-errors"></div>
	  						</div>


						</div>

						<div class="form-group col-md-12">
							<button class="btn btn-success" onclick="addRow3(this.form);" type="button" id="button-addon1">Agregar producto</button>
						</div>
						<!-- step-3 agregar  -->
						<div class="form-group col-md-12">
							<div id="itemRows3"></div>
						</div>

					</div>


		        </div>
		        <div id="step-4" class="">
	

					<div id="form-step-3" role="form" data-toggle="validator">
					  	<div class="form-row">

					    	<div class="form-group col-md-4">
					    	  <label for="inputState">Marca</label>
							  <select name="usemarca" id="usemarca" class="form-control input-lg dynamicu" data-dependent="usecategoria">
							  <option selected>Seleccione marca</option>
							  	@foreach($marca_list as $m)
	     							<option value="{{ $m->id }}" >{{ $m->nombre }}</option>
	     						@endforeach
					    	  </select>
					    	</div>

							<div class="form-group col-md-4">
					    	  <label for="inputState">Categoria</label>
					    	  <select name="usecategoria" id="usecategoria" class="form-control input-lg dynamipu" data-dependent="useproducto">
							  </select>
							  <div class="help-block with-errors"></div>
					    	</div>

							<div class="form-group col-md-4">
					    	  <label >Producto</label>
							  	<select name="useproducto" id="useproducto" class="form-control input-lg ">

								</select>
								<div class="help-block with-errors"></div>
					    	</div>
						</div>

						<div class="form-group col-md-12">
							<button class="btn btn-success" onclick="addRow4(this.form);" type="button" id="button-addon1">Agregar producto</button>
						</div>
						<!-- step-4 agregar  -->
						<div class="form-group col-md-12">
							<div id="itemRows4"></div>
						</div>
					</div>





		        </div>

		    </div>

		</div>
	</form >
</div>
	<!-- mi modal -->


		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Tienes un error</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      </div>
		    </div>
		  </div>
		</div>


	<!-- mi modal -->

	<input type="hidden" value="{{ csrf_token()}}" name="_token">
	<input type="hidden" value="{{ route('servicios_al_cliente.buscar')}}" id="url">
	<input type="hidden" value="{{ route('servicios_al_cliente.mostrar')}}" id="url2">
	<input type="hidden" value="{{ route('servicios_al_cliente.cformula')}}" id="url3">

@endsection

@section('libreriasjavascript')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{url('assets/js/jquery.smartWizard.min.js')}}"></script>
<script src="{{url('assets/js/validator.js')}}"></script>

<script>

//carga dinamica destep-1
		   $(document).ready(function(){
				   
				$('#buscar').keyup(function(){ 
					var url=$("#url").val();
					   var query = $(this).val();
					   
					   if(query != '')
					   {
						var _token = $('input[name="_token"]').val();
						$.ajax({
						 url:url,
						 method:"POST",
						 data:{query:query, _token:_token},
						 success:function(data){
							
						  	$('#countryList').fadeIn();  
							$('#countryList').html(data);
							$('#nombre').addClass('input-oculto');
							$('#email').addClass('input-oculto');
							$('#label-nombre').addClass('input-oculto');
							$('#label-email').addClass('input-oculto');
							$('#idcliente').attr('value', '');
							$('#nombre').attr('value', '');
							$('#email').attr('value', '');

						 }
						});
					   }
				  });
			  
				$(document).on('click', '.usuariosclientes', function(){  
				   $('#buscar').val($(this).text());
				   $('#countryList').fadeOut();  
					var mostrar=$(this).text();
					var url2=$("#url2").val();
					
					if(mostrar != '')
					   {
						var _token = $('input[name="_token"]').val();
						$.ajax({
						 url:url2,
						 method:"POST",
						 data:{mostrar:mostrar, _token:_token},
						 success:function(data){
							 
							if(data != '')
							{
								$('#nombre').removeClass('input-oculto');
								$('#email').removeClass('input-oculto');
								$('#label-nombre').removeClass('input-oculto');
								$('#label-email').removeClass('input-oculto');
								$('#idcliente').val(data[0]['id']);
								$('#nombre').val(data[0]['name']);
								$('#email').val(data[0]['email']);
							}else{

								$('#myModal').modal("show");
								$("#myModal").appendTo("body");
								$( ".modal-body" ).empty();
								$('.modal-body').append("Cliente no existente");
							}
							
						 }
						});
					   }

				});  

			});
//carga dinamica destep-1

//carga dinamica destep-2
			
		$(document).ready(function(){
			//categoria//
				$('.dynamichacer').change(function(){
				 	if($(this).val() != '')
				 	{
				 	 	var select = $(this).attr("id");
				 	 	var value = $(this).val();
				 	 	var dependent = $(this).data('dependent');
				 	 	var _token = $('input[name="_token"]').val();
				 	 	$.ajax({
				 	 	 	url:"{{route('servicios_al_cliente.hacer')}}",
				 	 	 	method:"POST",
				 	 	 	data:{select:select, value:value, _token:_token, dependent:dependent},

				 	 	 	success:function(result)
				 	 	 	{

								$('#'+dependent).html(result);
				 	 	 	}
						   
				 	 	})
				 	}
				});
			//categoria//

			//producto//

				$('.dynamip').change(function(){
				 	if($(this).val() != '')
				 	{
				 	 	var select = $(this).attr("id");
						  var value = $(this).val();
						  var dataid = 0;
				 	 	var dependent = $(this).data('dependent');
				 	 	var _token = $('input[name="_token"]').val();
				 	 	$.ajax({
				 	 	 	url:"{{route('servicios_al_cliente.pformula')}}",
				 	 	 	method:"POST",
				 	 	 	data:{select:select, value:value, _token:_token, dependent:dependent},

				 	 	 	success:function(result)
				 	 	 	{
								
								$('#'+dependent).html(result);
				 	 	 	}
						   
				 	 	})
				 	}
				});
			//producto//


		$('#formulamarca').change(function(){
		 $('#formulacategoria').val('');
		 $('#formulaproducto').val('');
		});
		$('#formulacategoria').change(function(){
		 $('#formulaproducto').val('');
		});






	});	

//carga dinamica destep-2


//carga dinamica destep-3
			
	$(document).ready(function(){
			//categoria//
				$('.dynamic').change(function(){
				 	if($(this).val() != '')
				 	{
				 	 	var select = $(this).attr("id");
				 	 	var value = $(this).val();
				 	 	var dependent = $(this).data('dependent');
				 	 	var _token = $('input[name="_token"]').val();
				 	 	$.ajax({
				 	 	 	url:"{{route('servicios_al_cliente.cformula')}}",
				 	 	 	method:"POST",
				 	 	 	data:{select:select, value:value, _token:_token, dependent:dependent},

				 	 	 	success:function(result)
				 	 	 	{

								$('#'+dependent).html(result);
				 	 	 	}
						   
				 	 	})
				 	}
				});
			//categoria//



		$('#add_qty3').change(function(){
		 $('#add_name3').val('');
		});


	});	

//carga dinamica destep-3



//carga dinamica destep-4

	$(document).ready(function(){

		//categoria//
			$('.dynamicu').change(function(){
				if($(this).val() != '')
				{
				 	var select = $(this).attr("id");
				 	var value = $(this).val();
				 	var dependent = $(this).data('dependent');
				 	var _token = $('input[name="_token"]').val();
				 	$.ajax({
				 	 	url:"{{route('servicios_al_cliente.cuse')}}",
				 	 	method:"POST",
				 	 	data:{select:select, value:value, _token:_token, dependent:dependent},
				 	 	success:function(result)
				 	 	{
							$('#'+dependent).html(result);
				 	 	}
					   
				 	})
				}
			});
		//categoria//


		//producto//
			$('.dynamipu').change(function(){
			 	if($(this).val() != '')
			 	{
			 	 	var select = $(this).attr("id");
			 	 	var value = $(this).val();
			 	 	var dependent = $(this).data('dependent');
			 	 	var _token = $('input[name="_token"]').val();
			 	 	$.ajax({
			 	 	 	url:"{{route('servicios_al_cliente.puse')}}",
			 	 	 	method:"POST",
			 	 	 	data:{select:select, value:value, _token:_token, dependent:dependent},
			 	 	 	success:function(result)
			 	 	 	{
							$('#'+dependent).html(result);
			 	 	 	}
					   
			 	 	})
			 	}
			});
		//producto//

		$('#usemarca').change(function(){
		 $('#usecategoria').val('');
		 $('#useproducto').val('');
		});
		$('#usecategoria').change(function(){
		 $('#useproducto').val('');
		});



	});

//carga dinamica destep-4


//step-2 agregar

		var row2Num = 0;		
		var n2=0;
		function addRow2(frm) {
			if($('#formulaproducto').val())
			{	
				row2Num ++;		
			var nombre = $("#copyproducto").attr("name");
			var row = '<div class="form-row contar2" id="rowNum2'+row2Num+'"><div class="form-group col-md"><input class="form-control" type="text" name="productonombre[]" size="4" value="'+frm.formulaproducto.value+'" disabled><input name="productoid[]" type="hidden" value="'+nombre+'"></div><div class="form-group col-md"><input class="form-control" id="formulacantidad" type="number" name="formulacantidad[]" placeholder="Valor" required><div class="help-block with-errors"></div></div><div class="form-group col-md"><select class="form-control "name="formulatipo[]" ><option selected>Onza</option><option>Gramos</option></select></div><div class="form-group col-md-1"><button class="btn btn-danger" type="button" value="Remove" onclick="removeRow('+row2Num+');">X</button><div class="form-group col-md-1"></div>';

		    jQuery('#itemRows2').append(row);
			n2 = $(".contar2").length;
			
			}
			
		}
		
		function removeRow(rnum) {
			jQuery('#rowNum2'+rnum).remove();
			n2 = $(".contar2").length;
			
		}
	
//step-2 agregar
</script>
<script>
//step-3 agregar

		var row3Num = 0;
		var n3=0;
		function addRow3(frm) {

			if($('#add_name3').val())
			{
		    	row3Num ++;
		    	var row = '<div class="form-row contar3" id="rowNum3'+row3Num+'"><div class="form-group col-md"><input class="form-control" type="text" name="hacertipo[]" size="4" value="'+frm.add_qty3.value+'" disabled><input class="form-control" name="hacertipo[]" type="hidden" size="4" value="'+frm.add_qty3.value+'" ></div><div class="form-group col-md"><input class="form-control" value="'+frm.add_name3.value+'" placeholder="Valor" name="quehacerdetalles[]" disabled><input class="form-control" name="quehacerdetalles[]" type="hidden" value="'+frm.add_name3.value+'"  ></div><div class="form-group col-md-1"><button class="btn btn-danger" type="button" value="Remove" onclick="removeRow3('+row3Num+');">X</button><div class="form-group col-md-1"></div>';
				jQuery('#itemRows3').append(row);
				n3 = $(".contar3").length;

			}
		    }
		
		function removeRow3(rnum) {
			jQuery('#rowNum3'+rnum).remove();
			n3 = $(".contar3").length;
			}
	
//step-3 agregar
 </script>
 <script>
//step-4 agregar

		var row4Num = 0;
		var n4=0;
		function addRow4(frm) {

			if($('#useproducto').val())
			{
				row4Num ++;
				var nombre = $("#usecopyproducto").attr("name");
		    	var row = '<div class="form-row contar4" id="rowNum4'+row4Num+'"><div class="form-group col-md"><input class="form-control" type="text" name="productorecomendado[]" size="4" value="'+frm.useproducto.value+'" disabled><input type="hidden" name="iduseproducto[]" value="'+nombre+'"></div><div class="form-group col-md"><input class="form-control" type="number" placeholder="Valor" id="usorecomendado" name="usorecomendado[]" required><div class="help-block with-errors"></div></div><div class="form-group col-md"><select class="form-control" name="tipousorecomendado[]"><option>Diario</option><option>Semanal</option><option>Mensual</option></select></div><div class="form-group col-md-1"><button class="btn btn-danger" type="button" value="Remove" onclick="removeRow4('+row4Num+');">X</button><div class="form-group col-md-1"></div>';
				jQuery('#itemRows4').append(row);
				n4 = $(".contar4").length;
				
			}

		}
		
		function removeRow4(rnum) {
			jQuery('#rowNum4'+rnum).remove();
			n4 = $(".contar4").length;
		}
	
//step-4 agregar
</script>


<script type="text/javascript">
// Smart Wizard




  $(document).ready(function() {
  			
		// botones // Smart Wizard
		 	var btnFinish = $('<button></button>').text('Guardar')
                                             .addClass('btn btn-finish ocultar')
                                             .on('click', function(){
                                                    if( !$(this).hasClass('disabled')){
                                                        var elmForm = $("#myForm");
                                                        if(elmForm){
                                                            elmForm.validator('validate');
                                                            var elmErr = elmForm.find('.has-error');
                                                            if(elmErr && elmErr.length > 0){
																$('#myModal').modal("show");
																$("#myModal").appendTo("body");
																$( ".modal-body" ).empty();
																$('.modal-body').append("Tiene un error en el formulario");
                                                                return false;
															}if(n2 === 0)
															{
																$('#myModal').modal("show");
																$("#myModal").appendTo("body");
																$( ".modal-body" ).empty();
																$('.modal-body').append("Debe agregar al menos una formula");
																return false;
																

															}if(n3 === 0)
															{
																$('#myModal').modal("show");
																$("#myModal").appendTo("body");
																$( ".modal-body" ).empty();
																$('.modal-body').append("Debe agregar al menos una recomendación");
																return false;
																
															}if(n4 === 0)
															{
																$('#myModal').modal("show");
																$("#myModal").appendTo("body");
																$( ".modal-body" ).empty();
																$('.modal-body').append("Debe recomendar un producto");
																return false;
																
															}if($('#formulacantidad').val() === '')
															{
																$('#myModal').modal("show");
																$("#myModal").appendTo("body");
																$( ".modal-body" ).empty();
																$('.modal-body').append("El numero de la cantidad de la formula no es valido");
																return false;
																
															}if($('#usorecomendado').val() === '')
															{
																$('#myModal').modal("show");
																$("#myModal").appendTo("body");
																$( ".modal-body" ).empty();
																$('.modal-body').append("El numero de la cantidad del producto no es valido");
																return false;
																
															}else{
																$('#myModal').modal("show");
																$("#myModal").appendTo("body");
																$( ".modal-body" ).empty();
																$('.modal-body').append("Formuliario listo");
                                                                elmForm.submit();
                                                                return false;
                                                            }
                                                        }
                                                    }
                                                });
        	var btnCancel = $('<button></button>').text('Cancel')
                                             .addClass('btn btn-Cancel')
                                             .on('click', function(){
                                                    $('#smartwizard').smartWizard("reset");
                                                    $('#myForm').find("input, textarea").val("");
                                                });

	  
		// botones// Smart Wizard
      
      	// Smart Wizard// Asistente inteligente
      		
      		$('#smartwizard').smartWizard({
      		//$ ('# smartwizard'). smartWizard ({
      		      selected: 0,  // Initial selected step, 0 = first step 
      		      //seleccionado: 0, // Paso inicial seleccionado, 0 = primer paso 
      		      keyNavigation:false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
      		      //keyNavigation: true, // Habilitar / Deshabilitar la navegación con el teclado (las teclas izquierda y derecha se usan si están habilitadas)
      		      autoAdjustHeight:true, // Automatically adjust content height
      		     // autoAdjustHeight: true, // Ajustar automáticamente la altura del contenido
      		      cycleSteps: false, // Allows to cycle the navigation of steps
      		      //cycleSteps: false, // Permite alternar la navegación de los pasos
      		      backButtonSupport: true, // Enable the back button support
      		      //backButtonSupport: true, // Habilitar la compatibilidad con el botón de retroceso
      		      useURLhash: true, // Enable selection of the step based on url hash
      		      //useURLhash: true, // Habilitar la selección del paso basado en urh hash
      		      lang: {  // Language variables
      		      //lang: {// Variables de lenguaje
      		          next: 'Continuar', 
      		          previous: 'Atras'
      		      },
      		      toolbarSettings: {
      		          toolbarPosition: 'bottom', // none, top, bottom, both
      		          toolbarButtonPosition: 'right', // left, right
      		          showNextButton: true, // show/hide a Next button
      		          showPreviousButton: true, // show/hide a Previous button
      		          toolbarExtraButtons: [btnFinish, btnCancel]
					},
					
      		      anchorSettings: {
      		          anchorClickable: true, // Enable/Disable anchor navigation
      		          enableAllAnchors: false, // Activates all anchors clickable all times
      		          markDoneStep: true, // add done css
						enableAnchorOnDoneStep: true, // Enable/Disable the done steps navigation
      		          markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
      		          removeDoneStepOnNavigateBack: true // While navigate back done step after active step will be cleared
					
      		      },            
      		      contentURL: null, // content url, Enables Ajax content loading. can set as data data-content-url on anchor
      		      disabledSteps: [],    // Array Steps disabled
      		      errorSteps: [],    // Highlight step with errors
      		      theme: 'default',
      		      transitionEffect: 'fade', // Effect on navigation, none/slide/fade
      		      transitionSpeed: '400'
			  }); 
		// Smart Wizard// Asistente inteligente	   

	  	//validate
	 		$("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
					var elmForm = $("#form-step-" + stepNumber);

     		      	// stepDirection === 'forward' :- this condition allows to do the form validation
					   // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
				if($('#idcliente').val()){	   
     		      	if(stepDirection === 'forward' && elmForm){
						elmForm.validator('validate');

						var elmErr = elmForm.find('.has-error');
     		      	    if(elmErr && elmErr.length > 0){
						
						
     		      	        // Form validation failed
     		      	        return false;
     		      	    }
     		      	}
					return true;
				}

					$('#myModal').modal("show");
					$("#myModal").appendTo("body");
					$( ".modal-body" ).empty();
					$('.modal-body').append("Busque y elija un cliente");

				return false;	

			 });
			 $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
					// Enable finish button only on last step

        	        if(stepNumber == 3){
        	            $('.btn-finish').removeClass('ocultar');
        	        }else{
        	            $('.btn-finish').addClass('ocultar');
        	        }
			});
		//validate
      
  }); 


</script>

  <script type="text/javascript">
  	$(document).ready(function() {
		// Initialize Smart Wizard 

		
	  $('#smartwizard').smartWizard();
	   
    	
		
      // Initialize the beginReset event
      $("#smartwizard").on("beginReset", function(e) {
		  
		  
	 return confirm("Do you want to reset the wizard?");
      });
            
      // Initialize the endReset event
      $("#smartwizard").on("endReset", function(e) {
		  
	 alert("endReset called");
      });  
            
      // Initialize the themeChanged event
      $("#smartwizard").on("themeChanged", function(e, theme) {
	 alert("Theme changed. New theme name: " + theme);
	  });
		// validar Smart Wizard 



		// validar Smart Wizard 
      
  	}); 

	</script>





@endsection

@section('javascript')





@endsection
