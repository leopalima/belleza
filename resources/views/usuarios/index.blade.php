@extends('layout.index')

@section('libreriascss')
{{-- Formularios --}}
<!--Morris css-->
<link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
{{-- Formularios avanzados --}}
<!--Bootstrap-daterangepicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">

<!--Bootstrap-datepicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}">

<!--iCheck css-->
<link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}">

<!--Bootstrap-colorpicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}">

<!--Bootstrap-timepicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">

<!--Select2 css-->
<link rel="stylesheet" href="{{url('assets/plugins/select2/select2.css')}}">

{{-- Wysiwing Editor --}}
<!--Bootstrap-wysihtml5 css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

{{-- Notificaciones emergentes --}}
<!--Toastr css-->
<link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">


@endsection

@section('cuerpo')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
		{{--  <div style="display:scroll; position:fixed;bottom:100px;right:50px;"><a href="{{ route('usuarios.create')}}" class="btn btn-info m-b-5 m-t-5">Nuevo</a></div>  --}}
			<div class="card-header">
				<h4>Usuarios</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover mb-0 text-nowrap">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Email</th>
							<th>Tipo</th>
							<th>Location</th>
							<th>Phone</th>
							<th>Instagram</th>
							<th></th>
						</tr>
						@foreach($usuarios as $u)
						<tr>
							<td></td>
							<td>{{$u->name}}</td>
							<td>{{$u->email}}</td>
							<td>{{$u->tipo}}</td>
							<td>{{$u->location}}</td>
							<td>{{$u->phone}}</td>
							<td>{{$u->instagram}}</td>
							<td>
								<button id="btnEditar" class="btn btn-info" data-url="{{action('usuarios\UsuariosController@show', ['id' => $u->id])}}">Editar</button>
							</td>
						</tr>
						@endforeach
                    </table>
                    <div>
                        {{ $usuarios->links() }}
                    </div>
                    <div>
                        <a href="{{ route('usuarios.create')}}" class="btn btn-info m-b-5 m-t-5">Nuevo</a>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('libreriasjavascript')

<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Editar usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="frmEditar" method="POST" action="">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Nombre:</label>
                        <input type="text" class="form-control" id="enombre" name="enombre">
                        <input type="hidden" class="form-control" name="_method" value="">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Email:</label>
                        <input type="text" class="form-control" id="eemail" name="eemail">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Phone:</label>
                        <input type="text" class="form-control" id="ephone" name="ephone">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Instagram:</label>
                        <input type="text" class="form-control" id="einstagram" name="einstagram">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Location:</label>
                        <input type="text" class="form-control" id="elocation" name="elocation">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Tipo:</label>
                        <select class="form-control" id="etipo" name="etipo">
                            <option value="cliente">Cliente</option>
                            <option value="profesional">Profesional</option>
                            <option value="admin">Administrador</option>
                        </select>
                    </div>
                    <div id="divCategorias">
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Categorías:</label>
                    </div>
                    @foreach($categorias as $categoria)
                    <div class="form-group">
                        <label>
                            <input type="checkbox" class="minimal" id="echkFormulacion{{$categoria->id}}" name="ecategorias[]" value="{{$categoria->id}}">
                            {{$categoria->nombre}}
                        </label>
                    </div>
                    @endforeach
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                <button id="btnGuardarEditar" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>



{{-- Formularios avanzados --}}
<!--Select2 js-->
<script src="{{url('assets/plugins/select2/select2.full.js')}}"></script>
<!--Inputmask js-->
<script src="{{url('assets/plugins/inputmask/jquery.inputmask.js')}}"></script>
<!--Moment js-->
<script src="{{url('assets/plugins/moment/moment.min.js')}}"></script>
<!--Bootstrap-daterangepicker js-->
<script src="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!--Bootstrap-datepicker js-->
<script src="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<!--Bootstrap-colorpicker js-->
<script src="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
<!--Bootstrap-timepicker js-->
<script src="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
<!--iCheck js-->
<script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
<!--forms js-->
<script src="{{url('assets/js/forms.js')}}"></script>

{{-- Wysiwing Editor --}}
<!--ckeditor js-->
<script src="{{url('assets/plugins/tinymce/tinymce.min.js')}}"></script>
<!--Scripts js-->
<script src="{{url('assets/js/formeditor.js')}}"></script>

{{-- Notificaciones emergentes --}}
<!--Toastr js-->
<script src="{{url('assets/plugins/toastr/build/toastr.min.js')}}"></script>
<script src="{{url('assets/plugins/toaster/garessi-notif.js')}}"></script>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function($) {
    $.each($("#btnEditar"), function(){
        $("table").delegate("#btnEditar", "click", function(){
            // console.log($(this).data("url"));
            $("#modalEditar").modal("toggle");
            show($(this).data("url"));

        })
    })

    $("#btnGuardarEditar").click(function(){
        $url = $("#frmEditar").attr("action");
        update($url);
    });

    $("#frmEditar select").change(function(){
        if($(this).val()=="profesional"){
            $("#divCategorias").prop("hidden", false);
        }else{
            $('input[name="ecategorias[]"]').iCheck('uncheck');
            $("#divCategorias").prop("hidden", true);
        }
    });




function show($url){
    $url = $url;
    $.ajax({
        url: $url,
        method: "GET",
        dataType: "JSON",
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    }).done(function(d){
        
        $("#enombre").val(d.name);
        $("#eemail").val(d.email);
        $("#elocation").val(d.location);
        $("#ephone").val(d.phone);
        $("#einstagram").val(d.instagram);
        $("#frmEditar select").val(d.tipo);
        $("#frmEditar").attr("action", $url);

        $('input[name="ecategorias[]"]').iCheck('uncheck');
        if(d.tipo == "profesional"){
            $("#divCategorias").attr("hidden", false);
            for($i=0; $i < d.miscategorias.length; $i++){
                if($('input[id="echkFormulacion'+ d.miscategorias[$i].categoria_id +'"]').val() == d.miscategorias[$i].categoria_id){
                    $('input[id="echkFormulacion'+ d.miscategorias[$i].categoria_id +'"]').iCheck('check');
                }
            }
        }else{
            $("#divCategorias").attr("hidden", true);
        }
    });
}

function update($url){
    $url = $url;
    $data = $("#frmEditar").serialize();
    $.ajax({
        url: $url,
        method: "PUT",
        data: $data,
        dataType: "JSON",
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    }).done(function(d){
        if(d.exito == 1){
            location.reload();
        }
        if(d.error == 0){
            console.log("error");
        }
    });
}

});
</script>
@endsection
