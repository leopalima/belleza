@extends('layout.index')

@section('libreriascss')
{{-- Formularios --}}
<!--Morris css-->
<link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
{{-- Formularios avanzados --}}
<!--Bootstrap-daterangepicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">

<!--Bootstrap-datepicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}">

<!--iCheck css-->
<link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}">

<!--Bootstrap-colorpicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}">

<!--Bootstrap-timepicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">

<!--Select2 css-->
<link rel="stylesheet" href="{{url('assets/plugins/select2/select2.css')}}">

{{-- Wysiwing Editor --}}
<!--Bootstrap-wysihtml5 css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

{{-- Notificaciones emergentes --}}
<!--Toastr css-->
<link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">


@endsection

@section('cuerpo')
{{-- iCheck - Checkbox & Radio Inputs --}}
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-header">
				<h4>Mis categorías</h4>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-6">
                    <form method="POST" action="{{action('usuarios\MisCategoriasController@modificarCategorias', ['id' => auth::user()->id])}}">
                    @csrf
                        @php
                            $mis = auth::user()->miscategorias;
                        @endphp
                        @foreach($categorias as $c)
						<div class="form-group">
							<label>
                                @php
                                    $cat = $c->id;
                                    $positivo = $mis->filter(function($v, $k) use($cat){
                                       return $v->categoria_id == $cat;
                                    });
                                @endphp
								<input name="categorias[]" value="{{$c->id}}" type="checkbox" class="minimal" {{count($positivo) > 0 ? 'checked' : ''}} >
                                {{$c->nombre}}
							</label>
						</div>
                        @endforeach

                        <button type="submit" class="btn btn-primary mt-1 mb-0">Guardar</button>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection

@section('libreriasjavascript')
{{-- Formularios avanzados --}}
<!--Select2 js-->
<script src="{{url('assets/plugins/select2/select2.full.js')}}"></script>
<!--Inputmask js-->
<script src="{{url('assets/plugins/inputmask/jquery.inputmask.js')}}"></script>
<!--Moment js-->
<script src="{{url('assets/plugins/moment/moment.min.js')}}"></script>
<!--Bootstrap-daterangepicker js-->
<script src="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!--Bootstrap-datepicker js-->
<script src="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<!--Bootstrap-colorpicker js-->
<script src="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
<!--Bootstrap-timepicker js-->
<script src="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
<!--iCheck js-->
<script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
<!--forms js-->
<script src="{{url('assets/js/forms.js')}}"></script>

{{-- Wysiwing Editor --}}
<!--ckeditor js-->
<script src="{{url('assets/plugins/tinymce/tinymce.min.js')}}"></script>
<!--Scripts js-->
<script src="{{url('assets/js/formeditor.js')}}"></script>

{{-- Notificaciones emergentes --}}
<!--Toastr js-->
<script src="{{url('assets/plugins/toastr/build/toastr.min.js')}}"></script>
<script src="{{url('assets/plugins/toaster/garessi-notif.js')}}"></script>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function($) {
	
});
</script>
@endsection
