@extends('layout.index')

@if (session()->has('back_previous_button'))
  {{session()->forget('back_previous_button')}}
@endif

@section('libreriascss')

@endsection

@section('cuerpo')
{{-- Hover Table - Headercolor Table --}}
<div class="row">
		<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<form class="form-horizontal" >
							<div class="form-group row">
								<div class="col-md-10">
									<input type="text" name="q" id="q" class="form-control" value="" placeholder="Search...">
								</div>
								<button type="submit" class="btn btn-primary mt-1 mb-0">Search</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		
	<div class="col-lg-12">
		<div class="card">
		{{--  <div style="display:scroll; position:fixed;bottom:100px;right:50px;"><a href="{{ route('producto.create')}}" class="btn btn-info m-b-5 m-t-5">Nuevo</a></div>
		<div style="display:scroll; position:fixed;bottom:100px;right:150px;"><a href="{{route('producto.importar')}}" class="btn btn-warning m-b-5 m-t-5">Importar</a></div>
		<div style="display:scroll; position:fixed;bottom:100px;right:275px;"><a href="{{ route('producto.exportar.productos')}}" class="btn btn-danger m-b-5 m-t-5">Exportar</a></div>  --}}
			<div class="card-header">
				<h4>Productos</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover mb-0 text-nowrap">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Marca</th>
							<th>Categoría</th>
							<th>Media</th>
							<th>Número</th>
							<th>Tipo uso</th>
							<th></th>
						</tr>
						@foreach($productos as $producto)
						<tr>
							<td>{{$producto->id}}</td>
							<td>{{$producto->nombre}}</td>
							<td>{{$producto->categoria->marca->nombre}}</td>
							<td>{{$producto->categoria->nombre}}</td>
							<td>{{$producto->medida}}</td>
							<td>{{$producto->numero}}</td>
							<td>{{$producto->tipo}}</td>
							<td>
								@if($producto->propietario->tipo == Auth::user()->tipo)
								<a class="btn btn-info" href="{{action('producto\productoController@edit', ['id' => $producto->id])}}">Editar</a>
								@endif
							</td>
						</tr>
						@endforeach
					</table>
					<div>
						{{ $productos->links() }}
					</div>
					<div>
						<a href="{{ route('producto.create')}}" class="btn btn-info m-b-5 m-t-5">Nuevo</a>
						<a href="{{route('producto.importar')}}" class="btn btn-warning m-b-5 m-t-5">Importar</a>
						<a href="{{ route('producto.exportar.productos')}}" class="btn btn-danger m-b-5 m-t-5">Exportar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('libreriasjavascript')

@endsection

@section('javascript')
@endsection
