@extends('layout.index')

@if (!session()->has('back_previous_button'))
  {{session()->put('back_previous_button', url()->previous())}}
@endif

@section('libreriascss')
{{-- Formularios --}}
<!--Morris css-->
<link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
{{-- Notificaciones emergentes --}}
<!--Toastr css-->
<link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">
@endsection

@section('cuerpo')
{{-- Formularios - General Elements --}}
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h4>Nuevo menú</h4>
            </div>
			<div class="card-body">
				<form enctype="multipart/form-data" name="frmProductos" class="form-horizontal" method="POST" action="{{action('producto\productoController@update', ['id' => $producto->id])}}">
					@csrf
					@method('PUT')
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Nombre</label>
						<div class="col-md-9">
							<input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre') ? old('nombre') : $producto->nombre}}">
						</div>
                    </div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Medida</label>
						<div class="col-md-9">
							<select class="form-control" id="medida" name="medida" data-url="{{old('medida')}}">
                                <option value="gr" {{ $producto->medida == 'gr' ? 'selected' : ''}}>gr</option>
								<option value="oz" {{ $producto->medida == 'oz' ? 'selected' : ''}}>oz</option>
							</select>
						</div>
                    </div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Numero</label>
						<div class="col-md-9">
							<input type="text" name="numero" id="numero" class="form-control" value="{{old('numero') ? old('numero') : $producto->numero}}">
						</div>
                    </div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Descripción</label>
						<div class="col-md-9">
							<input type="text" name="descripcion" id="descripcion" class="form-control" value="{{old('descripcion') ? old('descripcion') : $producto->descripcion}}">
						</div>
                    </div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Tipo uso</label>
						<div class="col-md-9">
							<select class="form-control"  id="Tipo_uso" name="Tipo_uso">
                        	        <option value="Local" {{ $producto->tipo == 'Local' ? 'selected' : ''}} >Local</option>
									<option value="Cliente" {{ $producto->tipo == 'Cliente' ? 'selected' : ''}}>Cliente</option>            
							</select>
						</div>
                    </div>
                    <div class="form-group row">
						<label class="col-md-3 col-form-label">Marca</label>
						<div class="col-md-9">
							<select class="form-control" id="marca" name="marca" data-url="{{route('marca.categoria')}}">
                                @foreach($marcas as $marca)
                                <option value='{{$marca->id}}' {{$marca->id == $producto->categoria->marca_id ? 'selected' : ''}}>{{$marca->nombre}}</option>
                                @endforeach
							</select>
						</div>
					</div>
                    <div class="form-group row">
						<label class="col-md-3 col-form-label">Categoria</label>
						<div class="col-md-9">
							<select class="form-control" id="categorias" name="categorias">
                                @foreach($categorias as $categoria)
                                <option value='{{$categoria->id}}' {{$categoria->id == $producto->categoria_id ? 'selected' : ''}}>{{$categoria->nombre}}</option>
                                @endforeach
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label"></label>
						<div class="col-md-9">
							<img src="{{$producto->url_foto}}" alt="Smiley face" height="80" width="80">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Imagen</label>
						<div class="col-md-9">
							<input id="url_foto" name="url_foto" type="file" accept="image/*" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label"></label>
						<div class="col-md-9">
							<button type="submit" class="btn btn-info">Guardar</button>
							{{--  <a href="{{route('producto.index')}}" class="btn btn-primary">Volver</a>  --}}
							<a href="{{ session()->get('back_previous_button')}}" class="btn btn-primary">Volver</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@if(session('actualizado'))
	<div style="display: none;" id="actualizado" name="actualizado" class="alert alert-success" data-mensaje="{{session('actualizado')}}"></div>
@endif
@if($errors->has('nombre'))
	<div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{$errors->first('nombre')}}"></div>
@endif
@if($errors->has('medida'))
	<div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{$errors->first('medida')}}"></div>
@endif
@if($errors->has('numero'))
	<div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{$errors->first('numero')}}"></div>
@endif

@endsection

@section('libreriasjavascript')
{{-- Notificaciones emergentes --}}
<!--Toastr js-->
<script src="{{url('assets/plugins/toastr/build/toastr.min.js')}}"></script>
<script src="{{url('assets/plugins/toaster/garessi-notif.js')}}"></script>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function($) {
	if($('#actualizado').length){
		$mensaje = $('#actualizado').data('mensaje');
		toastr.success($mensaje, 'inBe');
	}
	if($('#error').length){
		$mensaje = $('#error').data('mensaje');
		toastr.error($mensaje, 'inBe');
	}

    $('#marca').change(function(){
        var $id=$(this).val();
        var $url=$(this).data('url') + '/' + $id;
        $('#categorias').empty();
        $.get($url, function(data){
            for($i=0; $i<data[0].length; $i++){
                console.log(data[0][$i]['nombre']);
                $('#categorias').append($('<option>', {value:data[0][$i]['id'], text:data[0][$i]['nombre']}));
            }
        });
    })
});
</script>
@endsection
