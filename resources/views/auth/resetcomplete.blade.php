<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>inBe</title>
        
        <!--Favicon -->
        <link rel="icon" href="{{url('favicon.ico')}}" type="image/x-icon"/>

        <!--Bootstrap.min css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap/css/bootstrap.min.css')}}">

        <!--Icons css-->
        <link rel="stylesheet" href="{{url('assets/css/icons.css')}}">

        <!--Style css-->
        <link rel="stylesheet" href="{{url('assets/css/style.css')}}">

        <!--mCustomScrollbar css-->
        <link rel="stylesheet" href="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css')}}">

        <!--Sidemenu css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toggle-menu/sidemenu.css')}}">

        <style>
            .single-page {
                width:auto;
            }

            @media (max-width: 992px){
                .single-page {
                    margin: 0;
                    width: 100% !important;
                    display: block;
                }
            }
        </style>

    </head>

    <body class="bg-primary">
        <div id="app">
            <section class="section section-2">
                <div class="container">
                    <div class="row">
                        <div class="single-page single-pageimage construction-bg cover-image">
                            <div class="row" style="display:block">
                                <div class="col-lg-6" style="max-width: 100%">
                                    <div class="wrapper wrapper2">
                                            @if (session('status'))
                                                <form class="card-body" tabindex="500" style="width:100%">
                                                    <img src="{{url('assets/img/brand/logo-inbi-200x113.png')}}" class="mb-2 mt-4 mt-lg-0 " alt="logo">
                                                    <div class="alert alert-success" role="alert">
                                                        {{ session('status') }}
                                                    </div>
                                                    <div class="submit">
                                                        <a href="inbiapp://" class="btn btn-primary btn-block">
                                                            Got to app
                                                        <a/>
                                                    </div>
                                                </form>
                                            @else
                                                <form id="login" class="card-body" tabindex="500" method="POST" action="{{ route('password.email') }}">
                                                    @csrf
                                                    <img src="{{url('assets/img/brand/logo-inbi-200x113.png')}}" class="mb-2 mt-4 mt-lg-0 " alt="logo">
                                                    <h3>Reset successfull</h3>
                                                    <div class="submit">
                                                        <a href="inbiapp://" class="btn btn-primary btn-block">
                                                            Got to app
                                                        <a/>
                                                    </div>
                                                </form>
                                            @endif
                                        {{--
                                        <div class="card-body border-top">
                                            <a class="btn  btn-social btn-facebook btn-block"><i class="fa fa-facebook"></i> Acceder con Facebook</a>
                                            <a class="btn  btn-social btn-google btn-block mt-2"><i class="fa fa-google-plus"></i> Acceder con Google</a>
                                        </div>
                                        --}}
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </section>
        </div>

        <!--Jquery.min js-->
        <script src="{{url('assets/js/jquery.min.js')}}"></script>

        <!--popper js-->
        <script src="{{url('assets/js/popper.js')}}"></script>

        <!--Tooltip js-->
        <script src="{{url('assets/js/tooltip.js')}}"></script>

        <!--Bootstrap.min js-->
        <script src="{{url('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

        <!--Jquery.nicescroll.min js-->
        <script src="{{url('assets/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>

        <!--Scroll-up-bar.min js-->
        <script src="{{url('assets/plugins/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
        
        <script src="{{url('assets/js/moment.min.js')}}"></script>

        <!--mCustomScrollbar js-->
        <script src="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

        <!--Sidemenu js-->
        <script src="{{url('assets/plugins/toggle-menu/sidemenu.js')}}"></script>

        <!--Scripts js-->
        <script src="{{url('assets/js/scripts.js')}}"></script>

    </body>
</html>
