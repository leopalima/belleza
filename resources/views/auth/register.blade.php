<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>inBe</title>
        
        <!--favicon -->
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>

        <!--Bootstrap.min css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
        
        <!--Icons css-->
        <link rel="stylesheet" href="{{url('assets/css/icons.css')}}">

        <!--Style css-->
        <link rel="stylesheet" href="{{url('assets/css/style.css')}}">

        <!--mCustomScrollbar css-->
        <link rel="stylesheet" href="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css')}}">

        <!--Sidemenu css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toggle-menu/sidemenu.css')}}">

    </head>

    <body class="bg-primary">
        <div id="app">
            <section class="section">
                <div class="container">
                    <div class="row">
                        <div class="single-page construction-bg single-pageimage cover-image" data-image-src="assets/img/news/img14.jpg">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="wrapper wrapper2">
                                        <form method="POST" action="{{ route('register') }}" class="card-body" tabindex="500">
                                            @csrf
                                            <h3>Crear una cuenta de inBi!</h3>
                                            <div class="name">
                                                <input id="name" type="text" name="name" value="{{ old('name') }}" required autofocus>
                                                <label>Nombre</label>
                                            </div>
                                            @if ($errors->has('name'))
                                                <div style="text-align: left;">
                                                    <p class="h6 text-danger mb-0">{{ $errors->first('name') }}</p>
                                                </div>
                                            @endif
                                            <div class="mail">
                                                <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                                                <label>Email de usuario</label>
                                            </div>
                                            @if ($errors->has('email'))
                                                <div style="text-align: left;">
                                                    <p class="h6 text-danger mb-0">{{ $errors->first('email') }}</p>
                                                </div>
                                            @endif
                                            <div class="passwd">
                                                <input id="password" type="password" name="password" required>
                                                <label>Contraseña</label>
                                            </div>
                                            @if ($errors->has('password'))
                                                <div style="text-align: left;">
                                                    <p class="h6 text-danger mb-0">{{ $errors->first('password') }}</p>
                                                </div>
                                            @endif
                                            <div class="passwd">
                                                <input type="password" name="password_confirmation" id="password-confirm" required>
                                                <label>Confirma contraseña</label>
                                            </div>
                                            <div class="submit">
                                                <button type="submit" class="btn btn-primary btn-block">Registrarse</button>
                                            </div>
                                            <p class="text-dark mb-0">¿Ya tienes una cuenta?<a href="{{route('login')}}" class="text-primary ml-1">Iniciar sesión</a></p>
                                            
                                        </form>
                                        <div class="card-body border-top">
                                            <a class="btn  btn-social btn-google btn-block"><i class="fa fa-google-plus"></i> Acceder con Google</a>
                                            <a class="btn  btn-social btn-facebook btn-block mt-2"><i class="fa fa-facebook"></i> Acceder con Facebook</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="log-wrapper ">
                                        <div class="log-wrapper text-center ">
                                            <img src="assets/img/brand/logo-white.png" class="mb-2 mt-4 mt-lg-0 " alt="logo">
                                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                                            <a class="btn btn-primary mt-3" href="#">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </section>
        </div>

        <!--Jquery.min js-->
        <script src="{{url('assets/js/jquery.min.js')}}"></script>

        <!--popper js-->
        <script src="{{url('assets/js/popper.js')}}"></script>

        <!--Tooltip js-->
        <script src="{{url('assets/js/tooltip.js')}}"></script>

        <!--Bootstrap.min js-->
        <script src="{{url('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

        <!--Jquery.nicescroll.min js-->
        <script src="{{url('assets/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>

        <!--Scroll-up-bar.min js-->
        <script src="{{url('assets/plugins/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
        <script src="{{url('assets/js/moment.min.js')}}"></script>

        <!--mCustomScrollbar js-->
        <script src="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

        <!--Sidemenu js-->
        <script src="{{url('assets/plugins/toggle-menu/sidemenu.js')}}"></script>

        <!--Scripts js-->
       <!--  <script src="{{url('assets/js/scripts.js')}}"></script> -->

    </body>
</html>