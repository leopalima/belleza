<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>inBe</title>
        
        <!--Favicon -->
        <link rel="icon" href="{{url('favicon.ico')}}" type="image/x-icon"/>

        <!--Bootstrap.min css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap/css/bootstrap.min.css')}}">

        <!--Icons css-->
        <link rel="stylesheet" href="{{url('assets/css/icons.css')}}">

        <!--Style css-->
        <link rel="stylesheet" href="{{url('assets/css/style.css')}}">

        <!--mCustomScrollbar css-->
        <link rel="stylesheet" href="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css')}}">

        <!--Sidemenu css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toggle-menu/sidemenu.css')}}">

        <style>
            .single-page {
                width:auto;
            }

            @media (max-width: 992px){
                .single-page {
                    margin: 0;
                    width: 100% !important;
                    display: block;
                }
            }
            b,strong {
                font-size: smaller;
            }
        </style>

    </head>

    <body class="bg-primary">
        <div id="app">
            <section class="section section-2">
                <div class="container">
                    <div class="row">
                        <div class="single-page single-pageimage construction-bg cover-image">
                            <div class="row" style="display:block">
                                <div class="col-lg-6" style="max-width: 100%">
                                    <div class="wrapper wrapper2">
                                        <form id="login" class="card-body" tabindex="500" method="POST" action="{{ route('password.update') }}">
                                            @csrf
                                            <input type="hidden" name="token" value="{{ $token }}">
                                            <img src="{{url('assets/img/brand/logo-inbi-200x113.png')}}" class="mb-2 mt-4 mt-lg-0 " alt="logo">
                                            <h3>{{ __('Reset Password') }}</h3>
                                            <div class="mail">
                                                <input id="email" type="email" name="email" value="{{ $email ?? old('email') }}" required autofocus>
                                                <label>Email</label>
                                            </div>
                                            <div class="passwd">
                                                <input id="password" type="password" name="password" required>
                                                <label>Password</label>
                                            </div>
                                            <div class="passwd">
                                                <input id="password-confirm" type="password" name="password_confirmation" required>
                                                <label>Confirm Password</label>
                                            </div>
                                            @if ($errors->has('password'))
                                                <div>
                                                    <span class="alert alert-danger" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                </div>
                                            @endif
                                            @if ($errors->has('email'))
                                                <div>
                                                    <span class="alert alert-danger" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                </div>
                                            @endif
                                            <div class="submit">
                                                <button type="submit" class="btn btn-primary btn-block">
                                                    {{ __('Reset Password') }}
                                                </button>
                                            </div>
                                        </form>
                                        {{--
                                        <div class="card-body border-top">
                                            <a class="btn  btn-social btn-facebook btn-block"><i class="fa fa-facebook"></i> Acceder con Facebook</a>
                                            <a class="btn  btn-social btn-google btn-block mt-2"><i class="fa fa-google-plus"></i> Acceder con Google</a>
                                        </div>
                                        --}}
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </section>
        </div>

        <!--Jquery.min js-->
        <script src="{{url('assets/js/jquery.min.js')}}"></script>

        <!--popper js-->
        <script src="{{url('assets/js/popper.js')}}"></script>

        <!--Tooltip js-->
        <script src="{{url('assets/js/tooltip.js')}}"></script>

        <!--Bootstrap.min js-->
        <script src="{{url('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

        <!--Jquery.nicescroll.min js-->
        <script src="{{url('assets/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>

        <!--Scroll-up-bar.min js-->
        <script src="{{url('assets/plugins/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
        
        <script src="{{url('assets/js/moment.min.js')}}"></script>

        <!--mCustomScrollbar js-->
        <script src="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

        <!--Sidemenu js-->
        <script src="{{url('assets/plugins/toggle-menu/sidemenu.js')}}"></script>

        <!--Scripts js-->
        <script src="{{url('assets/js/scripts.js')}}"></script>

    </body>
</html>
