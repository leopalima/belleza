@extends('layout.index')

@section('libreriascss')
{{-- Formularios --}}
<!--Morris css-->
<link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
{{-- Formularios avanzados --}}
<!--Bootstrap-daterangepicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">

<!--Bootstrap-datepicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}">

<!--iCheck css-->
<link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}">

<!--Bootstrap-colorpicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}">

<!--Bootstrap-timepicker css-->
<link rel="stylesheet" href="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">

<!--Select2 css-->
<link rel="stylesheet" href="{{url('assets/plugins/select2/select2.css')}}">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@endsection

@section('cuerpo')
{{-- Hover Table - Headercolor Table --}}
<div class="row">
	<div class="col-lg-12">
		<div class="card">
		{{--  <div style="display:scroll; position:fixed;bottom:100px;right:50px;"><button id="btnNuevo" data-url="{{route('categorias.store')}}" class="btn btn-info m-b-5 m-t-5">Nuevo</button></div>
		<div style="display:scroll; position:fixed;top:100px;right:50px;"><button id="btnAsignarCategoria" data-url="{{route('categorias.store')}}" class="btn btn-info m-b-5 m-t-5">Asignar categoría</button></div>  --}}
			<div class="card-header">
				<h4>Categorías</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover mb-0 text-nowrap">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Formulación</th>
							<th>Diseño</th>
							<th></th>
						</tr>
						@foreach($categorias as $categoria)
						<tr>
							<td>{{$categoria->id}}</td>
							<td>{{$categoria->nombre}}</td>
							<td>
                                @if($categoria->formulacion)
                                <i class="fa fa-check" data-toggle="tooltip" title="" data-original-title=" fa-check"></i>
                                @else
                                <i class="fa fa-close" data-toggle="tooltip" title="" data-original-title=" fa-check"></i>
                                @endif
                            </td>
							<td>
                                @if($categoria->disenio)
                                <i class="fa fa-check" data-toggle="tooltip" title="" data-original-title=" fa-check"></i>
                                @else
                                <i class="fa fa-close" data-toggle="tooltip" title="" data-original-title=" fa-check"></i>
                                @endif
                            </td>
							<td>
								<button type="button" name="btnEditar" id="btnEditar" class="btn btn-info" data-categoria_id="{{$categoria->id}}" data-url="{{action('usuarios\CategoriasController@update', ['id' => $categoria->id])}}">Editar</button>
							</td>
						</tr>
						@endforeach
                    </table>
                    <div>
                        <button id="btnNuevo" data-url="{{route('categorias.store')}}" class="btn btn-info m-b-5 m-t-5">Nuevo</button>
                        <button id="btnAsignarCategoria" data-url="{{route('categorias.store')}}" class="btn btn-warning m-b-5 m-t-5">Asignar categoría</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('libreriasjavascript')
{{-- Formularios avanzados --}}
<!--Select2 js-->
<script src="{{url('assets/plugins/select2/select2.full.js')}}"></script>
<!--Inputmask js-->
<script src="{{url('assets/plugins/inputmask/jquery.inputmask.js')}}"></script>
<!--Moment js-->
<script src="{{url('assets/plugins/moment/moment.min.js')}}"></script>
<!--Bootstrap-daterangepicker js-->
<script src="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!--Bootstrap-datepicker js-->
<script src="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<!--Bootstrap-colorpicker js-->
<script src="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
<!--Bootstrap-timepicker js-->
<script src="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
<!--iCheck js-->
<script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
<!--forms js-->
<script src="{{url('assets/js/forms.js')}}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Message Modal -->
<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Editar categoría</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="frmEditar" action="" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Nombre de la categoría:</label>
                        <input type="text" class="form-control" id="nombre" name="nombre">
                        <input type="hidden" class="form-control" id="id" name="id">
                        <input type="hidden" class="form-control" id="modo" name="modo">
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="checkbox" class="minimal" id="chkFormulacion" name="formulacion">
                            Formulacion
                        </label>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="checkbox" class="minimal" id="chkDisenio" name="disenio">
                            Diseño
                        </label>
                    </div>
                    <div class="memberblock mb-0">
                        <div class="row ">
                            <div class="col-lg-4 pl-1 pr-1 m-t-5 m-b-5">
                                {{-- Foto de 150x150 --}}
                                <a href="#" class="member"> <img height="150" width="150" id="img_icon_url" src="" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="">
                                <input id="icon_url" name="icon_url" type="file" accept="image/*" class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                <button id="btnGuardar" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para asignar categoria a un usuario -->
<div class="modal fade" id="modalAsignar" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="example-Modal3">Asignar categoría</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="frmAsignar" action="" method="POST">
                @csrf
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Nombre del profesional:</label>
                        <input type="text" class="form-control" id="nombreProf" name="nombreProf" val="">
                        <input type="hidden" class="form-control" id="prof_id" name="prof_id">
                    </div>
                    @foreach($categorias as $categoria)
                    <div class="form-group">
                        <label>
                            <input type="checkbox" class="minimal" id="chkFormulacion{{$categoria->id}}" name="categorias[]" value="{{$categoria->id}}">
                            {{$categoria->nombre}}
                        </label>
                    </div>
                    @endforeach
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                <button id="btnGuardarAsignarCategoria" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function($) {

        $.each($("#btnEditar"), function(){
            $("table").delegate("#btnEditar", "click", function(){
                $boton = $(this);
                $("#frmEditar").attr('action', $boton.attr('data-url'));
                $("#modalEditar").modal("toggle");
                $("#modo").val("edicion");
                buscarCategoria($boton.data('categoria_id'))

            });
        });

        $("#btnGuardar").click(function(){
            if($("#modo").val() == "edicion"){
                guardarCategoria();  
            }
            if($("#modo").val() == "nuevo"){
                $('input[name="_method"]').val("");
                nuevaCategoria();
            }            
        })

        $("#btnGuardarAsignarCategoria").click(function(e){
            e.preventDefault();
            console.log("btnGuardarAsignarCategoria");
            guardarAsignarCategoria();
        })

        $("#btnNuevo").click(function(){
            $boton = $(this);
            console.log($boton.attr('data-url'));
            $("#frmEditar").attr('action', $boton.attr('data-url'));
            $('input[id="chkFormulacion"]').iCheck('uncheck');
            $('input[id="chkDisenio"]').iCheck('uncheck');
            $("#frmEditar")[0].reset();
            $("#modo").val("nuevo");
            $("#img_icon_url").attr("src", "");
            $("#modalEditar").modal("toggle");
        })

        $("#btnAsignarCategoria").click(function(){
            $boton = $(this);
            $("#frmEditar").attr('action', $boton.attr('data-url'));
            $('input[name="categorias[]"]').iCheck('uncheck');
            $("#frmEditar")[0].reset();
            $("#modo").val("nuevo");
            $("#prof_id").val("");
            $("#nombreProf").val("");
            $("#modalAsignar").modal("toggle");
        })

    });

function buscarCategoria($id){
    $url = "{{route('categorias.buscar')}}";
    $.ajax({
        url: $url,
        data: {data:$id},
        method: "POST",
        dataType: "JSON",
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    })
    .done(function(d){
        $("#nombre").val(d.nombre);
        $("#id").val(d.id);
        $("#img_icon_url").attr("src", d.icon_url);
        if(d.formulacion==1){
            $('input[id="chkFormulacion"]').iCheck('check');
        }else{
            $('input[id="chkFormulacion"]').iCheck('uncheck');
        }
        if(d.disenio==1){
            $('input[id="chkDisenio"]').iCheck('check');
        }else{
            $('input[id="chkDisenio"]').iCheck('uncheck');
        }
    });
}

function guardarCategoria(){
    console.log("Guardando...");
    $url = $("#frmEditar").attr("action");
    $data = new FormData(jQuery("#frmEditar")[0]);

    $.ajax({
        url: $url,
        data: $data,
        type: "POST",
        dataType: "JSON",
        cache: false,
        contentType: false,
        processData: false,
        async: true,
    }).done(function(d){
        // console.log(d);
        location.reload();
    });
}

function nuevaCategoria(){
    console.log("Guardando...");
    $url = $("#frmEditar").attr("action");
    $data = new FormData(jQuery("#frmEditar")[0]);

    $.ajax({
        url: $url,
        data: $data,
        method: "POST",
        dataType: "JSON",
        cache: false,
        contentType: false,
        processData: false,
        async: true,
    })
    .done(function(d){
        // console.log(d);
        if(d.error){
            console.log("error");
        }
        if(d.exito==1){
            location.reload();
        }
    });
}

function categoriaUsuario($id){
    $url = "{{route('categorias.usuario')}}/" + $id;
    $.ajax({
        url: $url,
        method: "GET",
        dataType: "JSON",
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    })
    .done(function(d){
        console.log(d);
        $('input[id="chkFormulacion*"]').iCheck('uncheck');
        for($i=0; $i < d.length; $i++){
            if($('input[id="chkFormulacion'+ d[$i].categoria_id +'"]').val() == d[$i].categoria_id){
                $('input[id="chkFormulacion'+ d[$i].categoria_id +'"]').iCheck('check');
            }
        }
    });
}

function guardarAsignarCategoria(){
    $url = "{{route('categorias.usuario.guardar')}}";
    $data = $("#frmAsignar").serialize()
    $.ajax({
        url: $url,
        data: $data,
        method: "POST",
        dataType: "JSON",
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    })
    .done(function(d){
        if(d.exito == 1){
            location.reload();
        }
        if(d.error == 0){
            console.log(d);
        }
    });
}

</script>

<script>
  $( function() {
    
    $("#nombreProf").autocomplete({
      source: "{{route('categorias.buscarProfesional')}}",
      appendTo: "#frmAsignar",
      select: function( event, ui ) {
        event.preventDefault();
        $("#nombreProf").val(ui.item.label);
        $("#prof_id").val(ui.item.value);
        categoriaUsuario(ui.item.value);
        return ui.item.label;
        // console.log( "Selected: " + ui.item.label + " ***** " + ui.item.value );
      }
    });
  });
  </script>
@endsection
