@extends('layout.index')

@section('libreriascss')
{{-- Formularios --}}
<!--Morris css-->
<link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
{{-- Notificaciones emergentes --}}
<!--Toastr css-->
<link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">
<!--iCheck css-->
<link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}">
@endsection

@section('cuerpo')
{{-- Formularios - General Elements --}}
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h4>Rol y sus servicios</h4>
			</div>
			<div class="card-body">
				<form name="frmRoles" class="form-horizontal" method="POST" action="{{action('roles\rolesController@update', ['id' => $rol->id])}}">
					@csrf
					@method('PUT')
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Nombre</label>
						<div class="col-md-9">
							<input type="text" name="name" id="name" class="form-control" value="{{old('name') ? old('name') :  $rol->name}}" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Descripción</label>
						<div class="col-md-9">
							<textarea class="form-control" name="descripcion" rows="6" readonly>{{old('descripcion') ? old('descripcion') :$rol->descripcion}}</textarea>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h4>Servicios del rol: {{$rol->name}}</h4>
			</div>
			<div class="card-body">
				<form name="frmRoles" class="form-horizontal" method="POST" action="{{action('roles\rolesController@update', ['id' => $rol->id])}}">
					@csrf
					@method('PUT')
					<div class="row">
					@php
						$i=0;
					@endphp
					@foreach($servicios as $servicio)
					@php
						$check = '';
					@endphp

					@php
					if (in_array($servicio->id, $serviciosAsociados)) {
					    $check = 'checked';
					}
					@endphp



					

					<div class="col-md-6">
						<div class="form-group">
								<label>
									<input type="checkbox" {{$check}} id="chkServ" name="chkServ" value="{{$servicio->id}}" data-url={{url()->current()}} class="minimal">
									{{$servicio->name}}
								</label>
						</div>
					</div>
					@endforeach
					</div>
					<div class="form-group row">
						<div class="col-lg-12">
							<a href="{{route('roles.index')}}" class="btn btn-primary">Volver</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}" />
@if(session('actualizado'))
	<div style="display: none;" id="actualizado" name="actualizado" class="alert alert-success" data-mensaje="{{session('actualizado')}}"></div>
@endif
@if($errors->has('name'))
	<div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{$errors->first('name')}}"></div>
@endif

@endsection

@section('libreriasjavascript')
{{-- Formularios avanzados --}}
<!--Select2 js-->
<script src="{{url('assets/plugins/select2/select2.full.js')}}"></script>
<!--Inputmask js-->
<script src="{{url('assets/plugins/inputmask/jquery.inputmask.js')}}"></script>
<!--Moment js-->
<script src="{{url('assets/plugins/moment/moment.min.js')}}"></script>
<!--Bootstrap-daterangepicker js-->
<script src="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!--Bootstrap-datepicker js-->
<script src="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<!--Bootstrap-colorpicker js-->
<script src="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
<!--Bootstrap-timepicker js-->
<script src="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
<!--iCheck js-->
<script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
<!--forms js-->
<script src="{{url('assets/js/forms.js')}}"></script>
{{-- Notificaciones emergentes --}}
<!--Toastr js-->
<script src="{{url('assets/plugins/toastr/build/toastr.min.js')}}"></script>
<script src="{{url('assets/plugins/toaster/garessi-notif.js')}}"></script>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function($) {

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	if($('#actualizado').length){
		$mensaje = $('#actualizado').data('mensaje');
		toastr.success($mensaje, 'inBe');
	}
	if($('#error').length){
		$mensaje = $('#error').data('mensaje');
		toastr.error($mensaje, 'inBe');
	}

	$('input').on('ifChecked', function(event){
		$url = $(this).data('url');
  		modificarServicios($url, $(this).val(), 'agregar');
	});

	$('input').on('ifUnchecked', function(event){
		$url = $(this).data('url');
  		modificarServicios($url, $(this).val(), 'quitar');
	});
});

function modificarServicios($url, $servicio, $metodo){
		$datos = {
			'servicio' : $servicio,
			'metodo' : $metodo,
		};

	  	$.post($url, $datos)
	  		.done(function(data){
	  			console.log(data);
	  			toastr.success('Actualizado', 'inBe');
	  		})

}
</script>
@endsection
