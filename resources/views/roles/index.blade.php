@extends('layout.index')

@section('libreriascss')

@endsection

@section('cuerpo')
{{-- Hover Table - Headercolor Table --}}
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h4>Roles</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover mb-0 text-nowrap">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Servicios</th>
							<th></th>
						</tr>
						@foreach($roles as $rol)
						<tr>
							<td>{{$rol->id}}</td>
							<td>{{$rol->name}}</td>
							<td>{{Str::limit($rol->descripcion, 25)}}</td>
							<td>
								@foreach($rol->servicios as $servicio)
									  {{$servicio->servicios->name}} <br />
								@endforeach
							</td>
							<td>
								<a class="btn btn-info" href="{{action('roles\rolesController@edit', ['id' => $rol->id])}}">Editar</a>
								<a class="btn btn-warning" href="{{action('roles\serviciosController@index', ['id' => $rol->id])}}">Servicios</a>
							</td>
						</tr>
						@endforeach
					</table>
					{{ $roles->links() }}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('libreriasjavascript')

@endsection

@section('javascript')
@endsection
