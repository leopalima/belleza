@extends('layout.index')

@section('libreriascss')

@endsection

@section('cuerpo')
{{-- Hover Table - Headercolor Table --}}
<div class="row">
	<div class="col-lg-12">
		<div class="card">
		{{--  <div style="display:scroll; position:fixed;bottom:100px;right:50px;"><a href="{{route('categoria.create')}}" class="btn btn-info m-b-5 m-t-5">Nuevo</a></div>  --}}
			<div class="card-header">
				<h4>Categorías</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover mb-0 text-nowrap">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Marca</th>
							<th></th>
						</tr>
						@foreach($categorias as $categoria)
						<tr>
							<td>{{$categoria->id}}</td>
							<td>{{$categoria->nombre}}</td>
							<td>{{$categoria->marca->nombre}}</td>
							<td>
								@if($categoria->propietario->id == Auth::user()->id)
								<a class="btn btn-info" href="{{action('producto\categoriaController@edit', ['id' => $categoria->id])}}">Editar</a>
								@endif
							</td>
						</tr>
						@endforeach
					</table>
					<div>
						{{ $categorias->links() }}
					</div>
					<div>
						<a href="{{route('categoria.create')}}" class="btn btn-info m-b-5 m-t-5">Nuevo</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('libreriasjavascript')

@endsection

@section('javascript')
@endsection
