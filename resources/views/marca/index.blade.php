@extends('layout.index')

@section('libreriascss')

@endsection

@section('cuerpo')
{{-- Hover Table - Headercolor Table --}}
<div class="row">
	<div class="col-lg-12">
		<div class="card">
		{{--  <div style="display:scroll; position:fixed;bottom:100px;right:50px;"><a href="{{route('marca.create')}}" class="btn btn-info m-b-5 m-t-5">Nuevo</a></div>  --}}
			<div class="card-header">
				<h4>Marcas</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover mb-0 text-nowrap">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th></th>
						</tr>
						@foreach($marcas as $marca)
						<tr>
							<td>{{$marca->id}}</td>
							<td>{{$marca->nombre}}</td>
							<td>
								@if($marca->propietario->id == Auth::user()->id)
								<a class="btn btn-info" href="{{action('producto\marcaController@edit', ['id' => $marca->id])}}">Editar</a>
								@endif
							</td>
						</tr>
						@endforeach
					</table>
					<div>
						{{ $marcas->links() }}
					</div>
					<div>
						<a href="{{route('marca.create')}}" class="btn btn-info m-b-5 m-t-5">Nuevo</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('libreriasjavascript')

@endsection

@section('javascript')
@endsection
