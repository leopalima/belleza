@extends('layout.index')

@section('libreriascss')
{{-- Formularios --}}
<!--Morris css-->
<link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
{{-- Notificaciones emergentes --}}
<!--Toastr css-->
<link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
<link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">
@endsection

@section('cuerpo')
{{-- Formularios - General Elements --}}
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h4>Marca - editar</h4>
			</div>
			<div class="card-body">
				<form enctype="multipart/form-data" name="frmMarcas" class="form-horizontal" method="POST" action="{{action('producto\marcaController@update', ['id' => $marca->id])}}">
					@csrf
					@method('PUT')
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Nombre</label>
						<div class="col-md-9">
							<input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre') ? old('nombre') : $marca->nombre}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label"></label>
						<div class="col-md-9">
							<img src="{{$marca->url_foto}}" alt="Smiley face" height="80" width="80">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Imagen</label>
						<div class="col-md-9">
							<input id="url_foto" name="url_foto" type="file" accept="image/*" class="form-control">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label"></label>
						<div class="col-md-9">
							<button type="submit" class="btn btn-info">Guardar</button>
							<a href="{{route('marca.index')}}" class="btn btn-primary">Volver</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@if(session('actualizado'))
	<div style="display: none;" id="actualizado" name="actualizado" class="alert alert-success" data-mensaje="{{session('actualizado')}}"></div>
@endif
@if($errors->has('nombre'))
	<div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{$errors->first('nombre')}}"></div>
@endif

@endsection

@section('libreriasjavascript')
{{-- Notificaciones emergentes --}}
<!--Toastr js-->
<script src="{{url('assets/plugins/toastr/build/toastr.min.js')}}"></script>
<script src="{{url('assets/plugins/toaster/garessi-notif.js')}}"></script>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function($) {
	if($('#actualizado').length){
		$mensaje = $('#actualizado').data('mensaje');
		toastr.success($mensaje, 'inBe');
	}
	if($('#error').length){
		$mensaje = $('#error').data('mensaje');
		toastr.error($mensaje, 'inBe');
	}
});
</script>
@endsection
