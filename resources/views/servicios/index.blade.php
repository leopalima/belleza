@extends('layout.index')

@section('libreriascss')

<style>

.modal-header{

	background: linear-gradient(#3d4192, #5d61bf) !important;
    color: white !important;
}

.table thead th {
    
    background: linear-gradient(#3d4192, #5d61bf) !important;
    color: white !important; 
}

.modal-dialog {
    max-width: 50%;
}


</style>

@endsection

@section('cuerpo')
{{-- Hover Table - Headercolor Table --}}
<div class="row">
@csrf
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h4>Roles</h4>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered table-hover mb-0 text-nowrap">
						<tr>
							<th>#</th>
							<th>Nombre del profesional</th>
							<th>Nombre del servicio</th>
							<th>Nombre del cliente</th>
							<th>descripción proceso</th>
							<th></th>
						</tr>
						<?php $a = 0; ?>
						@foreach($servicios as $servicio)
						<tr>
						<?php $a++; ?>
							<td>{{$a}}</td>
							<td>{{$servicio->profesional->name}}</td>
							<td>{{$servicio->formulas->name}}</td>
							<td>{{$servicio->cliente->name}}</td>
							<td>{{$servicio->formulas->descripcion_proceso}}</td>
							<td>
                                <span >
                                    <a class="btn btn-warning btn-lg mostrar" href="" data-toggle="modal" data-target="#myModal" data-href="{{$servicio->id}}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </span>
                            </td>
						</tr>
						@endforeach
					</table>
					
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade " id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detalles de producto</h4>
      </div>
      <div class="modal-body">

	  
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<input type="hidden" value="{{ csrf_token()}}" name="_token">
@endsection

@section('libreriasjavascript')



@endsection

@section('javascript')

<script>
$('#myModal').appendTo("body");

$(document).ready(function($) {
    $("#myModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);

        
		var id = link.attr("data-href")
			if(id != '')
			{
					var _token = $('input[name="_token"]').val();
					$.ajax({
					url:"/servicios/"+id,
					method:"POST",
					data:{id:id, _token:_token},
							success:function(data){
									console.log(data.recomendaciones.use)

								var compraformato = "";
								//titulo cliente
									compraformato = compraformato + "<table class='table table-bordered'>";
										compraformato = compraformato + "<thead class='thead-color'>";
										compraformato = compraformato + "<tr>";
											compraformato = compraformato + "<th>Nombre del profesional</th>";
											compraformato = compraformato + "<th>Nombre del cliente</th>";
											compraformato = compraformato + "<th>Nombre del servicio</th>";
										compraformato = compraformato + "</tr>";
										compraformato = compraformato + "</thead>";
										compraformato = compraformato + "<tbody>";
										compraformato = compraformato + "<tr>";
											compraformato = compraformato + "<td>"+ data.profesional.name +"</td>";
											compraformato = compraformato + "<td>"+ data.cliente.name +"</td>";
											compraformato = compraformato + "<td>"+ data.formulas.name +"</td>";
										compraformato = compraformato + "</tr>";
										compraformato = compraformato + "</tbody>";
									compraformato = compraformato + "</table>";
								//titulo cliente

										

								//titulo producto
									compraformato = compraformato + "<h4>Productos de la formula:</h4><table class='table table-bordered'>";
										compraformato = compraformato + "<thead class='thead-color'>";
										compraformato = compraformato + "<tr>";
											compraformato = compraformato + "<th>Nombre del producto</th>";
											compraformato = compraformato + "<th>Cantidad</th>";
											compraformato = compraformato + "<th>Medida</th>";
										compraformato = compraformato + "</tr>";
										compraformato = compraformato + "</thead>";
										compraformato = compraformato + "<tbody>";
										for(i=0; i < data.formulas.productos.length; i++){
												compraformato = compraformato + "<tr>";
												compraformato = compraformato + "<td>"+data.formulas.productos[i].producto.nombre+"</td>";
												compraformato = compraformato + "<td>"+ data.formulas.productos[i]['cantidad'] +"</td>";
												compraformato = compraformato + "<td>"+ data.formulas.productos[i]['medida'] +"</td>";
												compraformato = compraformato + "</tr>";
										}
										compraformato = compraformato + "</tbody>";
									compraformato = compraformato + "</table>";
								//titulo producto

										

								//titulo Recomendaciones
								
									compraformato = compraformato + "<h4>Recomendaciones de la formula:</h4><table class='table table-bordered'>";
										compraformato = compraformato + "<thead class='thead-color'>";
										compraformato = compraformato + "<tr>";
											compraformato = compraformato + "<th>Tipo</th>";
											compraformato = compraformato + "<th>Recomendaciones</th>";
										compraformato = compraformato + "</tr>";
										compraformato = compraformato + "</thead>";
										compraformato = compraformato + "<tbody>";

										for(i=0; i < data.recomendaciones.dodont.length; i++){
												compraformato = compraformato + "<tr>";
												compraformato = compraformato + "<td>"+data.recomendaciones.dodont[i].tipo+"</td>";
												compraformato = compraformato + "<td>"+ data.recomendaciones.dodont[i].tarea +"</td>";
												compraformato = compraformato + "</tr>";
									
										}
										compraformato = compraformato + "</tbody>";
									compraformato = compraformato + "</table>";

								//titulo Recomendaciones

										

								//titulo Recomendaciones producto

										compraformato = compraformato + "<h4>Recomendacion de productos:</h4><table class='table table-bordered'>";
										compraformato = compraformato + "<thead class='thead-color'>";
										compraformato = compraformato + "<tr>";
											compraformato = compraformato + "<th>Nombre del producto</th>";
											compraformato = compraformato + "<th>Cantidad</th>";
										compraformato = compraformato + "<th>uso</th>";
										compraformato = compraformato + "</tr>";
										compraformato = compraformato + "</thead>";
										compraformato = compraformato + "<tbody>";

										for(i=0; i < data.recomendaciones.use.length; i++){
												compraformato = compraformato + "<tr>";
												compraformato = compraformato + "<td>"+data.recomendaciones.use[i].producto.nombre+"</td>";
												compraformato = compraformato + "<td>"+data.recomendaciones.use[i].cantidad +"</td>";
												compraformato = compraformato + "<td>"+data.recomendaciones.use[i].medida +"</td>";
												compraformato = compraformato + "</tr>";
									
										}
										compraformato = compraformato + "</tbody>";
									compraformato = compraformato + "</table>";
								
								//titulo Recomendaciones producto


								$("#myModal").find(".modal-body").html(compraformato);

							}
					});
			}
        
		

    });
})

</script>




@endsection
