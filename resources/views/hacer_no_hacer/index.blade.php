@extends('layout.index')

@section('libreriascss')

<style>

    #user_table th{

        background: linear-gradient(#3d4192, #5d61bf);
        color: white;

    }

    .btn-info:hover{
        color:#01b8ff !important;
        background-color: #fff !important;
        border-color: #01b8ff !important;
    }

    .btn-warning:hover{
        color: #ffb209 !important;
        background-color: #fff !important;
        border-color: #ffb209 !important;
    }

    .btn-danger:hover{
        color: #ff473d !important;
        background-color: #fff !important;
        border-color: #ff473d !important;
    }

    .btn-secondary:hover{
        color: #6c757d !important;
        background-color: #fff !important;
        border-color: #6c757d !important;
    }

</style>
@endsection

@section('cuerpo')
{{-- Hover Table - Headercolor Table --}}
<div class="row">
	<div class="col-lg-12">
		<div class="card">
		{{--  <div style="display:scroll; position:fixed;bottom:100px;right:50px;" ><button data-toggle="modal" id="create_record"  class="btn btn-info m-b-5 m-t-5" >Nuevo</button></div>  --}}
			<div class="card-header">
				<h4>
                    @switch($dodont)
                        @case('hacer')
                            Do
                            @break
                        @case('no hacer')
                            Dont
                            @break
                        @default
                            {{$dodont}}
                    @endswitch
                </h4>
			</div>
			<div class="card-body">
				<div class="table-responsive ">
					<table class="table table-bordered  mb-0 text-nowrap" id="user_table" >
						<tr>
							<th>#</th>
							<th>Description</th>
							<th></th>
						</tr>
						
						<tr>
                        @foreach($hacer as $h)
						<tr>
							<td>{{$h->id}}</td>
							<td>{{$h->descripcion}}</td>
							<td>
								<button class="btn btn-warning editar" id="{{ $h->id }}" name="editar" >Editar</button>
                                <button type="button" id="{{ $h->id }}" class="btn btn-danger eliminar">Eliminar</button>
							</td>
						</tr>
						@endforeach
					</table>
                    {{ $hacer->links() }}
                    <div><button data-toggle="modal" id="create_record"  class="btn btn-info m-b-5 m-t-5" >Nuevo</button></div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Button trigger modal -->

<!-- Modal guardar / editar -->
<div class="modal fade " id="formmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Crear</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">

            <span id="form_result"></span>
             <form method="post" id="sample_form" class="form-horizontal validation" enctype="multipart/form-data">
              @csrf
                <div class="form-group  col-md-12">
                            <input type="hidden" name="tipohacer", value="{{$dodont}}">
	  						<label for="validationTextarea">descripción</label>
							<textarea class="form-control" id="hacerdescripcion" name="hacerdescripcion" placeholder=""></textarea>
	  			</div>
               <div class="form-group" align="center">
                    <input type="hidden" name="action" id="action" />
                    <input type="hidden" name="hidden_id" id="hidden_id" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" name="action_button" id="action_button" class="btn btn-info" value="Guardar"/>
               </div>
             </form>
        </div>
    </div>
  </div>
</div>
<!-- Modal guardar editar -->


<!-- Modal confirmar eliminar -->
<div class="modal fade" id="confirmmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar descripción de uso </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Esta seguro que quiere eliminar esta descripción de uso?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="ok_button"class="btn btn-danger ">Ok</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal confirmar eliminar -->


@endsection

@section('libreriasjavascript')

$(document)

@endsection

@section('javascript')
<script type="text/javascript">

$(document).ready(function() {
  			
   
    $('#create_record').click(function(){

        $('.modal-title').text("Crear descripción");
        $('#form_result').html('');
        $('#sample_form')[0].reset();
        $('#action_button').removeClass("btn-warning").addClass("btn-info").val("Guardar");
        $('#action').val("Guardar");
        $('#formmodal').modal('show').appendTo("body");
        
    });
      

            $('#sample_form').on('submit', function(event){
               
                    event.preventDefault();
            //crear guardar       
                    if($('#action').val() == 'Guardar')
                    {
                        
                          $.ajax({
                           url:"{{ route('hacer_no_hacer.store') }}",
                           method:"POST",
                           data: new FormData(this),
                           contentType: false,
                           cache:false,
                           processData: false,
                           dataType:"json",
                                success:function(data)
                                {
                                    var html = '';
                                    if(data.errors)
                                    {
                                        
                                       html = '<div class="alert alert-danger">';

                                       for(var count = 0; count < data.errors.length; count++)
                                       {
                                                
                                               html += '<p>' + 'El campo descripción no puede estar vacío' + '</p>';
                                        }

                                       html += '</div>';
                                    }

                                    if(data.success)
                                    {
                                     html = '<div class="alert alert-success">' + data.success + '</div>';
                                     $('#sample_form')[0].reset();
                                     $("#user_table").load(" #user_table");
                                    }
                                    $('#form_result').html(html);

                                }
                          })
                    }
            //crear guardar  
            //editar guardar
                    // console.log($('#action').val())
                        if($('#action').val() == "editar")
                        {
                            var id = $('#hidden_id').val();
                            $.ajax({
                             url:"/do /"+id,
                             method:"POST",
                             data:new FormData(this),
                             contentType: false,
                             cache: false,
                             processData: false,
                             dataType:"json",
                             success:function(data)
                             {
                                var html = '';
                                if(data.errors)
                                {
                                    html = '<div class="alert alert-danger">';
                                    for(var count = 0; count < data.errors.length; count++)
                                    {
                                     html += '<p>' + 'El campo descripción no puede estar vacío' + '</p>';
                                    }
                                    html += '</div>';
                                }
                                if(data.success)
                                {
                                    html = '<div class="alert alert-success">' + data.success + '</div>';
                                    
                                    $("#user_table").load(" #user_table");
                                    
                                    
                                }

                                    $('#form_result').html(html);
                             }
                            });
                        }
            //editar guardar
                
            });
            //editar
                $(document).on('click', '.editar', function(){

                    var id = $(this).attr('id');
                    $('#form_result').html('');
                    $.ajax({
                        url:"/do /"+id,
                        dataType:"json",
                        success:function(html){


                            if(html.data.tipo =='Hacer')
                            {
                                $("div.select select").val("Hacer");

                            }else{
                                $("div.select select").val("No hacer");

                            }

                            $('#hacerdescripcion').val(html.data.descripcion);
                            $('#hidden_id').val(html.data.id);
                            $('.modal-title').text("Editar la descripción");
                            $('#action_button').val("Editar");
                            $('#action_button').removeClass("btn-info");
                            $('#action_button').addClass("btn-warning");
                            $('#action').val("editar");
                            $('#formmodal').modal('show');
                            $("#formmodal").appendTo("body");
                        }
                    })
                });

            //editar 
            //eliminar
                var user_id;

                $(document).on('click', '.eliminar', function(){
                 user_id = $(this).attr('id');
                 $("#confirmmodal").modal('show');
                 $("#confirmmodal").appendTo("body");
                 $('#ok_button').text('Ok');
                });

                $('#ok_button').click(function(){
                 $.ajax({
                  url:"destroy/"+user_id,
                  beforeSend:function(){
                   $('#ok_button').text('Eliminando...');
                  },
                  success:function(data)
                  {
                   setTimeout(function(){
                    $('#confirmmodal').modal('hide');
                    $("#user_table").load(" #user_table");
                   }, 2000);
                  }
                 })
                });
            //eliminar


});

</script>



@endsection
